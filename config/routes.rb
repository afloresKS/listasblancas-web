Rails.application.routes.draw do

  resources :shops
  resources :upload_files
  resources :list_blancas
  resources :historics

  get 'home/index'
  get 'ptlfs/filtro'
  get 'clients/show_client'
  get 'clients/dynamic'
  get 'cards/send_file'

  get 'message/index'
  filter :locale
  resources :views
  resources :profiles
  resources :permissions
  resources :areas
  resources :home
  resources :users
  resources :bins
  resources :banks
  resources :switches
  resources :clients
  resources :card_types
  resources :cards
  resources :districts
  resources :regions
  resources :currencies
  resources :bins
  resources :partial_transacctions
  resources :transacction_types
  resources :ptlfs
  resources :plans
  resources :promotions
  resources :affiliations
  resources :affiliate_promotions
  resources :routers
  get '/:locale' => 'home#index'

  #devise_for :users
  devise_for :users, :controllers => {:registrations => "user/registrations", :sessions => "user/sessions"}, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_scope :user do
    authenticated :user do
      root :to => 'home#index', as: :authenticated_root
    end
    unauthenticated :user do
      root :to => 'devise/sessions#new', as: :unauthenticated_root
      resources :expired_accounts
      get "expired_accounts/new"
    end
    get "auth/logout" => "devise/sessions#destroy"
  end
end
