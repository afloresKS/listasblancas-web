# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
# Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

#Rails.application.config.assets.precompile += %w[base/variables.scss]
#Rails.application.config.assets.precompile += Ckeditor.assets
# Rails.application.config.assets.precompile += %w(ckeditor/*)
# Rails.application.config.assets.precompile += %w(plugin/flot/jquery.flot.cust.js)
# Rails.application.config.assets.precompile += %w(plugin/flot/jquery.flot.resize.js)
# Rails.application.config.assets.precompile += %w(plugin/flot/jquery.flot.tooltip.min.js)
# Rails.application.config.assets.precompile += %w( demo.js )
#
# Rails.application.config.assets.precompile += %w( plugin/pace/pace.js )
# Rails.application.config.assets.precompile += %w( plugin/chartjs/chart.js )
# Rails.application.config.assets.precompile += %w( metrics.js )

Rails.application.config.assets.precompile += %w( style.css )
Rails.application.config.assets.precompile += %w( profiles.js )
Rails.application.config.assets.precompile += %w( profiles.css )
Rails.application.config.assets.precompile += %w( areas.js )
Rails.application.config.assets.precompile += %w( areas.css )
Rails.application.config.assets.precompile += %w( views.js )
Rails.application.config.assets.precompile += %w( views.css )
Rails.application.config.assets.precompile += %w( chartkick.js )
Rails.application.config.assets.precompile += %w( charts/jquery.flot.js )
Rails.application.config.assets.precompile += %w( charts/jquery.flot.time.js )
Rails.application.config.assets.precompile += %w( charts/jquery.flot.animator.min.js )
Rails.application.config.assets.precompile += %w( charts/jquery.flot.resize.min.js )
Rails.application.config.assets.precompile += %w( sparkline.min.js )
Rails.application.config.assets.precompile += %w( easypiechart.js )
Rails.application.config.assets.precompile += %w( charts.js )
Rails.application.config.assets.precompile += %w( footable/footable.core.css )
Rails.application.config.assets.precompile += %w( footable/footable.js )
Rails.application.config.assets.precompile += %w( jquery.maskMoney.min.js )
Rails.application.config.assets.precompile += %w( dataTables/jquery.dataTables.js )
Rails.application.config.assets.precompile += %w( dataTables/dataTables.TableTools.js )
Rails.application.config.assets.precompile += %w( dataTables/jquery.js )
Rails.application.config.assets.precompile += %w( clients.js )
Rails.application.config.assets.precompile += %w( clients.css )
Rails.application.config.assets.precompile += %w( cards.css )
Rails.application.config.assets.precompile += %w( cards.js )
Rails.application.config.assets.precompile += %w( ptlfs.js )
Rails.application.config.assets.precompile += %w( affiliations.js )
Rails.application.config.assets.precompile += %w( affiliations.css )
Rails.application.config.assets.precompile += %w( districts.js )
Rails.application.config.assets.precompile += %w( districts.css )
Rails.application.config.assets.precompile += %w( regions.css )
Rails.application.config.assets.precompile += %w( regions.js )
Rails.application.config.assets.precompile += %w( banks.js )
Rails.application.config.assets.precompile += %w( banks.css )
Rails.application.config.assets.precompile += %w( sessions.js )
Rails.application.config.assets.precompile += %w( sessions.css )
Rails.application.config.assets.precompile += %w( registre.js )
Rails.application.config.assets.precompile += %w( registre.css )
Rails.application.config.assets.precompile += %w( perfiles.js )
Rails.application.config.assets.precompile += %w( zonas.js )
