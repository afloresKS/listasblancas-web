# Listas Blancas

EL proyecto esta corriendo con la version de Ruby 2.5.5 y Rails 5.0.7.2.

Versión del portal: 4.0.1.3

> En caso que en su pantalla le muestre que le hacen faltan migraciones, se pude revisar cuales son
las pendientes con los siguientes comandos:


 ```bash
    Rails 4
     
     rake db:migrate:status 
     Si el status se encuetra como: down, esta pendiente dicha migración
     Si el status se encuenta como: up, la migración ya ha sido guardada
    
    Rails 5
     
     rails db:migrate:status
     Si el status se encuetra como: down, esta pendiente dicha migración
     Si el status se encuenta como: up, la migración ya ha sido guardada   
    
 ``` 
 -----------------------------------------------------------------------------------------
 
## Porcesos a Configurar con CRON.

**Proceso de liberación de saldo**
> **rake tarea:saldo >> log/LiberaciónDeSaldo.log &**

**Porceso de actualización de limite de transacción**
> **rake tarea:limit >> log/LimiteDeTransacción.log &**

-----------------------------------------------------------------------------------------
 
 ***Configuración de Cron***
 
 | * | * | * | * | * | comando a ejecutar |
 | --- | --- | --- | --- | --- | ------------------- |
 | minuto (0 - 59 ) | hora (0 - 23) | dia de la semana (1 - 30/31) | mes (1 - 12) | día de la semana (0 - 6) | /bin/bash |
 
 ***EJEMPLO***
 ```
 Importante: Es necesario seguir esta configuración, ya que es la que se utiliza para puros procesos programados con Ruby on Rails
 ``` 
 * 55 2 * * * rm ***/opt/ks/web/listas_blancas/log/LiberaciónDeSaldo.log*** 
   > Eliminación del Archivo Log, todos los dias, antes de las 3 de la mañana.
   
   > En el ejemplo existe información remarcada, la cual debe de ser modificada por el usuario que configurara los procesos.
   
 * 0 3 * * * /bin/bash -l -c "cd ***/opt/ks/web/listas_blancas*** && RAILS_ENV=***development*** ***/home/sysadmin/.rbenv/shims/bundle*** exec rake ***tarea:saldo*** >> ***log/LiberaciónDeSaldo.log &*** &
   > Este proceso se ejecutará a las 3 de la mañana, actulizando el saldo de la cuenta.
   
   > En el ejemplo existe información remarcada, la cual debe de ser modificada por el usuario que configurara los procesos.
   
 ***Descripción de la información remarcada***
   
   * ***/opt/ks/web/listas_blancas***
     * Posicionarse en la raíz del proyecto y digitar: **pwd**, este comando les mostrara en pantalla la ruta completa donde se encuentra alojado el portal.
   * ***/home/sysadmin/.rbenv/shims/bundle***
     * Para obtener la ruta donde se enucuentra la librerira, digitamos el siguiente comando: **which bundle**.
   * ***developmen***
     * Este codigo, se tiene que cambiar conforme se haya levantado el portal, si el portal fue levantado como ***desarrollo(development), pruebas(test) o producción(production)***
   * ***tarea:saldo***
     * Tarea a ejecutar
   * ***log/LiberaciónDeSaldo.log &***
     * ***log***: nombre de la carpeta donde se guardara el log.
     * ***LiberaciónDeSaldo.log***: nombre del archivo log.
     * ***&***: comando para dejar el proceso, corriendo como demonio.
     
  -----------------------------------------------------------------------------------------    