class Card < ActiveRecord::Base

  has_many :ptlfs
  has_many :partial_transactions
  belongs_to :client
  belongs_to :cardType
  belongs_to :currency

end
