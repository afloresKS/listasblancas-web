class Ptlf < ActiveRecord::Base

  has_many :partialTransactions

  belongs_to :user

  belongs_to :card
  belongs_to :transacctionType
  belongs_to :plan
  belongs_to :promotion

end
