# class Role < ActiveRecord::Base
#   col_widths= [30,40,20,50,50,50,25,35,35,35,25]
#   wb = xlsx_package.workbook
#
#   wb.styles do |style|
#     highlight_cell = style.add_style(bg_color: "66CDAA",alignment: { horizontal: :center }, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
#     content_cell = style.add_style(alignment: { horizontal: :center }, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
#     date_cell = style.add_style(format_code: "yyyy-mm-dd", alignment: { horizontal: :center }, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
#     bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
#
#     wb.add_worksheet(name: "Cards") do |sheet|
#       sheet.add_row ['Customer Name', 'Bank', 'Account', 'Credit', 'Actual Balance', 'Floor Limit', 'Card Type', 'Limit Operations', 'Available Operations', 'Accounts Status', 'Transaction Detail'], style: [highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
#
#       @cardxlsx.each do |cxls|
#
#         if cxls.client.present?
#           if cxls.client.name.present? && cxls.client.last_name.present?
#             @name = cxls.client.name.to_s + ' ' + cxls.client.last_name.to_s
#           elsif cxls.client.name.present?
#             @name = cxls.client.name.to_s
#           elsif cxls.client.last_name.present?
#             @name = cxls.client.last_name.to_s
#           end
#         else
#           @name = 'N/A'
#         end
#
#         if !Bank.find_by_bank_id(cxls.cvv).nil?
#           @tienda = Bank.find_by_bank_id(cxls.cvv)
#           @tienda = @tienda.name
#         else
#           @tienda = 'N/A'
#         end
#
#         if cxls.number_card.present?
#
#           @num_card = cxls.number_card
#         else
#           @num_card = 'N/A'
#         end
#
#         if cxls.initial_balance.present?
#           convertido = number_with_delimiter(number_with_precision (cxls.initial_balance.to_f)/100, precision: 2)
#           @in_bal = '$ '+convertido.to_s
#         else
#           @in_bal = 'N/A'
#         end
#
#         if cxls.actual_balance.present?
#           convertido2 = number_with_delimiter(number_with_precision (cxls.actual_balance.to_f)/100, precision: 2)
#           @ac_bal = '$ '+convertido2.to_s
#         else
#           @ac_bal = 'N/A'
#         end
#
#         if cxls.floor_limit.present?
#           convertido3 = number_with_delimiter(number_with_precision (cxls.floor_limit.to_f)/100, precision: 2)
#           @flo_bal = '$ '+convertido3.to_s
#         else
#           @flo_bal = 'N/A'
#         end
#
#         if cxls.cardType.present?
#           if cxls.cardType.name.present?
#             @caType = cxls.cardType.name
#           end
#         else
#           @caType = 'N/A'
#         end
#
#         if cxls.transaction_initial.present?
#           @ti = cxls.transaction_initial
#         else
#           @ti = 'N/A'
#         end
#
#         if cxls.transaction_actual.present?
#           @ta = cxls.transaction_actual
#         else
#           @ta = 'N/A'
#         end
#
#         if cxls.active == true
#           @act = 'Active'
#         elsif cxls.active == false
#           @act = 'Locked'
#         end
#
#         @count = Ptlf.where("card_id = ?", cxls.id).count
#
#         @total_trans = 'Total: '+@count.to_s
#
#         sheet.add_row [@name, @tienda, @num_card, @in_bal, @ac_bal, @flo_bal, @caType, @ti, @ta, @act, @total_trans ], style: [content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell]
#       end
#
#       sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " " ], style: [bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
#       sheet.column_widths *col_widths
#
#     end
#
#   end
# end
