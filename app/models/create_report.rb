class CreateReport
  require 'action_view'
  include ActionView::Helpers::NumberHelper


  def creaReporte(total, idioma, user)
    @@valores = total
    @idioma = idioma
    @user_log = user

    # Hash de Nombre de Tienda
    @dato_tienda = Affiliation.all
    @hash_tienda = Hash.new
    @dato_tienda.each do |tienda|
      @hash_tienda.store(tienda.affiliation_id, tienda.name)
    end

    # Hash de Nombre de Region
    @dato_region = Region.all
    @hash_region = Hash.new
    @dato_tienda.each do |tienda|
      @dato_region.each do |region|
        @hash_region.store(tienda.affiliation_id, region.name)
      end
    end

    # Hash de Nombre de Distrito
    @dato_distrito = District.all
    @hash_distrito = Hash.new
    @dato_tienda.each do |tienda|
      @dato_distrito.each do |distrito|
        @hash_distrito.store(tienda.affiliation_id, distrito.name)
      end
    end

    @transaction = TransacctionType.all
    @hash_transaction = Hash.new
    @transaction.each do |trans|
      @hash_transaction.store(trans.id, trans.name)
    end

    # Numero de Cuenta
    @dato_card = Card.all
    @hash_card = Hash.new

    @dato_card_type = CardType.all
    @hash_card_type = Hash.new
    @hash_ct = Hash.new

    @dato_cliente = Client.all
    @hash_cliente = Hash.new
    @hash_name = Hash.new
    @hash_last_name = Hash.new

    @dato_card_type.each do |cat|
      @hash_ct.store(cat.id, cat.name)
    end

    @dato_cliente.each do |nombre|
      @hash_name.store(nombre.id, "#{nombre.name}" "#{nombre.last_name}")
    end

    @dato_card.each do |card|
      @hash_card.store(card.id, card.number_card)
      @hash_card_type.store(card.id, @hash_ct[card.cardType_id])
      @hash_cliente.store(card.id, @hash_name[card.client_id])
    end

    begin
      @fecha_comienzo = Time.now
      puts '>------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<'

      col_widths = [5, 35, 20, 20, 20, 20, 15, 20, 25, 20, 25, 30, 35, 30, 35, 30, 35, 30, 35, 30, 35, 5]
      p = Axlsx::Package.new
      p.use_autowidth = true
      wb = p.workbook

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "003057", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        #Titulo
        main_title_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)

        ##**************************----------------------  HOJA Transacciones -------------------------**************************##
        #
        wb.add_worksheet(name: "Transactions") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('C2:V5') #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells('E6:G6') #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 120
            image.height = 93
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end
          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          if @idioma == 'es'
            sheet.add_row [" ", " ", "Resultados de Consulta Transaccional", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Result from Transaction Search", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick]
          end
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center, center]

          if @idioma == 'es'
            sheet.add_row [" ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold]
          end
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
          ##----------------------- Cabecera -------------------------#
          if @idioma == 'es'
            sheet.add_row [' ', 'Región', 'Distrito', 'Tienda', 'Creado', 'ID de Registro', 'Número de Transacción', 'Número de Servicio', 'ID de la Transacción', 'Número de Cuenta', 'Tipo de Cuenta', 'Check Number', 'Nombre del Cliente', 'Tipo de Transacción', 'Resultado de la Transacción', 'Código de Respuesta', 'Contrato', 'Monto', 'Aprobación', 'Estatus de la Devolución', 'Check Estatus', 'Estatus de la Recuperación', ' '], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [' ', 'Region', 'District', 'Store', 'Created at', 'Registrer ID', 'Tran. Num.', 'Service Num.', 'Tran. ID', 'Account number', 'Account Type', 'Check Number', 'Customer name', 'Tran. Type', 'Tran. Results', 'Response Code', 'Contract', 'Amount', 'Approval', 'Dev. Status', 'Check Status', 'Recovery Status', ' '], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end
          @@valores.each do |cxls|

            if @hash_tienda[cxls.affiliation_id].present?
              @tienda = @hash_tienda[cxls.affiliation_id].to_s
            else
              @tienda = 'N/A'
            end

            if @hash_region[cxls.affiliation_id].present?
              @region = @hash_region[cxls.affiliation_id].to_s
            else
              @region = 'N/A'
            end

            if @hash_distrito[cxls.affiliation_id].present?
              @distrito = @hash_distrito[cxls.affiliation_id].to_s
            else
              @distrito = 'N/A'
            end

            @fecha = cxls.created_at.strftime("%d/%m/%Y  %H:%M:%S").to_s

            if cxls.registration_id.present?
              @registro_id = cxls.registration_id.to_s
            else
              @registro_id = 'N/A'
            end

            if cxls.transaction_number.present?
              @num_tran = cxls.transaction_number.to_s
            else
              @num_tran = 'N/A'
            end

            if cxls.service_number.present?
              @num_servi = cxls.service_number.to_s
            else
              @num_servi = 'N/A'
            end

            @id_tran = cxls.id

            if @hash_card[cxls.card_id].present?
              @num_cuent = @hash_card[cxls.card_id].to_s
            else
              @num_cuent = 'N/A'
            end


            if @dato_card[cxls.card_id].present?
              if @hash_card_type[cxls.card_id].present?
                @tipo_cuenta = @hash_card_type[cxls.card_id].to_s
              else
                @tipo_cuenta = 'N/A'
              end
            else
              @tipo_cuenta = 'N/A'
            end

            if cxls.check_number.present?
              @numero_check = cxls.check_number.to_s
            else
              @numero_check = 'N/A'
            end

            if @dato_card[cxls.card_id].present?
              if @hash_cliente[cxls.card_id].present?
                @nombre = @hash_cliente[cxls.card_id].to_s
              else
                @nombre = 'N/A'
              end
            else
              @nombre = 'N/A'
            end

            if @hash_transaction[cxls.transacctionType_id].present?
              @tran_name = @hash_transaction[cxls.transacctionType_id].to_s
            else
              @tran_name = 'N/A'
            end

            if cxls.response_code.present?
              if cxls.response_code == '02'
                @tra_code = 'Authorized'
                @res_code = 'Approval'
              elsif cxls.response_code == '13'
                @tra_code = 'Rejected'
                @res_code = 'Invalid amount'
              elsif cxls.response_code == '14'
                @tra_code = 'N/A'
                @res_code = 'Invalid account'
              elsif cxls.response_code == '12'
                @tra_code = 'N/A'
                @res_code = 'Invalid transaction'
              elsif cxls.response_code == '51'
                @tra_code = 'Rejected'
                @res_code = 'Insufficient balance'
              elsif cxls.response_code == '53'
                @tra_code = 'Rejected'
                @res_code = 'Nonexistent account'
              elsif cxls.response_code == '61'
                @tra_code = 'Rejected'
                @res_code = 'Exceeds floor limit'
              elsif cxls.response_code == '76'
                @tra_code = 'Rejected'
                @res_code = 'Blocked account'
              elsif cxls.response_code == '93'
                @tra_code = 'Rejected'
                @res_code = 'Operation unavailable'
              else
                @tra_code = 'Code no exist'
                @res_code = 'Code no exist'
              end
            else
              @tra_code = 'N/A'
              @res_code = 'N/A'
            end

            @contract = cxls.affiliation_id.to_s
            monto = number_with_delimiter(number_with_precision (cxls.amount.to_f) / 100, precision: 2)
            @monto = '$' + monto.to_s
            @prov = cxls.approval.to_s

            if cxls.devolution == true
              @sta_dev = 'Returned'
            elsif cxls.devolution == false
              @sta_dev = 'Not returned'
            else
              @sta_dev = 'N/A'
            end

            if cxls.returned_balance == true
              @sta_che = 'Cashed'
            elsif cxls.returned_balance == false
              @sta_che = 'In process'
            else
              @sta_che = 'N/A'
            end

            if cxls.recovered == true
              @sta_rec = 'Recovered'
            elsif cxls.recovered == false
              @sta_rec = 'Unrecovered'
            else
              @sta_rec = 'N/A'
            end

            sheet.add_row [' ', @region, @distrito, @tienda, @fecha, @registro_id, @num_tran, @num_servi, @id_tran, @num_cuent, @tipo_cuenta, @numero_check, @nombre, @tran_name, @tra_code, @res_code, @contract, @monto, @prov, @sta_dev, @sta_che, @sta_rec], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [nil, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string, :string]

          end
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          ##----------------------- CERRANDO HOJA 1 -------------------------##
          # sheet.column_widths * col_widths
          puts('Terminando hoja 1...')
        end
      end
      ##**************************----------------------- TERMINAN LIBRO DE EXCEL -------------------------**************************##
      sleep(2)
      ##-------------------- GUARDANDO EXCEL ----------------------##

      @serialTime = Time.now.strftime('%H%M%S%d%m%Y')
      @idUser = @user_log.id
      p.serialize("public/generated_xls/Transactional_Detail/ResultExportfromTransactionSearch-#{@serialTime}-#{@idUser}.xlsx")
      @name_file = "ResultExportfromTransactionSearch-#{@serialTime}-#{@idUser}.xlsx"
      @path_file = "public/generated_xls/Transactional_Detail/#{@name_file}"
      puts "Archivo creado:   #{@name_file}"
      puts('Archivo almacenado...')
      @fecha_termino = Time.now

      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #
      if @idioma == "es"
        SendFile.send_ptlfs_xls_es(@fecha_comienzo, @user_log, @path_file, @name_file, @fecha_termino).deliver
      elsif @idioma == "en"
        SendFile.send_ptlfs_xls_en(@fecha_comienzo, @user_log, @path_file, @name_file, @fecha_termino).deliver
      else
        puts 'Sin idioma'
      end
      puts 'Listo, proceso terminado'

    rescue => e
      puts red('Error HORA: ' + Time.now.to_s)
      puts red('Archivo: log/error_hilos_xls.log')
      logger = Logger.new("log/error_hilos_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)
    end
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

end

