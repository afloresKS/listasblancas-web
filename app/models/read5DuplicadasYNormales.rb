class Read

  require 'roo'

  def self.process(ruta, upload_file, idFile)

    #xlsx = Roo::Spreadsheet.open(ruta)
    xlsx = Roo::Excelx.new(ruta)

    @@x = 1
    @@y = 0
    @@a = 0
    @@c = 0

    xlsx.each() do |hash|

      @@x = @@x.to_i + 1

      total = hash.inspect

      @@trasaction_initial = 0

      @@name_account = xlsx.cell(@@x, 'A') # nombre del cliente
      @@account = xlsx.cell(@@x, 'B').to_i # cuenta
      @@cvv = xlsx.cell(@@x, 'C').to_i # Banco
      @@card_type = xlsx.cell(@@x, 'D').to_i # Tipo de Tarjeta
      @@floor_limit = xlsx.cell(@@x, 'E').to_f * 100 # Limite de Piso
      @@initial_balance = xlsx.cell(@@x, 'F').to_f * 100 # Limite de Credito
      @@trasaction_initial = xlsx.cell(@@x, 'G').to_i # Limite de Operaciones

      @@floor_limit = @@floor_limit.to_i

      if @@account != 0 # Si el registro no cuenta con una Cuenta

        @@cvv = @@cvv.to_s.rjust(3, '0') # Reajusto el CVV llenando a la izqierda con 0

        if @@card_type == 2 # Tipo de tarjeta 2 = 1
          @@card_type = 1
        elsif @@card_type == 12 # Tipo de tarjeta 12 = 2
          @@card_type = 2
        end

        ############### Doble Cuenta Inicia

        # @existCard = Card.where('number_card = "?" and cardType_id = 2',@@account)
        # @existCard = Card.find_by(number_card: @@account, cardType_id: '2')
        # @card = Card.find_or_initialize_by(number_card: @@account, cardType_id: '2')
        #
        # @cardCheque = Card.find_by_number_card(@@account)
        # @cardChequeExist = Card.where('number_card = ?',@@account.to_s).exists?
        #
        # if !@existCard.present? || @card.present?
        #   if @cardChequeExist == false
        #     @cliente = Client.new
        #     @cliente.name = @@name_account
        #     @cliente.save
        #     @@c += 1
        #     @card.number_card = @@account
        #     @card.client_id = @cliente.id
        #     @card.cvv = @@cvv
        #     @card.cardType_id = 2
        #     @card.floor_limit = @@floor_limit
        #     @card.initial_balance = @@initial_balance
        #     @card.transaction_initial = @@trasaction_initial
        #     @card.active = 1
        #     @card.actual_balance = @card.initial_balance
        #     @card.transaction_actual = @@trasaction_initial
        #     @card.save
        #     @@y += 1
        #   else
        #     @card.number_card = @@account
        #     @card.client_id = @cardCheque.client_id
        #     @card.cvv = @@cvv
        #     @card.cardType_id = 2
        #     @card.floor_limit = @@floor_limit
        #     @card.initial_balance = @@initial_balance
        #     @card.transaction_initial = @@trasaction_initial
        #     @card.active = 1
        #     @card.actual_balance = @card.initial_balance
        #     @card.transaction_actual = @@trasaction_initial
        #     @card.save
        #     @@y += 1
        #   end
        #
        # elsif @card.present? && @card.cardType_id == 2
        #   if @@initial_balance > @card.initial_balance
        #     diferencia = @@initial_balance - @card.initial_balance
        #     @card.initial_balance = @@initial_balance
        #     @card.actual_balance = @card.actual_balance + diferencia
        #   else #@@initial_balance < @card.actual_balance
        #     diferencia = @card.initial_balance - @@initial_balance
        #     @card.initial_balance = @@initial_balance
        #     @card.actual_balance = @card.actual_balance - diferencia
        #   end
        #   if @@trasaction_initial > @card.transaction_initial
        #     diferencia = @@trasaction_initial - @card.transaction_initial
        #     @card.transaction_initial = @@trasaction_initial
        #     @card.transaction_actual = @card.transaction_actual + diferencia
        #   else
        #     diferencia = @card.transaction_initial - @@trasaction_initial
        #     @card.transaction_initial = @@trasaction_initial
        #     @card.transaction_actual = @card.transaction_actual - diferencia
        #   end
        #   @card.floor_limit = @@floor_limit
        #   if @card.save
        #     @@a += 1
        #   end
        #
        # end

        ######## Doble Cuena Termina

        ######## Inserta Cuentas Normales Inicia
        # @card = Card.find_or_initialize_by(number_card: @@account)
        # if @card.present?
        #   client_id = Client.where('id = ?', @card.client_id).count
        #   if client_id.to_i < 1
        #     @client = Client.new
        #     @client.name = @@name_account
        #     if @client.save
        #       @@c += 1
        #       @card.number_card = @@account
        #       @card.client_id = @client.id
        #       @card.cvv = @@cvv
        #       # @card.cardType_id = @@card_type Cambio que se hizo para solucion el 23 de Noviembre
        #       @card.cardType_id = @@card_type
        #       @card.floor_limit = @@floor_limit
        #       @card.initial_balance = @@initial_balance
        #       @card.transaction_initial = @@trasaction_initial
        #       @card.active = 1
        #       @card.actual_balance = @card.initial_balance
        #       @card.transaction_actual = @@trasaction_initial
        #       @card.save
        #       @@y += 1
        #     end
        #   else
        #     if @@initial_balance > @card.initial_balance
        #       diferencia = @@initial_balance - @card.initial_balance
        #       @card.initial_balance = @@initial_balance
        #       @card.actual_balance = @card.actual_balance + diferencia
        #     else #@@initial_balance < @card.actual_balance
        #       diferencia = @card.initial_balance - @@initial_balance
        #       @card.initial_balance = @@initial_balance
        #       @card.actual_balance = @card.actual_balance - diferencia
        #     end
        #     if @@trasaction_initial > @card.transaction_initial
        #       diferencia = @@trasaction_initial - @card.transaction_initial
        #       @card.transaction_initial = @@trasaction_initial
        #       @card.transaction_actual = @card.transaction_actual + diferencia
        #     else
        #       diferencia = @card.transaction_initial - @@trasaction_initial
        #       @card.transaction_initial = @@trasaction_initial
        #       @card.transaction_actual = @card.transaction_actual - diferencia
        #     end
        #     @card.floor_limit = @@floor_limit
        #     if @card.save
        #       @@a += 1
        #     end
        #   end
        # end

        ######## Inserta Cuentas Normales Termina



        ############# Codigo Viejo Mike Inicia

        # @card = Card.find_or_initialize_by(number_card: @@account, cvv: @@cvv)
        # if @card.client == nil
        #   @client = Client.new()
        #   @client.name = @@name_account
        #   if @client.save
        #     @@new_cl = "Nuevo Cliente"
        #     @@c = @@c + 1
        #     @card.client_id = @client.id
        #     @card.cardType_id = @@card_type
        #     @card.floor_limit = @@floor_limit
        #     @card.initial_balance = @@initial_balance
        #     @card.transaction_initial = @@trasaction_initial
        #     @card.active = 1
        #     @card.actual_balance = @card.initial_balance
        #     @card.transaction_actual = @@trasaction_initial
        #     if @card.save
        #       # puts "Guarda la cuenta del cliente con la relacion"
        #       # Enviar correo electronico
        #       @@save = "Cuenta(s) guardada(s)"
        #       @@y = @@y + 1
        #     else
        #       @@save = "Error agregar cuenta(s)"
        #     end
        #   end
        # else
        #   if @card.cardType_id == @@card_type
        #     @card.floor_limit = @@floor_limit
        #     if @@initial_balance > @card.initial_balance
        #       diferencia = @@initial_balance - @card.initial_balance
        #       @card.actual_balance = @card.actual_balance + diferencia
        #     else
        #       if @@initial_balance < @card.actual_balance
        #         diferencia = @@initial_balance - @card.initial_balance
        #         actual = diferencia + @card.actual_balance
        #         if actual < 0
        #           @card.actual_balance = 0
        #         end
        #       end
        #     end
        #     @card.initial_balance = @@initial_balance
        #     if @@trasaction_initial > @card.transaction_initial
        #       diferencia2 = @@trasaction_initial - @card.transaction_initial
        #       @card.transaction_actual = @card.transaction_actual + diferencia2
        #     else
        #       if @@trasaction_initial < @card.transaction_actual
        #         diferencia2 = @@trasaction_initial - @card.transaction_initial
        #         actual2 = diferencia2 + @card.transaction_actual
        #         if actual2 < 0
        #           @card.transaction_actual = 0
        #         end
        #       end
        #     end
        #     @card.transaction_initial = @@trasaction_initial
        #     @card.active = 1
        #     if @card.save
        #       @@save = "Cuentas(s) modificadas"
        #       @@y = @@y + 1;
        #     end
        #   else
        #     @card2 = Card.find_or_initialize_by(number_card: @@account, cvv: @@cvv, cardType_id: @@card_type)
        #     if @card2.client == nil
        #       @card2.client_id = @card.client_id
        #       @card2.actual_balance = @@initial_balance
        #       @card2.transaction_actual = @@trasaction_initial
        #     end
        #     @card2.cardType_id = @@card_type
        #     @card2.floor_limit = @@floor_limit
        #     @card2.initial_balance = @@initial_balance
        #     @card2.transaction_initial = @@trasaction_initial
        #     @card2.active = 1
        #     if @card2.save
        #       # Cuenta agregada al usuario ya sea post o al dia
        #       # Enviar correo electronico
        #       @@save = "Cuenta(s) agregadada(s)"
        #       @@y = @@y + 1;
        #     else
        #     end
        #   end
        # end

        ############# Codigo Viejo Mike Termina
      end
    end

    #UploadFileMailer.alert_confirmation(upload_file, @@y, @@save).deliver #### Ejemplo
    @updateFileUpload = UploadFile.find(idFile)
    @updateFileUpload.end_upload_file = Time.now
    @updateFileUpload.save

    UploadFileMailer.alert_confirmation(upload_file, @@y, @@c, @@a).deliver ### BuenEnvio

  end

end