class Promotion < ActiveRecord::Base

  has_many   :ptlfs
  has_many   :affiliatePromotions
  belongs_to :transacctionType
  belongs_to :bin

end
