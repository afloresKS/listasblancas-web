class UploadFile < ActiveRecord::Base

  belongs_to :user
  
  #has_attached_file :file, :default_url => "/system/:basename.:extension"
  #has_attached_file :file, :path => "/system/:basename.:extension"
	#has_attached_file :file, :path => "#{Rails.root}/files/:basename.:extension"
	has_attached_file :file, :path => "#{Rails.root}/files/:basename.:extension"



  validates_attachment_content_type :file , :content_type => ["application/vnd.ms-excel", "text/plain",
                                                              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  ]
  validates_attachment_size :file , :less_than => 20.megabytes
end
