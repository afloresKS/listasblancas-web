class CreateFile
  require 'action_view'
  include ActionView::Helpers::NumberHelper

  def creaXlSx(valores, idioma, user)
    @@valores = valores
    puts @@valores
    @idioma = idioma
    @user_log = user

    @dato_banco = Bank.all
    @hash_banco = Hash.new
    @dato_banco.each do |banco|
      @hash_banco.store(banco.bank_id, banco.name)
    end

    @dato_card = CardType.all
    @hash_card = Hash.new
    @dato_card.each do |card|
      @hash_card.store(card.id, card.name)
    end

    @dato_cliente = Client.all
    @hash_cliente = Hash.new
    @dato_cliente.each do |cliente|
      @hash_cliente.store(cliente.id, "#{cliente.name}" "#{cliente.last_name}")
    end

    # @dato_ptlf = Ptlf.all
    # @hash_ptlf = Hash.new
    # @dato_ptlf.each do |ptlf|
    #   @hash_ptlf.store(ptlf.id, ptlf.card_id)
    # end

    begin
      @fecha_comienzo = Time.now
      puts '>------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<
            >------------------------------------------------------------------------<'

      col_widths = [5, 50, 35, 20, 20, 20, 20, 20, 30, 25, 30]
      p = Axlsx::Package.new
      p.use_autowidth = true
      wb = p.workbook

      wb.styles do |style|
        highlight_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        section__cell = style.add_style(bg_color: "003057", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true, :fg_color => 'FFFFFFFF')
        #Titulo
        main_title_cell = style.add_style(bg_color: "396F9B", alignment: {horizontal: :center}, :sz => 12, border: Axlsx::STYLE_THIN_BORDER, :b => true)
        content_cell = style.add_style(alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        date_cell = style.add_style(:num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS, alignment: {horizontal: :center}, :sz => 10, border: {:edges => [:left, :right], :style => :thin, :color => 'FF000000'})
        border_header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:top, :left], :style => :thick, :color => 'FF000000'})
        border_header_dates_notbold = style.add_style(:sz => 12, :b => false, border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        header_dates_bold = style.add_style(:sz => 12, :b => true, border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        header_dates_notbold = style.add_style(alignment: {horizontal: :left}, :sz => 12, :b => false, :num_fmt => Axlsx::NUM_FMT_YYYYMMDDHHMMSS)
        bottom = style.add_style(border: {:edges => [:top], :style => :thin, :color => 'FF000000'})
        bottom_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'})
        left_thick = style.add_style(border: {:edges => [:left], :style => :thick, :color => 'FF000000'})
        center = style.add_style(alignment: {horizontal: :center, vertical: :center}, border: {:style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        top_thick = style.add_style(border: {:edges => [:top], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        border_thick = style.add_style(border: {:edges => [:top, :left, :right, :bottom], :style => :thick, :color => 'FF000000'}, :sz => 16, :b => true)
        ##**************************----------------------  HOJA Accounts -------------------------**************************##
        #

        wb.add_worksheet(name: "Cards") do |sheet|

          #FILAS COMBINADAS
          sheet.merge_cells('C2:K5') #Título del reporte
          sheet.merge_cells('B2:B6') #logo
          sheet.merge_cells('C6:D6') #fecha creación título
          sheet.merge_cells('E6:K6') #fecha creación cont

          img = File.expand_path(Rails.root + 'app/assets/images/logoKS.png', __FILE__)
          sheet.add_image(:image_src => img, :noSelect => true, :noMove => true) do |image|
            image.width = 120
            image.height = 93
            image.anchor.from.rowOff = 2050000
            image.anchor.from.colOff = 1550000
          end
          ##---------------------------------- HEADER, LOGO ------------------------------ ##
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          if @idioma == 'es'
            sheet.add_row [" ", " ", "Resultados de Consulta Cuentas", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Result from Accounts Search", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick, border_thick]
          end
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, center]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, center]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, center, center, center, center, center, center, center, center, center]

          if @idioma == 'es'
            sheet.add_row [" ", " ", "Fecha de exportación: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold]
          elsif @idioma == 'en'
            sheet.add_row [" ", " ", "Export Date: ", " ", Time.now.to_date.strftime("%d/%m/%Y"), " ", " ", " ", " ", " ", " "], style: [nil, nil, header_dates_bold, header_dates_bold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold, header_dates_notbold]
          end
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [nil, nil, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick, bottom_thick]
          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

          ##----------------------- Cabecera -------------------------#
          if @idioma == 'es'
            sheet.add_row [' ', 'Cliente', 'Banco', 'Cuenta', 'Crédito', 'Balance Actual', 'Limite de Piso', 'Tipo de Cuenta', 'Limite de Operaciones', 'Operaciones Autorizadas', 'Estatus de la Cuenta', ' '], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          elsif @idioma == 'en'
            sheet.add_row [' ', 'Customer Name', 'Bank', 'Account', 'Credit', 'Actual Balance', 'Floor Limit', 'Card Type', 'Limit Operations', 'Available Operations', 'Accounts Status', ' '], style: [nil, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell, highlight_cell]
          end


          @@valores.each do |cxls|


            if @hash_cliente[cxls.client_id].present?
              @name = @hash_cliente[cxls.client_id].to_s
            else
              @name = 'N/A'
            end


            if @hash_banco[cxls.cvv].present?
              @tienda = @hash_banco[cxls.cvv].to_s
            else
              @tienda = 'N/A'
            end

            if cxls.number_card.present?
              @num_card = cxls.number_card.to_s
            else
              @num_card = 'N/A'
            end

            if cxls.initial_balance.present?
              convertido = number_with_delimiter(number_with_precision (cxls.initial_balance.to_f) / 100, precision: 2)
              @in_bal = '$ ' + convertido.to_s
            else
              @in_bal = 'N/A'
            end

            if cxls.actual_balance.present?
              convertido2 = number_with_delimiter(number_with_precision (cxls.actual_balance.to_f) / 100, precision: 2)
              @ac_bal = '$ ' + convertido2.to_s
            else
              @ac_bal = 'N/A'
            end

            if cxls.floor_limit.present?
              convertido3 = number_with_delimiter(number_with_precision (cxls.floor_limit.to_f) / 100, precision: 2)
              @flo_bal = '$ ' + convertido3.to_s
            else
              @flo_bal = 'N/A'
            end

            if @dato_card[cxls.cardType_id].present?
              if @hash_card[cxls.cardType_id].present?
                @caType = @hash_card[cxls.cardType_id].to_s
              else
                @caType = 'N/A'
              end
            else
              @caType = 'N/A'
            end

            if cxls.transaction_initial.present?
              @ti = cxls.transaction_initial
            else
              @ti = 'N/A'
            end

            if cxls.transaction_actual.present?
              @ta = cxls.transaction_actual
            else
              @ta = 'N/A'
            end

            if cxls.active == true
              @act = 'Active'
            elsif cxls.active == false
              @act = 'Locked'
            end

            sheet.add_row [" ", @name, @tienda, @num_card.to_s, @in_bal, @ac_bal, @flo_bal, @caType, @ti, @ta, @act], style: [nil, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell, content_cell], :types => [nil, :string, :string, :string, :string, :string, :string, :string, :string, :string]
          end

          sheet.add_row [" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "], style: [bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom, bottom]
          ##----------------------- CERRANDO HOJA 1 -------------------------##
          sheet.column_widths *col_widths
          puts('Terminando hoja 1...')
        end
      end

      ##**************************----------------------- TERMINAN LIBRO DE EXCEL -------------------------**************************##
      sleep(2)
      ##-------------------- GUARDANDO EXCEL ----------------------##
      @serialTime = Time.now.strftime('%H%M%S%d%m%Y')
      @idUser = @user_log.id
      p.serialize("public/generated_xls/Accounts/cards_-#{@serialTime}-#{@idUser}.xlsx")
      @name_file = "cards_-#{@serialTime}-#{@idUser}.xlsx"
      @path_file = "public/generated_xls/Accounts/#{@name_file}"
      puts "Archivo creado:   #{@name_file}"
      puts('Archivo almacenado...')
      @fecha_termino = Time.now

      # --------------------------------- CORREO ELECTRÓNICO -------------------------------- #
      if @idioma == "es"
        SendFile.send_accounts_xls_es(@fecha_comienzo, @user_log, @path_file, @name_file, @fecha_termino).deliver
      elsif @idioma == "en"
        SendFile.send_accounts_xls_en(@fecha_comienzo, @user_log, @path_file, @name_file, @fecha_termino).deliver
      else
        puts 'Sin idioma'
      end
      puts 'Listo, proceso terminado'

    rescue => e
      puts e.to_s
      puts red('Error HORA: ' + Time.now.to_s)
      puts red('Archivo: log/error_hilos_xls.log')
      logger = Logger.new("log/error_hilos_xls.log")
      logger.error('----------------------------------------------------------')
      logger.error(e)

    end
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end
end