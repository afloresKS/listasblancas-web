class Saldos

  @@dias = 5

  def recuperar
    fecha = Time.now.to_date - @@dias
    puts "Iniciando proceso " + Time.now.to_s
    puts "Dias a procesar: " + @@dias.to_s
    puts "Fecha: " + fecha.to_s
    #Consultando los saldos
    transacciones = Ptlf.where("updated_at <= ? AND devolution = ? AND returned_balance = ? AND response_code='02'", fecha.to_s, false, false)
    puts "Transacciones a procesar: " + transacciones.count.to_s
    transacciones.each do |datos|
      if datos.card != nil
        if datos.card.actual_balance == nil
          datos.card.actual_balance = datos.amount
        else
          datos.card.actual_balance += datos.amount
        end
        if datos.card.save
          datos.returned_balance = true
          datos.save
        end
      else
        puts "Registro no relacionado: " + datos.card_id.to_s
      end
    end
    #Verificacion de una cuenta que no tenga m�s dinero en el balance que en el inicial
    puts "Procesando cuentas"
    cuentas = Card.where('initial_balance < actual_balance')
    puts "Cuentas a procesar: " + cuentas.count.to_s
    cuentas.each do |cuenta|
      cuenta.actual_balance = cuenta.initial_balance
      cuenta.save
    end
    puts "Termino proceso"
  end
end