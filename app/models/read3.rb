class Read

  require 'roo'

  def self.process(ruta, upload_file)

    #xlsx = Roo::Spreadsheet.open(ruta)
    xlsx = Roo::Excelx.new(ruta)

    @@x = 1
    @@y = 0
    @@c = 0

    xlsx.each() do |hash|

      @@x = @@x.to_i + 1

      total = hash.inspect

      @@trasaction_initial = 0

      @@name_account = xlsx.cell(@@x, 'A') # nombre del cliente
      @@account = xlsx.cell(@@x, 'B').to_i # cuenta
      @@cvv = xlsx.cell(@@x, 'C').to_i # Banco
      @@card_type = xlsx.cell(@@x, 'D').to_i # Tipo de Tarjeta
      @@floor_limit = xlsx.cell(@@x, 'E').to_f #* 100 # Limite de Piso
      @@initial_balance = xlsx.cell(@@x, 'F').to_f #* 100 # Limite de Credito
      @@trasaction_initial = xlsx.cell(@@x, 'G').to_i # Limite de Operaciones
      @@c = 0

      if @@account != 0 # Si el registro no cuenta con una Cuenta

        @@cvv = @@cvv.to_s.rjust(3,'0') # Reajusto el CVV llenando a la izqierda con 0

        if @@card_type == 2 # Tipo de tarjeta 2 = 1
          @@card_type = 1
        elsif @@card_type == 12 # Tipo de tarjeta 12 = 2
          @@card_type = 2
        end


        @card = Card.find_or_initialize_by(number_card: @@account)
        #@card = Card.find_by_number_card(@@account)

        if @card.client == nil
          @client = Client.new
          @client.name = @@name_account

          if @client.save
            @@n_c = "New Customers:"
            @@c += 1

            @card.client_id = @client.id
            @card.cardType_id = @@card_type
            @card.floor_limit = @@floor_limit
            @card.initial_balance = @@initial_balance
            @card.transaction_initial = @@trasaction_initial
            @card.active = 1
            @card.actual_balance = @card.initial_balance
            @card.transaction_actual = @@trasaction_initial

            if @card.save
              @@n_t = "Ne Cards: "
              @@y += 1
            end
          end

        else
          @card.floor_limit = @@floor_limit
          if  @@initial_balance >  @card.initial_balance
            diferencia = @@initial_balance - @card.initial_balance
            @card.initial_balance = @@initial_balance
            @card.actual_balance =  @card.actual_balance + diferencia
          elsif #@@initial_balance < @card.actual_balance
            diferencia = @card.initial_balance - @@initial_balance
            @card.actual_balance = @card.actual_balance - diferencia
          end
        end
        # if @card.client == nil
        #
        #   @client = Client.new()
        #   @client.name = @@name_account
        #
        #   if @client.save
        #
        #     @@new_cl = "Nuevo Cliente"
        #     @@c = @@c + 1
        #
        #     @card.client_id = @client.id
        #     @card.cardType_id = @@card_type
        #     @card.floor_limit = @@floor_limit
        #     @card.initial_balance = @@initial_balance
        #     @card.transaction_initial = @@trasaction_initial
        #     @card.active = 1
        #     @card.actual_balance = @card.initial_balance
        #     @card.transaction_actual = @@trasaction_initial
        #
        #     if @card.save
        #       # puts "Guarda la cuenta del cliente con la relacion"
        #       # Enviar correo electronico
        #       @@save = "Cuenta(s) guardada(s)"
        #       @@y = @@y + 1
        #     else
        #       @@save = "Error agregar cuenta(s)"
        #     end
        #   end
        # else
        #   # if @card.cardType_id == @@card_type
        #   #
        #   #   @card.floor_limit = @@floor_limit
        #   #   if @@initial_balance > @card.initial_balance
        #   #     diferencia = @@initial_balance - @card.initial_balance
        #   #     @card.actual_balance = @card.actual_balance + diferencia
        #   #   else
        #   #     if @@initial_balance < @card.actual_balance
        #   #       diferencia = @@initial_balance - @card.initial_balance
        #   #       actual = diferencia + @card.actual_balance
        #   #       if actual < 0
        #   #         @card.actual_balance = 0
        #   #       end
        #   #     end
        #   #   end
        #   #
        #   #
        #   #   @card.initial_balance = @@initial_balance
        #   #
        #   #   if @@trasaction_initial > @card.transaction_initial
        #   #     diferencia2 = @@trasaction_initial - @card.transaction_initial
        #   #     @card.transaction_actual = @card.transaction_actual + diferencia2
        #   #   else
        #   #     if @@trasaction_initial < @card.transaction_actual
        #   #       diferencia2 = @@trasaction_initial - @card.transaction_initial
        #   #       actual2 = diferencia2 + @card.transaction_actual
        #   #       if actual2 < 0
        #   #         @card.transaction_actual = 0
        #   #       end
        #   #     end
        #   #
        #   #   end
        #   #   @card.transaction_initial = @@trasaction_initial
        #   #   @card.active = 1
        #   #
        #   #   if @card.save
        #   #     #puts "Modifica cuenta al cliente"
        #   #     # Enviar correo electronico
        #   #
        #   #     @@save = "Cuentas(s) modificadas"
        #   #     @@y = @@y + 1;
        #   #   end
        #   # else
        #   #   @card2 = Card.find_or_initialize_by(number_card: @@account, cvv: @@cvv, cardType_id: @@card_type)
        #   #
        #   #   if @card2.client == nil
        #   #
        #   #     @card2.client_id = @card.client_id
        #   #     @card2.actual_balance = @@initial_balance
        #   #     @card2.transaction_actual = @@trasaction_initial
        #   #
        #   #   end
        #   #
        #   #   @card2.cardType_id = @@card_type
        #   #   @card2.floor_limit = @@floor_limit
        #   #   @card2.initial_balance = @@initial_balance
        #   #   @card2.transaction_initial = @@trasaction_initial
        #   #
        #   #   @card2.active = 1
        #   #   if @card2.save
        #   #     # Cuenta agregada al usuario ya sea post o al dia
        #   #     # Enviar correo electronico
        #   #
        #   #     @@save = "Cuenta(s) agregadada(s)"
        #   #     @@y = @@y + 1;
        #   #
        #   #   else
        #   #
        #   #
        #   #   end
        #   #
        #   # end
        # end
      end
    end

    UploadFileMailer.alert_confirmation(upload_file, @@y, @@save, @@c).deliver

  end

end