class Tareas

  @@dias = 5

  def self.recuperar
    fecha = Time.now.to_date - @@dias
    puts "Iniciando proceso " + Time.now.to_s
    puts "Dias a procesar: " + @@dias.to_s
    puts "Fecha: " + fecha.to_s
    #Consultando los saldos
    #transacciones = Ptlf.where("updated_at <= ? AND devolution = ? AND returned_balance = ? AND response_code='02' ", fecha.to_s, false, false)
    transacciones = Ptlf.where("created_at <= ? AND devolution = ? AND returned_balance = ? AND response_code='02' ", fecha.to_s, false, false)
    puts "Transacciones a procesar: " + transacciones.count.to_s
    transacciones.each do |datos|
      if datos.card != nil
        if datos.card.actual_balance == nil
          datos.card.actual_balance = datos.amount
        else
          datos.card.actual_balance += datos.amount
        end
        datos.card.transaction_actual += 1
        if datos.card.save
          datos.returned_balance = true
          datos.save
        end
      else
        puts "Registro no relacionado: " + datos.card_id.to_s
      end
    end
    #Verificacion de una cuenta que no tenga más dinero en el balance que en el inicial
    puts "Procesando cuentas"
=begin
    cuentas = Card.where('initial_balance < actual_balance')
    puts "Cuentas a procesar: " + cuentas.count.to_s
    cuentas.each do |cuenta|
      cuenta.actual_balance = cuenta.initial_balance
      cuenta.save
    end
=end
    Card.where('initial_balance < actual_balance').update_all('actual_balance = initial_balance')
    Card.where('transaction_initial < transaction_actual').update_all('transaction_actual = transaction_initial')

    puts "Termino proceso"
  end

  def self.devuelto

    @@actual = 0
    puts "Inicio de devolucion de limite de transacción: "+ Time.now.to_s

    #card_dev = Card.where('transaction_actual < transaction_initial')
    card_dev = Card.all

    card_dev.each do |dev|

      if dev.transaction_initial != dev.transaction_actual

        dev.transaction_actual = dev.transaction_initial

        if dev.save
          @@mensaje = "Limite de Operaciones Actualizadas: "
          @@actual = @@actual + 1

        end
      else
        @@mensaje = "Limite de Operaciones Actualizadas: "
        @@actual = 0

      end


    end
    puts @@mensaje + @@actual.to_s
    puts "Temino de actualizacion de limite de transacción."

  end

end