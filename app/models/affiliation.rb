class Affiliation < ActiveRecord::Base

  has_many :affiliatePromotions
  belongs_to :district
  belongs_to :region

end
