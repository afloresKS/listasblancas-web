class Read

  require 'roo'

  def self.process(ruta, upload_file, idFile)

    #xlsx = Roo::Spreadsheet.open(ruta)
    xlsx = Roo::Excelx.new(ruta)

    @@x = 1
    @@y = 0
    @@a = 0
    @@c = 0

    xlsx.each() do |hash|

      @@x = @@x.to_i + 1

      total = hash.inspect

      @@trasaction_initial = 0

      @@name_account = xlsx.cell(@@x, 'A') # nombre del cliente
      @@account = xlsx.cell(@@x, 'B').to_i # cuenta
      @@cvv = xlsx.cell(@@x, 'C').to_i # Banco
      @@card_type = xlsx.cell(@@x, 'D').to_i # Tipo de Tarjeta
      @@floor_limit = xlsx.cell(@@x, 'E').to_f * 100 # Limite de Piso
      @@initial_balance = xlsx.cell(@@x, 'F').to_f * 100 # Limite de Credito
      @@trasaction_initial = xlsx.cell(@@x, 'G').to_i # Limite de Operaciones

      @@floor_limit = @@floor_limit.to_i

      if @@account != 0 # Si el registro no cuenta con una Cuenta

        @@cvv = @@cvv.to_s.rjust(3, '0') # Reajusto el CVV llenando a la izqierda con 0

        if @@card_type == 2 # Tipo de tarjeta 2 = 1
          @@card_type = 1
        elsif @@card_type == 12 # Tipo de tarjeta 12 = 2
          @@card_type = 2
        end

        ############### Doble Cuenta Inicia



        @existCard = Card.find_by(number_card: @@account, cardType_id: @@card_type)

        valor = @existCard.exist?

        #if !@existCard.present?
        if @existCard.present?
          @existCard.number_card = @@account
          @existCard.client_id = @cardCheque.client_id
          @existCard.cvv = @@cvv
          @existCard.cardType_id = @@card_type
          @existCard.floor_limit = @@floor_limit
          @existCard.initial_balance = @@initial_balance
          @existCard.transaction_initial = @@trasaction_initial
          @existCard.active = 1
          @existCard.actual_balance = @card.initial_balance
          @existCard.transaction_actual = @@trasaction_initial
          @existCard.save
            @@y += 1
        else
          @cliente = Client.new
          @cliente.name = @@name_account
          @cliente.save
          @@c += 1
          @existCard.number_card = @@account
          @existCard.client_id = @cliente.id
          @existCard.cvv = @@cvv
          @existCard.cardType_id = @@card_type
          @existCard.floor_limit = @@floor_limit
          @existCard.initial_balance = @@initial_balance
          @existCard.transaction_initial = @@trasaction_initial
          @existCard.active = 1
          @existCard.actual_balance = @card.initial_balance
          @existCard.transaction_actual = @@trasaction_initial
          @existCard.save
          @@y += 1
        end

        ######## Doble Cuena Termina

        ######## Inserta Cuentas Normales Inicia
        # @card = Card.find_or_initialize_by(number_card: @@account)
        # if @card.present?
        #   client_id = Client.where('id = ?', @card.client_id).count
        #   if client_id.to_i < 1
        #     @client = Client.new
        #     @client.name = @@name_account
        #     if @client.save
        #       @@c += 1
        #       @card.number_card = @@account
        #       @card.client_id = @client.id
        #       @card.cvv = @@cvv
        #       # @card.cardType_id = @@card_type Cambio que se hizo para solucion el 23 de Noviembre
        #       @card.cardType_id = @@card_type
        #       @card.floor_limit = @@floor_limit
        #       @card.initial_balance = @@initial_balance
        #       @card.transaction_initial = @@trasaction_initial
        #       @card.active = 1
        #       @card.actual_balance = @card.initial_balance
        #       @card.transaction_actual = @@trasaction_initial
        #       @card.save
        #       @@y += 1
        #
        #       @cuenta = Card.new
        #       @cuenta.number_card = @@account
        #       @cuenta.client_id = @client.id
        #       @cuenta.cvv = @@cvv
        #       # @card.cardType_id = @@card_type Cambio que se hizo para solucion el 23 de Noviembre
        #       @cuenta.cardType_id = 2
        #       @cuenta.floor_limit = @@floor_limit
        #       @cuenta.initial_balance = @@initial_balance
        #       @cuenta.transaction_initial = @@trasaction_initial
        #       @cuenta.active = 1
        #       @cuenta.actual_balance = @card.initial_balance
        #       @cuenta.transaction_actual = @@trasaction_initial
        #       @cuenta.save
        #       @@y += 1
        #
        #
        #     end
        #   else
        #     if @@initial_balance > @card.initial_balance
        #       diferencia = @@initial_balance - @card.initial_balance
        #       @card.initial_balance = @@initial_balance
        #       @card.actual_balance = @card.actual_balance + diferencia
        #     else #@@initial_balance < @card.actual_balance
        #       diferencia = @card.initial_balance - @@initial_balance
        #       @card.initial_balance = @@initial_balance
        #       @card.actual_balance = @card.actual_balance - diferencia
        #     end
        #     if @@trasaction_initial > @card.transaction_initial
        #       diferencia = @@trasaction_initial - @card.transaction_initial
        #       @card.transaction_initial = @@trasaction_initial
        #       @card.transaction_actual = @card.transaction_actual + diferencia
        #     else
        #       diferencia = @card.transaction_initial - @@trasaction_initial
        #       @card.transaction_initial = @@trasaction_initial
        #       @card.transaction_actual = @card.transaction_actual - diferencia
        #     end
        #     @card.floor_limit = @@floor_limit
        #     if @card.save
        #       @@a += 1
        #     end
        #   end
        # end

        ######## Inserta Cuentas Normales Termina
      end
    end

    @updateFileUpload = UploadFile.find(idFile)
    @updateFileUpload.end_upload_file = Time.now
    @updateFileUpload.save

    UploadFileMailer.alert_confirmation(upload_file, @@y, @@c, @@a).deliver ### BuenEnvio

  end

end