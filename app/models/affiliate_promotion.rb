class AffiliatePromotion < ActiveRecord::Base

  belongs_to :affiliation
  belongs_to :promotion

end
