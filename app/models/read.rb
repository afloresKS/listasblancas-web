require 'timeout'
require 'action_view'
include ActionView::Helpers::NumberHelper
class Read

  require 'roo'

  def self.process(ruta, id_user, idFile, nom_file)

    #xlsx = Roo::Spreadsheet.open(ruta)
    xlsx = Roo::Excelx.new(ruta)

    @@x = 1
    @@a = 0 # Varianble de Actualizacion de Cuentas
    @@c = 0 # Variable de Clientes Agregados
    @@y = 0 # Variables de Cuentas Agregadas

    @@registro_sin_cuenta = 0
    @@registro_dif_cardType = 0
    cuentas_cardType2 = 0
    cuentas_cardType3 = 0

    @@filas_cuentas = Array.new
    @@filas_tipoCuenta = Array.new

    xlsx.each() do |hash|
      @@x = @@x.to_i + 1
      total = hash.inspect
      @@trasaction_initial = 0
      @@name_account = xlsx.cell(@@x, 'A') # nombre del cliente
      @@account = xlsx.cell(@@x, 'B').to_i # cuenta
      @@cvv = xlsx.cell(@@x, 'C').to_i # Banco
      @@card_type = xlsx.cell(@@x, 'D').to_i # Tipo de Tarjeta
      @@floor_limit = xlsx.cell(@@x, 'E').to_f * 100 # Limite de Piso
      @@initial_balance = xlsx.cell(@@x, 'F').to_f * 100 # Limite de Credito
      @@trasaction_initial = xlsx.cell(@@x, 'G').to_i # Limite de Operaciones
      @@floor_limit = @@floor_limit.to_i
      if (@@account != 0) && ((@@card_type == 2) || (@@card_type == 12) || (@@card_type == 6)) # Si el registro no cuenta con una Cuenta
        @@cvv = @@cvv.to_s.rjust(3, '0') # Reajusto el CVV llenando a la izqierda con 0
        if @@card_type == 2 # Tipo de tarjeta 2 = 1
          @@card_type = 1
        elsif @@card_type == 12 # Tipo de tarjeta 12 = 2
          @@card_type = 2
        elsif @@card_type == 6 # Tipo de tarjeta 12 = 2
          @@card_type = 3
        end
        ######## Inserta Cuentas Normales Inicia
        @card = Card.find_or_initialize_by(number_card: @@account, cvv: @@cvv)
        if @card.client_id == nil
          @client = Client.new
          @client.name = @@name_account
          if @client.save
            @@c += 1
            @card.number_card = @@account
            @card.client_id = @client.id
            @card.cvv = @@cvv
            @card.cardType_id = @@card_type
            @card.floor_limit = @@floor_limit
            @card.initial_balance = @@initial_balance
            @card.transaction_initial = @@trasaction_initial
            @card.active = 1
            @card.actual_balance = @card.initial_balance
            @card.transaction_actual = @@trasaction_initial
            @card.save
            @@y += 1
          end
        else
          if @card.cardType_id == @@card_type
            # Inicia ActualizacionDeSaldos
            if @@initial_balance > @card.initial_balance
              diferencia = @@initial_balance - @card.initial_balance
              @card.actual_balance = @card.actual_balance + diferencia
              @card.initial_balance = @@initial_balance
            else
              if @@initial_balance < @card.actual_balance
                diferencia = @card.initial_balance - @@initial_balance
                @card.actual_balance = @card.actual_balance - diferencia
                if @card.actual_balance < 0
                  @card.actual_balance = 0
                end
                @card.initial_balance = @@initial_balance
              end
            end
            if @@trasaction_initial > @card.transaction_initial
              diferencia = @@trasaction_initial - @card.transaction_initial
              @card.transaction_actual = @card.transaction_actual + diferencia
              @card.transaction_initial = @@trasaction_initial
            else
              if @@trasaction_initial < @card.transaction_actual
                diferencia = @card.transaction_initial - @@trasaction_initial
                @card.transaction_actual = @card.transaction_actual - diferencia
                if @card.transaction_actual < 0
                  @card.transaction_actual = 0
                end
                @card.transaction_initial = @@trasaction_initial
              end
            end
            @card.floor_limit = @@floor_limit
            if @card.save
              @@a += 1
            end
            # Termina ActualizacionSaldos
          else
            ##### Inicia Agrego CardType == 2
            @card2 = Card.find_or_initialize_by(number_card: @@account, cardType_id: @@card_type, cvv: @@cvv)
            if @card2.client_id == nil
              @card2.client_id = @card.client_id
              @card2.actual_balance = @@initial_balance
              @card2.transaction_actual = @@trasaction_initial
            end
            @card2.cardType_id = @@card_type
            @card2.cvv = @@cvv
            @card2.floor_limit = @@floor_limit
            @card2.initial_balance = @@initial_balance
            @card2.transaction_initial = @@trasaction_initial
            @card2.active = 1

            if @card2.cardType_id == 2
              if @card2.save
                cuentas_cardType2 += 1
              end
            elsif @card2.cardType_id == 3
              if @card2.save
                cuentas_cardType3 += 1
              end
            end
          end
        end
        ######## Inserta Cuentas Normales Termina
      else
        if (@@account == 0) && (@@initial_balance != 0)
          @@registro_sin_cuenta += 1
          fila = @@x
          @@filas_cuentas << fila
        elsif ((@@card_type != 2) || (@@card_type != 12)) && (@@initial_balance != 0)
          @@registro_dif_cardType += 1
          fila = @@x
          @@filas_tipoCuenta << fila
        end
      end
    end
    # if idFile != 0
    #   @updateFileUpload = UploadFile.find(idFile)
    #   @updateFileUpload.end_upload_file = Time.now
    #   @updateFileUpload.save
    # end


    UploadFileMailer.alert_confirmation(nom_file, id_user, @@y, @@c, @@a, cuentas_cardType2, @@registro_sin_cuenta, @@registro_dif_cardType, @@filas_cuentas, @@filas_tipoCuenta, cuentas_cardType3).deliver ### BuenEnvio


  end
end