
class Codigos

  #response = HTTParty.get('https://api-codigos-postales.herokuapp.com/v2/codigo_postal/54710')
  #puts response.body

  include HTTParty
  base_uri 'api-codigos-postales.herokuapp.com'

  def initialize(codigo)
    @option = codigo
  end

  def find
    self.class.get("/v2/codigo_postal/"+@option.to_s)
  end

end