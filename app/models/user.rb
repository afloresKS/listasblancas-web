class User < ApplicationRecord
  belongs_to :profile, foreign_key: :profile_id
  belongs_to :area, foreign_key: :area_id
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :secure_validatable, :timeoutable,
         :password_expirable, :password_archivable , :confirmable, :lockable, :expirable , :authentication_keys => [:login]

  validates :user_name, :presence => true, :uniqueness => { :case_sensitive => false }

  attr_accessor :login

  def login=(login)
    @login = login
  end

  def login
    @login || self.user_name || self.email
  end

  #def current_user
  #@current_user ||= User.find(session[:id])
  #end

  def self.find_for_database_authentication(warden_conditions)
puts "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    conditions = warden_conditions.dup

    if login = conditions.delete(:login)

      #where(conditions.to_h).where(["lower(user_name) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      where(conditions).where(["user_name = :value OR lower(email) = lower(:value)", { :value => login }]).first

    else

      where(conditions.to_h).first

    end

  end
end
