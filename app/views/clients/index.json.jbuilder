json.array!(@clients) do |client|
  json.extract! client, :id, :last_name, :name, :email, :sex, :street, :city, :town,:state, :cp, :birthday ,:last_at, :account
  json.url client_url(client, format: :json)
end
