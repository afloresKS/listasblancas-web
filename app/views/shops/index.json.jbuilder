json.array!(@shops) do |shop|
  json.extract! shop, :id, :name, :port, :contract
  json.url shop_url(shop, format: :json)
end
