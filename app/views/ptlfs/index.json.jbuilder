json.array!(@ptlfs) do |ptlf|
  json.extract! ptlf, :id, :card_id, :transacctionType_id, :name, :amount, :amount2, :approval, :response_code, :affiliation_id, :address,:deferral, :partial, :plan_id, :promotion_id
  json.url ptlf_url(ptlf, format: :json)
end
