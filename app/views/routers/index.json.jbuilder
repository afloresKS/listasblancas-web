json.array!(@routers) do |router|
  json.extract! router, :id, :name, :ip, :port
  json.url router_url(router, format: :json)
end
