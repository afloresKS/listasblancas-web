json.array!(@bins) do |bin|
  json.extract! bin, :id, :number, :fiid, :count, :amount
  json.url bin_url(bin, format: :json)
end
