json.array!(@switches) do |switch|
  json.extract! switch, :id, :bin, :router
  json.url switch_url(switch, format: :json)
end
