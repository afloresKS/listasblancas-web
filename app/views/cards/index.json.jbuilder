json.array!(@cards) do |card|
  json.extract! card, :id, :client_id, :number_card, :cardType_id, :initial_balance, :actual_balance, :points, :currency_id, :expiration, :last_at, :cvv, :floor_limit
  json.url card_url(card, format: :json)
end
