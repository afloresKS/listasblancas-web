//>>>>>>>>>>>>>>>>>>>>>>> JQuery <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$(document).ready(function(){
//********************** Data Mask ***************************
$("#phon").mask("(99) 9999-9999");
$("#exts").mask("9999");
});

window.onload = function(){
  let newTraDan = document.getElementById('dan'),
      newTraWar = document.getElementById('war');
  //**************** Declaracion DOM ***************************
  let newForReg = document.getElementById('regi'),
      newTexNam = document.getElementById('name'),
      newTexApe = document.getElementById('last'),
      newTexMai = document.getElementById('mail'),
      newTexJef = document.getElementById('boss'),
      newTexPue = document.getElementById('empl'),
      newTexPho = document.getElementById('phon'),
      newSelPro = document.getElementById('prof'),
      newSelAre = document.getElementById('area'),
  //**************** Labels ***********************************
      newLabNam = document.getElementById('namelab'),
      newLabApe = document.getElementById('lastlab'),
      newLabJef = document.getElementById('bosslab'),
      newLabPue = document.getElementById('empllab'),
      newLabPho = document.getElementById('phonlab'),
      newLabPro = document.getElementById('proflab'),
      newTitPro = document.getElementById('titprof'),
      newLabAre = document.getElementById('arealab');

  //**************** Expresiones Regulares **********************

  var rexLett = /[^a-zA-Z]/g,
      rexNumb = /[^0-9]/g;

  //*********************** Estilos ****************************

  var dan = ['red','mistyrose',newTraDan.value,'danger'],
      war = ['orange','moccasin',newTraWar.value,'warning'];

  //>>>>>>>>>>>>>>>>>>>>>>> Validaciones de Campos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  //********************* Telefono **************************
  newTexPho.addEventListener('input', function() {
    this.value = this.value.replace (rexNumb,'');
  });
  newTexPho.addEventListener('focusout', function() {
    let vr = this.value.replace (rexNumb,'');
    vr.length < 10 && (this.value = "");
  });

  newTexPho.addEventListener('focusin',function(){
    newTexPho.removeAttribute('style');
    newLabPho.style.display="none";
  });

  //****************** Select Area *************************
  newSelAre.addEventListener('change',function(){
    newSelAre.removeAttribute('style');
    newLabAre.style.display="none";
  });
  //****************** Select Profile *************************
  newSelPro.addEventListener('change',function(){
    newSelPro.removeAttribute('style');
    newLabPro.style.display="none";
  });

  //********** Usuario, Nombre & Apellido *****************
  newTexNam.addEventListener('input',function(){valRange(newTexNam,newLabNam,0,255,1);this.value = this.value.replace (rexLett,'');});
  newTexApe.addEventListener('input',function(){valRange(newTexApe,newLabApe,0,255,1);this.value = this.value.replace (rexLett,'');});

  //******************* Jefe & Puesto  ********************
  newTexJef.addEventListener('input',function(){valRange(newTexJef,newLabJef,0,255,1);this.value = this.value.replace (rexLett,'');});
  newTexPue.addEventListener('input',function(){valRange(newTexPue,newLabPue,0,255,1);this.value = this.value.replace (rexLett,'');});

  //<<<<<<<<<<<<<<<<<<<<<<< Validaciones de Campos <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  //************************ Funcion de Rango ************************************
  function valRange (id,lab,min,max,typ){
    id.maxlength = max;
    let x = id.value.length
    typ == 1 && x > max || typ==2 && x < min ?
    (id.value="", messagex(id,lab,dan[0],dan[1],dan[2],dan[3])) :
    (lab.style.display="none", id.removeAttribute("style"));
  }
  //****************************** Validacion Sumbit *****************************
  newForReg.addEventListener('submit', function(e){
    newTexNam.value.trim()=='' && (e.preventDefault(),messagex(newTexNam,newLabNam,dan[0],dan[1],dan[2],dan[3]));
    newTexApe.value.trim()=='' && (e.preventDefault(),messagex(newTexApe,newLabApe,dan[0],dan[1],dan[2],dan[3]));
    newTexJef.value.trim()=='' && (e.preventDefault(),messagex(newTexJef,newLabJef,dan[0],dan[1],dan[2],dan[3]));
    newTexPue.value.trim()=='' && (e.preventDefault(),messagex(newTexPue,newLabPue,dan[0],dan[1],dan[2],dan[3]));
    newTexPho.value.trim()=='' && (e.preventDefault(),messagex(newTexPho,newLabPho,dan[0],dan[1],dan[2],dan[3]));
    newSelAre.value == 0 && (e.preventDefault(),messagex(newSelAre,newLabAre,dan[0],dan[1],dan[2],dan[3]));
    newSelPro.value == 0 && (e.preventDefault(),messagex(newSelPro,newLabPro,dan[0],dan[1],dan[2],dan[3]));
  });
  //********************* Funcion de Etiquetas ***********************************
  function messagex(id,lab,x1,x2,msg,typ){
    id.style.backgroundColor = x2;
    id.style.borderColor = x1;
    lab.style.display = "block";
    lab.style.width = "100%";
    lab.style.padding = 0;
    lab.style.margin = 0;
    lab.setAttribute("class","label label-" + typ);
    lab.textContent = msg;
  }
  //***************************** Ajax Select ************************************
    $("#area").change(function () {
      let id_area = this.value;
      $('.option_dynamic').remove();
      $.ajax({
        type: "POST",
        beforeSend: function(xhr){
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: '../consult_profiles',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
          area_id: id_area
        }),
        success: function(data){
          if ( data.length > 0  ){
            for ( const d of data){
              if (d.flag != 2){
                let option = document.createElement("OPTION");
                option.setAttribute('value',`${d.id}`);
                option.setAttribute('class','option_dynamic')
                option.innerHTML = `${d.name}`;
                newSelPro.appendChild(option)
              }
            }
            newSelPro.style.display = 'block';
            newTitPro.style.display = 'block';
          } else {
            newSelPro.style.display = 'none';
            newTitPro.style.display = 'none';
          }

        }

      });

    });

}
