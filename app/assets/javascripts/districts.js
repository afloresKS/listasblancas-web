$(document).ready(function () {
    var trad03 = $("#trad03").val();

    //Validacin para que los inputs no acepten NÚMEROS
    $('#name').on('input', function () {
        valor = $(this).val();
        patron = /^[ a-zA-Záéíóúüñ]*$/
        remp = /[^ a-zA-Záéíóúüñ]/gi
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });

    $("#form1").submit(function (e) {
        var name = $("#name").val().length;
        if (name == 0 || name < 2) {
            $("#name").val('');
            camponecesarioname();
            $('#namelabel').show();
            $('#namelabel').text(trad03);
            e.preventDefault();
        }
    });

    //Validacion para el campo name

    $('#name').focusout(function () {
        var name = $('#name').val().length;
        if (name < 2) {
            $(this).val('');
        } else {
            $('#namelabel').hide();
        }
    });
    $('#name').focusin(function () {
        $('#namelabel').hide();
    });

    function camponecesarioname() {
        $("#namelabel").addClass('label-danger');
    }

});