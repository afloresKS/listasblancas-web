//>>>>>>>>>>>>>>>>>>>>>>> JQuery <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$(document).ready(function(){
  let ext = $("#exts"),
      phn = $("#phon"),
      dat = $('.date'),
      birth = $('#birt');

//********************** Data Mask ***************************
    ext.mask("9999");
    phn.mask("(99) 9999-9999");
    birth.mask("99/99/9999")

//********************** Date Picker *************************
    dat.datepicker({
    format: 'dd/mm/yyyy',
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
  });
});

//>>>>>>>>>>>>>>>>>>>>>> Vanilla JS <<<<<<<<<<<<<<<<<<<<<<<<<<

window.onload = function(){
//**************** Traducciones ***************************
let newTraDan = document.getElementById('dan'),
    newTraWar = document.getElementById('war');
//**************** Declaracion DOM ***************************

let newForReg = document.getElementById('regi'),
    newTexUsr = document.getElementById('user'),
    newTexNam = document.getElementById('name'),
    newTexApe = document.getElementById('last'),
    newTexMai = document.getElementById('mail'),
    newTexBir = document.getElementById('birt'),
    newTexJef = document.getElementById('boss'),
    newTexPue = document.getElementById('empl'),
    newTexPho = document.getElementById('phon'),
    newTexExt = document.getElementById('exts'),
    newSelAre = document.getElementById('area'),
    newPasPwd = document.getElementById('pass'),
    newPasPw2 = document.getElementById('conf'),
    newBtnPwd = document.getElementById('eye1'),
    newBtnPw2 = document.getElementById('eye2'),
//**************** Labels ***********************************
    newLabUsr = document.getElementById('userlab'),
    newLabNam = document.getElementById('namelab'),
    newLabApe = document.getElementById('lastlab'),
    newLabMai = document.getElementById('maillab'),
    newLabBir = document.getElementById('birtlab'),
    newLabJef = document.getElementById('bosslab'),
    newLabPue = document.getElementById('empllab'),
    newLabPho = document.getElementById('phonlab'),
    newLabExt = document.getElementById('extslab'),
    newLabAre = document.getElementById('arealab'),
    newLabPwd = document.getElementById('passlab'),
    newLabPw2 = document.getElementById('conflab');


//**************** Expresiones Regulares **********************

var rexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    rexMAll = /[^a-zA-Z0-9-_.@\b]/gi,
    rexLett = /[^a-zA-Z ]/g,
    rexNumb = /[^0-9]/g;

//*********************** Estilos ****************************

var dan = ['red','mistyrose',newTraDan.value,'danger'],
    war = ['orange','moccasin',newTraWar.value,'warning'];

//>>>>>>>>>>>>>>>>>>>>>>> Validaciones de Campos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//********************* Telefono **************************
newTexPho.addEventListener('input', function() {
  this.value = this.value.replace (rexNumb,'');
});
newTexPho.addEventListener('focusout', function() {
  let vr = this.value.replace (rexNumb,'');
  vr.length < 10 && (this.value = "");
});

newTexPho.addEventListener('focusin',function(){
  newTexPho.removeAttribute('style');
  newLabPho.style.display="none";
});

//************************ E-mail ************************
newTexMai.addEventListener('input',function(){valRange(newTexMai,newLabMai,0,255,1);this.value = this.value.replace (rexMAll,'');});
newTexMai.addEventListener('focusout', function() {!rexMail.test(this.value) && (this.value = '');this.value=this.value.toLowerCase();});

//****************** Fecha Nacimiento ********************
newTexBir.addEventListener('focusin', function(e) {
  newTexBir.removeAttribute('style');
  newLabBir.style.display="none";
});

newTexBir.addEventListener('focusout', function() {
  let vr = this.value.replace (rexNumb,'');
  vr.length < 8 && (this.value = "");
  calculatedAge(newTexBir,newTexBir.value);
});

//****************** Select Area *************************
newSelAre.addEventListener('change',function(){
  newSelAre.removeAttribute('style');
  newLabAre.style.display="none";
});

//***************** Password & Confirmacion *************
newPasPw2.addEventListener('input',function(){valRange(newPasPw2,newLabPw2,0,255,1);});
newPasPwd.addEventListener('input',function(){valRange(newPasPwd,newLabPwd,0,255,1);});

//********** Usuario, Nombre & Apellido *****************
newTexUsr.addEventListener('input',function(){valRange(newTexUsr,newLabUsr,0,255,1);});
newTexNam.addEventListener('input',function(){valRange(newTexNam,newLabNam,0,255,1);this.value = this.value.replace (rexLett,'');});
newTexApe.addEventListener('input',function(){valRange(newTexApe,newLabApe,0,255,1);this.value = this.value.replace (rexLett,'');});
newTexNam.addEventListener('focusout',function(){this.value = this.value.charAt(0).toUpperCase()+ this.value.slice(1).toLowerCase();})
newTexApe.addEventListener('focusout',function(){
  var apes = this.value.toLowerCase().split(' ');
  for (var i = 0; i < apes.length; i++) {
    apes[i] = apes[i].charAt(0).toUpperCase() + apes[i].slice(1);
  }
  this.value = apes.join(' ');
})

//******************* Jefe & Puesto  ********************
newTexJef.addEventListener('input',function(){valRange(newTexJef,newLabJef,0,255,1);this.value = this.value.replace (rexLett,'');});
newTexPue.addEventListener('input',function(){valRange(newTexPue,newLabPue,0,255,1);this.value = this.value.replace (rexLett,'');});
newTexJef.addEventListener('focusout',function(){this.value = this.value.charAt(0).toUpperCase()+ this.value.slice(1);})
newTexPue.addEventListener('focusout',function(){this.value = this.value.charAt(0).toUpperCase()+ this.value.slice(1);})

//<<<<<<<<<<<<<<<<<<<<<<< Validaciones de Campos <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//************************ Funcion de Rango ************************************
function valRange (id,lab,min,max,typ){
  id.maxlength = max;
  let x = id.value.length
  typ == 1 && x > max || typ==2 && x < min ?
  (id.value="", messagex(id,lab,dan[0],dan[1],dan[2],dan[3])) :
  (lab.style.display="none", id.removeAttribute("style"));
}

//****************** Mostrar Password ******************************************
newBtnPwd.addEventListener('mouseover', function(e) {
  newPasPwd.setAttribute("type","text");
});
newBtnPw2.addEventListener('mouseover', function(e) {
  newPasPw2.setAttribute("type","text");
});
newBtnPwd.addEventListener('mouseout', function(e) {
  newPasPwd.setAttribute("type","password");
});
newBtnPw2.addEventListener('mouseout', function(e) {
  newPasPw2.setAttribute("type","password");
});

//****************************** Validacion Sumbit *****************************
newForReg.addEventListener('submit', function(e){

  calculatedAge(newTexBir,newTexBir.value);
  newTexUsr.value.trim()=='' && (e.preventDefault(),messagex(newTexUsr,newLabUsr,dan[0],dan[1],dan[2],dan[3]));
  newPasPwd.value.trim()=='' && (e.preventDefault(),messagex(newPasPwd,newLabPwd,dan[0],dan[1],dan[2],dan[3]));
  newTexNam.value.trim()=='' && (e.preventDefault(),messagex(newTexNam,newLabNam,dan[0],dan[1],dan[2],dan[3]));
  newTexApe.value.trim()=='' && (e.preventDefault(),messagex(newTexApe,newLabApe,dan[0],dan[1],dan[2],dan[3]));
  newTexMai.value.trim()=='' && (e.preventDefault(),messagex(newTexMai,newLabMai,dan[0],dan[1],dan[2],dan[3]));
  newTexBir.value.trim()=='' && (e.preventDefault(),messagex(newTexBir,newLabBir,dan[0],dan[1],dan[2],dan[3]));
  newTexJef.value.trim()=='' && (e.preventDefault(),messagex(newTexJef,newLabJef,dan[0],dan[1],dan[2],dan[3]));
  newTexPue.value.trim()=='' && (e.preventDefault(),messagex(newTexPue,newLabPue,dan[0],dan[1],dan[2],dan[3]));
  newTexPho.value.trim()=='' && (e.preventDefault(),messagex(newTexPho,newLabPho,dan[0],dan[1],dan[2],dan[3]));
  // newTexExt.value.trim()=='' && (e.preventDefault(),messagex(newTexExt,newLabExt,dan[0],dan[1],dan[2],dan[3]));
  newPasPw2.value.trim()=='' && (e.preventDefault(),messagex(newPasPw2,newLabPw2,dan[0],dan[1],dan[2],dan[3]));
  newSelAre.value == 0 && (e.preventDefault(),messagex(newSelAre,newLabAre,dan[0],dan[1],dan[2],dan[3]));
});

//********************* Funcion de Etiquetas ***********************************
function messagex(id,lab,x1,x2,msg,typ){
  id.style.backgroundColor = x2;
  id.style.borderColor = x1;
  lab.style.display = "block";
  lab.setAttribute("class","label label-" + typ);
  lab.textContent = msg;
}

//***************************** Funcion Edad **********************************
//#calcular si eres mayor de edad
function calculatedAge(id,birth){
  let arry = birth.split("/");
  let birt = arry[1] + "/" + arry[0] + "/" + arry[2];
  let vr = new Date(Date.now() - new Date(birt).getTime());
  let age = Math.abs(vr.getUTCFullYear()- 1970 );
  if (age < 18 || age > 80 || Number.isNaN(age)){id.value = '';}
}

}//<<<<<<<<<<<<<<<<<<<<<<< Window.onload <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
