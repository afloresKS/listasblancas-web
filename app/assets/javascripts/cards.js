$(document).ready(function () {
    $(function () {
        $('#credito').maskMoney({prefix: 'MEX $'});
        $('#floor').maskMoney({prefix: 'MEX $'});
    });
    let newTraDan = document.getElementById('dan'),
        newTraWar = document.getElementById('war'),
        newTextAccount = document.getElementById('cuenta'),
        newTextCredito = document.getElementById('credito'),
        newTextPiso = document.getElementById('floor'),
        newSelectType = document.getElementById('cardType'),
        newselectbank = document.getElementById('Banco'),
        newTextOper = document.getElementById('limite'),

        newLabAccount = document.getElementById('accountlabel'),
        newLabCredito = document.getElementById('creditlabel'),
        newLabPiso = document.getElementById('floorlabel'),
        newLabType = document.getElementById('cardtypelabel'),
        newLabbank = document.getElementById('banklabel'),
        newLabOper = document.getElementById('limitelabel'),

        newBtnCreate1 = document.getElementById('commit1'),
        newBtnCreate2 = document.getElementById('commit2'),
        dan = ['red', 'mistyrose', newTraDan.value, 'danger'],
        war = ['orange', 'moccasin', newTraWar.value, 'warning'];

    newTextAccount.addEventListener('focusin', function () {
        newTextAccount.removeAttribute('style');
        newLabAccount.style.display = "none";
    });
    newTextCredito.addEventListener('focusin', function () {
        newTextCredito.removeAttribute('style');
        newLabCredito.style.display = "none";
    });
    newTextPiso.addEventListener('focusin', function () {
        newTextPiso.removeAttribute('style');
        newLabPiso.style.display = "none";
    });
    newSelectType.addEventListener('focusin', function () {
        newSelectType.removeAttribute('style');
        newLabType.style.display = "none";
    });
    newselectbank.addEventListener('focusin', function () {
        newselectbank.removeAttribute('style');
        newLabbank.style.display = "none";
    });
    newTextOper.addEventListener('focusin', function () {
        newTextOper.removeAttribute('style');
        newLabOper.style.display = "none";
    });

    newBtnCreate1.addEventListener('click', function (e) {
        newTextAccount.value.trim() == '' && (e.preventDefault(), messagex(newTextAccount, newLabAccount, dan[0], dan[1], dan[2], dan[3]));
        newTextAccount.value.trim().length < 6 && newTextAccount.value.trim().length >= 1 && (e.preventDefault(), messagex(newTextAccount, newLabAccount, war[0], war[1], war[2], war[3]));
        newTextCredito.value.trim() == '' && (e.preventDefault(), messagex(newTextCredito, newLabCredito, dan[0], dan[1], dan[2], dan[3]));
        newTextPiso.value.trim() == '' && (e.preventDefault(), messagex(newTextPiso, newLabPiso, dan[0], dan[1], dan[2], dan[3]));
        newTextOper.value.trim() == '' && (e.preventDefault(), messagex(newTextOper, newLabOper, dan[0], dan[1], dan[2], dan[3]));
        newSelectType.selectedIndex == 0 && (e.preventDefault(), messagex(newSelectType, newLabType, dan[0], dan[1], dan[2], dan[3]));
        newselectbank.selectedIndex == 0 && (e.preventDefault(), messagex(newselectbank, newLabbank, dan[0], dan[1], dan[2], dan[3]));
    });
    newBtnCreate2.addEventListener('click', function (e) {
        newTextAccount.value.trim() == '' && (e.preventDefault(), messagex(newTextAccount, newLabAccount, dan[0], dan[1], dan[2], dan[3]));
        newTextAccount.value.trim().length < 6 && newTextAccount.value.trim().length >= 1 && (e.preventDefault(), messagex(newTextAccount, newLabAccount, war[0], war[1], war[2], war[3]));
        newTextCredito.value.trim() == '' && (e.preventDefault(), messagex(newTextCredito, newLabCredito, dan[0], dan[1], dan[2], dan[3]));
        newTextPiso.value.trim() == '' && (e.preventDefault(), messagex(newTextPiso, newLabPiso, dan[0], dan[1], dan[2], dan[3]));
        newTextOper.value.trim() == '' && (e.preventDefault(), messagex(newTextOper, newLabOper, dan[0], dan[1], dan[2], dan[3]));
        newSelectType.selectedIndex == 0 && (e.preventDefault(), messagex(newSelectType, newLabType, dan[0], dan[1], dan[2], dan[3]));
        newselectbank.selectedIndex == 0 && (e.preventDefault(), messagex(newselectbank, newLabbank, dan[0], dan[1], dan[2], dan[3]));
    });

    $('#form1').submit(function (e) {
        let credito = $("#credito").val(),
            floor = $("#floor").val(),
            c = /,/g,
            f = /,/g,
            act = $('#act').prop("checked");
        credito = credito.replace('MEX', '').replace('$', '').replace('.', '').replace(' ', '').replace(c, '');
        floor = floor.replace('MEX', '').replace('$', '').replace('.', '').replace(' ', '').replace(f, '');
        $('#acti').val(act);
        $('#credito_int').val(credito);
        $('#floor_limit').val(floor);
        $('#credito').val(credito);
        $('#floor').val(floor);
    });

    $('[data-toggle="tooltip"]').tooltip();
    // mascara de los campos credito y piso

    // Validacin para que los inputs no acepten Letras
    $('#cuenta, #credito_int, #floor_limit, #limite').on('input', function () {
        valor = $(this).val();
        patron = /^[0-9]*$/;
        remp = /[^0-9]/gi;
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });

    $("#floor").keyup(function () {
        let credito = $("#credito").val(),
            floor = $("#floor").val(),
            c = /,/g,
            f = /,/g;
        credito = credito.replace('MEX', '').replace('$', '').replace('.', '').replace(' ', '').replace(c, '');
        credito = parseInt(credito);
        floor = floor.replace('MEX', '').replace('$', '').replace('.', '').replace(' ', '').replace(f, '');
        floor = parseInt(floor);
        if (floor > credito) {
            $(this).val('');
        }
    });
    $("#floor").focusin(function () {
        let credito = $("#credito").val(),
            floor = $("#floor").val(),
            c = /,/g,
            f = /,/g;
        credito = credito.replace('MEX', '').replace('$', '').replace('.', '').replace(' ', '').replace(c, '');
        credito = parseInt(credito);
        floor = floor.replace('MEX', '').replace('$', '').replace('.', '').replace(' ', '').replace(f, '');
        floor = parseInt(floor);
        if (floor > credito) {
            $(this).val('');
        }
    });
});

