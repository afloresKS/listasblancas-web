$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    let trad3 = $("#trad3").val(),
        newTraDan = document.getElementById('dan'),
        newTraWar = document.getElementById('war'),
        newTexNam = document.getElementById('name'),
        newLabNam = document.getElementById('namelabel'),
        newTexAccount = document.getElementById('account'),
        newlabAcount = document.getElementById('accountlabel'),
        newTexCp = document.getElementById('codigo'),
        newlabCp = document.getElementById('cplabel'),
        newBtnClient = document.getElementById('add_client'),
        newBtnCliCa = document.getElementById('client_card'),
        newBtnClient2 = document.getElementById('add_client2'),
        newBtnCliCa2 = document.getElementById('client_card2'),
        dan = ['red', 'mistyrose', newTraDan.value, 'danger'],
        war = ['orange', 'moccasin', newTraWar.value, 'warning'],
        idioma = $("#idioma_app").val();

    newTexNam.addEventListener('focusin', function () {
        newTexNam.removeAttribute('style');
        newLabNam.style.display = "none";
    });
    newTexAccount.addEventListener('focusin', function () {
        newTexAccount.removeAttribute('style');
        newlabAcount.style.display = "none";
    });
    newTexCp.addEventListener('focusin', function () {
        newTexCp.removeAttribute('style');
        newlabCp.style.display = "none";
    });

    newBtnClient.addEventListener('click', function (e) {
        newTexNam.value.trim() == '' && (e.preventDefault(), messagex(newTexNam, newLabNam, dan[0], dan[1], dan[2], dan[3]));
        newTexAccount.value.trim().length < 16 && newTexAccount.value.trim().length > 0 && (e.preventDefault(), messagex(newTexAccount, newlabAcount, war[0], war[1], war[2], war[3]));
        newTexCp.value.trim().length < 5 && newTexCp.value.trim().length > 0 && (e.preventDefault(), messagex(newTexCp, newlabCp, war[0], war[1], war[2], war[3]));
    });
    newBtnCliCa.addEventListener('click', function (e) {
        newTexNam.value.trim() == '' && (e.preventDefault(), messagex(newTexNam, newLabNam, dan[0], dan[1], dan[2], dan[3]));
        newTexAccount.value.trim().length < 16 && newTexAccount.value.trim().length > 0 && (e.preventDefault(), messagex(newTexAccount, newlabAcount, war[0], war[1], war[2], war[3]));
        newTexCp.value.trim().length < 5 && newTexCp.value.trim().length > 0 && (e.preventDefault(), messagex(newTexCp, newlabCp, war[0], war[1], war[2], war[3]));
    });
    newBtnClient2.addEventListener('click', function (e) {
        newTexNam.value.trim() == '' && (e.preventDefault(), messagex(newTexNam, newLabNam, dan[0], dan[1], dan[2], dan[3]));
        newTexAccount.value.trim().length < 16 && newTexAccount.value.trim().length > 0 && (e.preventDefault(), messagex(newTexAccount, newlabAcount, war[0], war[1], war[2], war[3]));
        newTexCp.value.trim().length < 5 && newTexCp.value.trim().length > 0 && (e.preventDefault(), messagex(newTexCp, newlabCp, war[0], war[1], war[2], war[3]));
    });
    newBtnCliCa2.addEventListener('click', function (e) {
        newTexNam.value.trim() == '' && (e.preventDefault(), messagex(newTexNam, newLabNam, dan[0], dan[1], dan[2], dan[3]));
        newTexAccount.value.trim().length < 16 && newTexAccount.value.trim().length > 0 && (e.preventDefault(), messagex(newTexAccount, newlabAcount, war[0], war[1], war[2], war[3]));
        newTexCp.value.trim().length < 5 && newTexCp.value.trim().length > 0 && (e.preventDefault(), messagex(newTexCp, newlabCp, war[0], war[1], war[2], war[3]));
    });


    $('#client_birthday').mask('99/99/9999');

    $('#dato_1 .input-group.date').datepicker({
        format: 'dd/mm/yyyy',
        language: `${idioma}`,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        endDate: '+0d'
    });


//Validacin para que los inputs no acepten NÚMEROS
    $('#last_name, #state, #estado, #estado1, #town, #town1, #town2, #town3, #city').on('input', function () {
        valor = $(this).val();
        patron = /^[ a-zA-Záéíóúüñ]*$/;
        remp = /[^ a-zA-Záéíóúüñ]/gi;
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });

// Validacin para que los inputs no acepten Letras
    $('#account, #codigo').on('input', function () {
        valor = $(this).val();
        patron = /^[0-9]*$/;
        remp = /[^0-9]/gi;
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });

    $('#client_birthday').change(function () {
        var valorbirth = $(this).val(),
            values = valorbirth.split("/"),
            ano = values[2],
            mes = values[1],
            dia = values[0],
            fecha_hoy = new Date(),
            ano_hoy = fecha_hoy.getFullYear(),
            mes_hoy = fecha_hoy.getMonth() + 1,
            dia_hoy = fecha_hoy.getDate();

        if (valorbirth != "") {
            if ((mes < 13) && (dia < 32)) {
                if ((mes == 1) && (mes == 3) && (mes == 5) && (mes == 7) && (mes == 8) && (mes == 10) && (mes == 12)) {
                    if (dia <= 31) {
                        // $('#errorBirth').text('');
                    } else {
                        $('#client_birthday').val('');
                        return true;
                    }
                }
                if ((mes == 4) && (mes == 6) && (mes == 9) && (mes == 11)) {
                    if (dia <= 30) {
                        // $('#errorBirth').text('');
                    } else {
                        $('#client_birthday').val('');
                        return true;
                    }
                }

                if (mes == 2) {

                    if ((ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0))) {
                        if (dia <= 29) {
                            // $('#errorBirth').text('');
                        } else {
                            $('#client_birthday').val('');
                            return true;
                        }
                    } else {
                        if (dia <= 28) {
                            // $('#errorBirth').text('');
                        } else {
                            $('#client_birthday').val('');
                            return true;
                        }
                    }
                }
            } else {
                $('#client_birthday').val('');
                return true;
            }
        }

        var edad = (ano_hoy - ano);


        if (mes_hoy < mes) {
            edad--;
        } else if ((mes_hoy == mes) && (dia_hoy < dia)) {
            edad--;
        } else if ((mes == mes_hoy) && (dia_hoy == dia) && (ano_hoy == ano)) {
            edad = 0;
        } else {
            edad = edad;
        }

        if (edad < 18) {
            $("#client_birthday").val("").css({'background-color':'mistyrose', 'border-color':'red'});
            $('#edadLab').removeClass('hide');
            // alert('Menor')
        }

        if (edad > 100) {
            $("#client_birthday").val("").css({'background-color':'mistyrose', 'border-color':'red'});
            $('#edadLab').removeClass('hide');
            // alert('Menor')
        }

        if ((edad > 18) && (edad < 100)) {
            $("#client_birthday").css({'background-color':'', 'border-color':''});
            $('#edadLab').addClass('hide');
        }
    });


//---- Validación de email
    $('#mail').change(function () {
        var em = $(this).val(),
            re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (re.test(em)) {
            $("#mail").val(em);
            $("#errorMail").hide();
        } else {
            var msg = trad3;
            document.getElementById('errorMail').innerHTML = msg;
            $('#errorMail').show();
            $('#errorMail').addClass('label-danger');
            $("#mail").val("");
            // $("#mail").focus();

        }
    });
    $('#mail').on('input', function () {
        $(this).val($(this).val().replace(/[^a-zA-Z0-9-_.@\b]/gi, ''));
    });
    $('#mail').focusin(function () {
        $("#errorMail").hide();
    });

});