window.onload = function(){
  let newTraDan = document.getElementById('dan'),
      newTraWar = document.getElementById('war');
//****************************** Declaraciones DOM ****************************
let newBtnEye = document.getElementById('eye'),
    newPasPwd = document.getElementById('pwd'),
    newBtnAct = document.getElementById('act'),
    newBtnIni = document.getElementById('ini'),
    newTexUsr = document.getElementById('usr'),
    newLabPwd = document.getElementById('pwdlab'),
    newLabUsr = document.getElementById('usrlab'),
    newForUsr = document.getElementById('new_user');

//*********************************** Estilos *********************************
var dan = ['red','mistyrose',newTraDan.value,'danger'],
    war = ['orange','moccasin',newTraWar.value,'warning'];


//******************************* Mostrar Password ****************************
newBtnEye.addEventListener('mouseover', function(e) {
  newPasPwd.setAttribute("type","text");
});
newBtnEye.addEventListener('mouseout', function(e) {
  newPasPwd.setAttribute("type","password");
});

//**************************** Mostrar Boton de Activacion ************ //*NEFC*
try{
  let newTexMsg = document.getElementById('msj');
  if (newTexMsg.textContent.includes('Su cuenta ha expirado') || newTexMsg.textContent.includes('Your account has expired')) {
    newBtnAct.style.display = "block";
  }
}catch{};

//***************************** Validaciones de Campos *************************
newTexUsr.addEventListener('input',function(){
  newTexUsr.removeAttribute('style');
  newLabUsr.style.display="none";
});
newPasPwd.addEventListener('input',function(){
  newPasPwd.removeAttribute('style');
  newLabPwd.style.display="none";
});

 //*************************** Validaciones Submit *****************************
newForUsr.addEventListener('submit', function(e){
  newTexUsr.value.trim()=='' && (e.preventDefault(),messagex(newTexUsr,newLabUsr,dan[0],dan[1],dan[2],dan[3]));
  newPasPwd.value.trim()=='' && (e.preventDefault(),messagex(newPasPwd,newLabPwd,dan[0],dan[1],dan[2],dan[3]));
});

//**************************** Funcion de Etiquetas ****************************
function messagex(id,lab,x1,x2,msg,typ){
  id.style.backgroundColor = x2;
  id.style.borderColor = x1;
  lab.style.display = "block"
  lab.style.display="block"
  lab.setAttribute("class","label label-" + typ);
  lab.textContent = msg;
}

}//<<<<<<<<<<<<<<<<<<<<<<< Window.onload <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
