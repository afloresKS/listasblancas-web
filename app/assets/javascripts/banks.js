$(document).ready(function () {

    let newTraDan = document.getElementById('dan'),
        newTraWar = document.getElementById('war'),
        newTextName = document.getElementById('name'),
        newtextId = document.getElementById('bank_id'),

        newLabName = document.getElementById('namelabel'),
        newLabId = document.getElementById('idlabel'),

        newBtn1 = document.getElementById('bank1'),
        newBtn2 = document.getElementById('bank2'),
        dan = ['red', 'mistyrose', newTraDan.value, 'danger'],
        war = ['orange', 'moccasin', newTraWar.value, 'warning'];

    newTextName.addEventListener('focusin', function () {
        newTextName.removeAttribute('style');
        newLabName.style.display = "none";
    });

    newtextId.addEventListener('focusin', function () {
        newtextId.removeAttribute('style');
        newLabId.style.display = "none";
    });

    newBtn1.addEventListener('click', function (e) {
        newTextName.value.trim() == '' && (e.preventDefault(), messagex(newTextName, newLabName, dan[0], dan[1], dan[2], dan[3]));
        newtextId.value.trim() == '' && (e.preventDefault(), messagex(newtextId, newLabId, dan[0], dan[1], dan[2], dan[3]));
        newtextId.value.trim().length < 3 && newtextId.value.trim().length > 0 && (e.preventDefault(), messagex(newtextId, newLabId, war[0], war[1], war[2], war[3]));
    });

    newBtn2.addEventListener('click', function (e) {
        newTextName.value.trim() == '' && (e.preventDefault(), messagex(newTextName, newLabName, dan[0], dan[1], dan[2], dan[3]));
        newtextId.value.trim() == '' && (e.preventDefault(), messagex(newtextId, newLabId, dan[0], dan[1], dan[2], dan[3]));
        newtextId.value.trim().length < 3 && newtextId.value.trim().length > 0 && (e.preventDefault(), messagex(newtextId, newLabId, war[0], war[1], war[2], war[3]));
    });

    //Validacin para que los inputs no acepten NÚMEROS
    $('#name').on('input', function () {
        valor = $(this).val();
        patron = /^[ a-zA-Záéíóúüñ]*$/
        remp = /[^ a-zA-Záéíóúüñ]/gi
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });

    // Validacin para que los inputs no acepten Letras
    $('#bank_id').on('input', function () {
        valor = $(this).val();
        patron = /^[0-9]*$/
        remp = /[^0-9]/gi
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });
});