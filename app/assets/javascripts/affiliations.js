$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    let newTraDan = document.getElementById('dan'),
        newTraWar = document.getElementById('war'),
        newTextName = document.getElementById('name'),
        newTextNumero = document.getElementById('number'),
        newTextDireccion = document.getElementById('address'),
        newTextTienda = document.getElementById('affil'),
        newTextPuerto = document.getElementById('port'),
        newSelectedDist = document.getElementById('Dist'),
        newSelectedReg = document.getElementById('Reg'),

        newLabName = document.getElementById('namelabel'),
        newLabNumero = document.getElementById('numberlabel'),
        newLabDireccion = document.getElementById('addresslabel'),
        newLabTienda = document.getElementById('affilabel'),
        newLabReg = document.getElementById('regionlabel'),
        newLabDist = document.getElementById('districtlabel'),
        newLabPuerto = document.getElementById('port1'),

        newBtnCreate1 = document.getElementById('btnaffi1'),
        newBtnCreate2 = document.getElementById('btnaffi2'),
        dan = ['red', 'mistyrose', newTraDan.value, 'danger'],
        war = ['orange', 'moccasin', newTraWar.value, 'warning'];

    newTextName.addEventListener('focusin', function () {
        newTextName.removeAttribute('style');
        newLabName.style.display = "none";
    });

    newTextNumero.addEventListener('focusin', function () {
        newTextNumero.removeAttribute('style');
        newLabNumero.style.display = "none";
    });

    newTextDireccion.addEventListener('focusin', function () {
        newTextDireccion.removeAttribute('style');
        newLabDireccion.style.display = "none";
    });

    newTextTienda.addEventListener('focusin', function () {
        newTextTienda.removeAttribute('style');
        newLabTienda.style.display = "none";
    });

    newTextPuerto.addEventListener('focusin', function () {
        newTextPuerto.removeAttribute('style');
        newLabPuerto.style.display = "none";
    });

    newSelectedDist.addEventListener('focusin', function () {
        newSelectedDist.removeAttribute('style');
        newLabDist.style.display = "none";
    });

    newSelectedReg.addEventListener('focusin', function () {
        newSelectedReg.removeAttribute('style');
        newLabReg.style.display = "none";
    });

    newBtnCreate1.addEventListener('click', function (e) {
        newTextName.value.trim() == '' && (e.preventDefault(), messagex(newTextName, newLabName, dan[0], dan[1], dan[2], dan[3]));
        newTextNumero.value.trim() == '' && (e.preventDefault(), messagex(newTextNumero, newLabNumero, dan[0], dan[1], dan[2], dan[3]));
        newTextNumero.value.trim().length >= 1 && newTextNumero.value.trim().length < 4  && (e.preventDefault(), messagex(newTextNumero, newLabNumero, war[0], war[1], war[2], war[3]));
        newTextDireccion.value.trim() == '' && (e.preventDefault(), messagex(newTextDireccion, newLabDireccion, dan[0], dan[1], dan[2], dan[3]));
        newTextTienda.value.trim() == '' && (e.preventDefault(), messagex(newTextTienda, newLabTienda, dan[0], dan[1], dan[2], dan[3]));
        newTextTienda.value.trim().length >= 1 && newTextTienda.value.trim().length < 4  && (e.preventDefault(), messagex(newTextTienda, newLabTienda, war[0], war[1], war[2], war[3]));
        newSelectedDist.selectedIndex == 0 && (e.preventDefault(), messagex(newSelectedDist, newLabDist, dan[0], dan[1], dan[2], dan[3]));
        newSelectedReg.selectedIndex == 0 && (e.preventDefault(), messagex(newSelectedReg, newLabReg, dan[0], dan[1], dan[2], dan[3]));
        newTextPuerto.value.trim() == '' && (e.preventDefault(), messagex(newTextPuerto, newLabPuerto, dan[0], dan[1], dan[2], dan[3]));
        newTextPuerto.value.trim().length >= 1 && newTextPuerto.value.trim().length < 4  && (e.preventDefault(), messagex(newTextPuerto, newLabPuerto, war[0], war[1], war[2], war[3]));
    });

    newBtnCreate2.addEventListener('click', function (e) {
        newTextName.value.trim() == '' && (e.preventDefault(), messagex(newTextName, newLabName, dan[0], dan[1], dan[2], dan[3]));
        newTextNumero.value.trim() == '' && (e.preventDefault(), messagex(newTextNumero, newLabNumero, dan[0], dan[1], dan[2], dan[3]));
        newTextNumero.value.trim().length >= 1 && newTextNumero.value.trim().length < 4  && (e.preventDefault(), messagex(newTextNumero, newLabNumero, war[0], war[1], war[2], war[3]));
        newTextDireccion.value.trim() == '' && (e.preventDefault(), messagex(newTextDireccion, newLabDireccion, dan[0], dan[1], dan[2], dan[3]));
        newTextTienda.value.trim() == '' && (e.preventDefault(), messagex(newTextTienda, newLabTienda, dan[0], dan[1], dan[2], dan[3]));
        newTextTienda.value.trim().length >= 1 && newTextTienda.value.trim().length < 4  && (e.preventDefault(), messagex(newTextTienda, newLabTienda, war[0], war[1], war[2], war[3]));
        newSelectedDist.selectedIndex == 0 && (e.preventDefault(), messagex(newSelectedDist, newLabDist, dan[0], dan[1], dan[2], dan[3]));
        newSelectedReg.selectedIndex == 0 && (e.preventDefault(), messagex(newSelectedReg, newLabReg, dan[0], dan[1], dan[2], dan[3]));
        newTextPuerto.value.trim() == '' && (e.preventDefault(), messagex(newTextPuerto, newLabPuerto, dan[0], dan[1], dan[2], dan[3]));
        newTextPuerto.value.trim().length >= 1 && newTextPuerto.value.trim().length < 4  && (e.preventDefault(), messagex(newTextPuerto, newLabPuerto, war[0], war[1], war[2], war[3]));
    });

    //Validacin para que los inputs no acepten NÚMEROS
    $('#name, #sear').on('input', function () {
        valor = $(this).val();
        patron = /^[ a-zA-Záéíóúüñ]*$/;
        remp = /[^ a-zA-Záéíóúüñ]/gi;
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });

    //Validacin para address
    $('#address').on('input', function () {
        valor = $(this).val();
        patron = /^[ a-zA-Záéíóúüñ0-9#.,]*$/;
        remp = /[^ a-zA-Záéíóúüñ0-9#.,]/gi;
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });

    // Validacin para que los inputs no acepten Letras
    $('#number, #affil, #port').on('input', function () {
        valor = $(this).val();
        patron = /^[0-9]*$/;
        remp = /[^0-9]/gi;
        if (!patron.test(valor)) {
            this.value = valor.replace(remp, "");
        }
    });


});

