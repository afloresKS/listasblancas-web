$(document).on('ready', function(){

    //************* Footable y Tooltip
    $('table').footable({});
    $('[data-toggle="tooltip"]').tooltip();

    let name = $('#name'),
       account = $('#account');

    //**************** Expresiones Regulares **********************
    let rexNumb = /[^0-9]/g,
       rexNom = /[^a-zA-ZñÑáéíóúÁÉÍÓÚ ]/g;

    name.on('input', e => e.target.value = e.target.value.replace(rexNom,'')  );

    //********************* Campo cuenta *******************
    account.on('input', e => e.target.value = e.target.value.replace(rexNumb,'') );


    //*************************** Eliminar búsqueda y mostrar todos los resultados *****************************
    $(document).on('click', '#clean', function(){
        $('#name, #account').val('');
        $("#btnrt").click();
    });

    //*************************** Ajax Búsqueda *****************************
    $(document).on('click', '#btnrt', function() {
        $.get($('#name, #account').attr("action"), $('#name, #account').serialize(), null, "script");
        return false;
    })


});