window.onload = function(){
  let newTraDan = document.getElementById('dan'),
      newTraWar = document.getElementById('war');
  //**************** Declaracion DOM ***************************
  let newForReg = document.getElementById('regi'),
      newTexNam = document.getElementById('name'),
      newSelPro = document.getElementById('prof'),
      newSelAre = document.getElementById('area'),
  //**************** Labels ***********************************
      newLabNam = document.getElementById('namelab'),
      newLabPro = document.getElementById('proflab'),
      newLabAre = document.getElementById('arealab');

  //**************** Expresiones Regulares **********************

  var rexLett = /[^a-zA-Z ]/g;

  //*********************** Estilos ****************************

  var dan = ['red','mistyrose',newTraDan.value,'danger'],
      war = ['orange','moccasin',newTraWar.value,'warning'];

  //>>>>>>>>>>>>>>>>>>>>>>> Validaciones de Campos >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  //****************** Select Area *************************
  newSelAre.addEventListener('change',function(){
    newSelAre.removeAttribute('style');
    newLabAre.style.display="none";
  });
  //********************************** Nombre *********************************
  newTexNam.addEventListener('input',function(){valRange(newTexNam,newLabNam,0,255,1);this.value = this.value.replace (rexLett,'');});
  //<<<<<<<<<<<<<<<<<<<<<<< Validaciones de Campos <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  //************************ Funcion de Rango ************************************
  function valRange (id,lab,min,max,typ){
    id.maxlength = max;
    let x = id.value.length
    typ == 1 && x > max || typ==2 && x < min ?
    (id.value="", messagex(id,lab,dan[0],dan[1],dan[2],dan[3])) :
    (lab.style.display="none", id.removeAttribute("style"));
  }
  //****************************** Validacion Sumbit *****************************
  newForReg.addEventListener('submit', function(e){
    newTexNam.value.trim()=='' && (e.preventDefault(),messagex(newTexNam,newLabNam,dan[0],dan[1],dan[2],dan[3]));
    newSelAre.value == 0 && (e.preventDefault(),messagex(newSelAre,newLabAre,dan[0],dan[1],dan[2],dan[3]));
    if(newSelAre.value != 0){
      newSelPro.value == 0 && (e.preventDefault(),messagex(newSelPro,newLabPro,dan[0],dan[1],dan[2],dan[3]));
    }

  });
  //********************* Funcion de Etiquetas ***********************************
  function messagex(id,lab,x1,x2,msg,typ){
    id.style.backgroundColor = x2;
    id.style.borderColor = x1;
    lab.style.display = "block";
    lab.style.width = "100%";
    lab.style.padding = 0;
    lab.style.margin = 0;
    lab.setAttribute("class","label label-" + typ);
    lab.textContent = msg;
  }


}
