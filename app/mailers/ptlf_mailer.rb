class PtlfMailer < ApplicationMailer

  #default from: "Listas_Blancas@homedepot.com.mx"
  # default from: "aflores@kssoluciones.com.mx"
  default from: "#{APP_CONFIG[:email]}"

  def alert_confirmation(ptlf,save)

    @mensaje = save
    @user_name = ptlf.user.name
    @user_last_name = ptlf.user.last_name
    @fecha = Time.now.strftime("%d/%m/%Y  %H:%M:%S")
    @trnasaction_number = ptlf.id
    @account_number = ptlf.card.number_card
    @customer_name = ptlf.card.client.name
    @trans_type = ptlf.transacctionType.name

    @response = ptlf.response_code

    if @response == '02'
      @res = 'Approval'
    elsif @response == '13'
      @res = 'Invalid amount'
    elsif @response == '14'
      @res = 'Invalid account'
    elsif @response == '12'
      @res = 'Invalid transaction'
    elsif @response == '51'
      @res = 'Insufficient balance'
    elsif @response == '53'
      @res = 'Nonexistent account'
    elsif @response == '61'
      @res = 'Exceeds floor limit'
    elsif @response == '76'
      @res = 'Blocked account'
    elsif @response == '93'
      @res = 'Operation unavailable'
    else
      @res = 'Code no exist'
    end

    @affilia = ptlf.affiliation_id
    @amount =  ptlf.amount.to_f
    @approval = ptlf.approval
    @dev = ptlf.devolution

    if @dev == true
      @devu = 'Returned'
    elsif @dev == false
      @devu = 'Not returned'
    end
    
    @reco = ptlf.recovered

    if @reco == true
      @reco = "Recovered"
    elsif @reco == false
      @reco = "Unrecovered"
    else

    end

    @ret_bal = ptlf.returned_balance

    if @ret_bal == true
      @retun_bal = 'Cashed'
    elsif @ret_bal == false
      @retun_bal = 'In process'
    end

    @creat = ptlf.created_at.strftime("%d/%m/%Y  %H:%M:%S")
    mail to: ptlf.user.email,
         #cc: "listas_blancas_devoluciones@homedepot.com.mx",
         subject:  @mensaje

  end

end
