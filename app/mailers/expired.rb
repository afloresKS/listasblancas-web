class Expired < ApplicationMailer

  default from: "#{APP_CONFIG[:email]}"
  # default from: "aflores@kssoluciones.com.mx"
  #default from: "Listas_Blancas@homedepot.com.mx"

  def avisar_activacion(email, user)

    @email = email.to_s
    @usr_name = user.name
    @usr_last_name = user.last_name
    @usr_mail = user.email
    @id_usr = user.id

    mail to: @email, subject: default_i18n_subject(user: user.name)
  end

  def cuenta_activada(user)

    @user_id = user.id
    @user_email = user.email
    @user_name = user.name
    @last_name = user.last_name
    @login_name = user.user_name
    @role = user.profile.name

    @sh_profile = Permission.where("profile_id = ?", user.profile_id )

    mail to: @user_email, subject: default_i18n_subject(user: user.name)

  end
end