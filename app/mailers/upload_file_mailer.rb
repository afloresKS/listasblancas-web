class UploadFileMailer < ApplicationMailer

  default from: "#{APP_CONFIG[:email]}"

  def alert_confirmation(nom_file, id_user, t, c, cli, cu_ca_2, registro_sin_cuenta, registro_dif_cardType, arreglo1, arreglo2, cuentas_cardType3)
    @nombre_de_file = nom_file
    @totalCuentasGuar = t
    @totalClientGuard = c
    @totalCuaentaModi = cli
    @cuentas = t
    @clientes = c
    @cunetasMod = cli
    @cuentCardType2 = cu_ca_2
    @cuentCardType3 = cuentas_cardType3
    @registroSinCuenta = registro_sin_cuenta
    @filaSC = arreglo1
    @registroDifTipCuenta = registro_dif_cardType
    @filaDTC = arreglo2
    @mensaje = ""
    contadores = Hash.new()
    if (@cuentas > 0) && (@clientes > 0)
      @mensaje = "Cuentas y Clientes agregados"
      contadores["Cuentas"] = @cuentas
      contadores["Clientes"] = @clientes
    end
    if @cunetasMod > 0
      if @mensaje.bytesize != 0
        @mensaje += ", Cuentas modificadas"
        contadores["Cuentas Modificadas"] = @cunetasMod
      else
        @mensaje = "Cuentas Modificadas."
        contadores["Cuentas Modificadas"] = @cunetasMod
      end
    end
    if @cuentCardType2 > 0
      if @mensaje.bytesize != 0
        @mensaje += ", Cuentas agregadas con tipo de cuenta Post Date"
        contadores["Cuentas Post Date"] = @cuentCardType2
      else
        @mensaje = "Cuentas agregadas cont tipo de cuenta Post Date."
        contadores["Cuentas Post Date"] = @cuentCardType2
      end
    end
    if @cuentCardType3 > 0
      if @mensaje.bytesize != 0
        @mensaje += ", Cuentas agregadas con tipo de cuenta Revolving Check"
        contadores["Cuentas Revolving Check"] = @cuentCardType3
      else
        @mensaje = "Cuentas agregadas cont tipo de cuenta Revolving Check."
        contadores["Cuentas Revolving Check"] = @cuentCardType3
      end
    end
    if @registroSinCuenta > 0
      if @mensaje.bytesize != 0
        @mensaje += ", Registros sin Cuenta"
        contadores["Registros Sin Cuenta"] = @registroSinCuenta
      else
        @mensaje = "Registro sin Cuenta"
        contadores["Registros Sin Cuenta"] = @registroSinCuenta
      end
    end
    if @registroDifTipCuenta > 0
      if @mensaje.bytesize != 0
        @mensaje += ", Registro con tipo de cuenta no reconcido."
        contadores["Registro Tipo de Cuenta no Reconocido"] = @registroSinCuenta
      else
        @mensaje = "Registro con tipo de cuenta no reconocido."
        contadores["Registro Tipo de Cuenta no Reconocido"] = @registroSinCuenta
      end
    end

    user = User.find(id_user)

    @hash = contadores
    @@mensaje = @mensaje
    @@nom_file = nom_file
    # @user_name = upload_file.user.name
    # @user_last_name = upload_file.user.last_name
    @user_name = user.name
    @user_last_name = user.last_name
    # mail to: upload_file.user.email,
    mail to: user.email,
         #cc: "SopOpsListasBlancas@homedepot.com.mx",
         subject: @@mensaje
  end
end