class SendFile < ApplicationMailer

  default from: "#{APP_CONFIG[:email]}"

  def send_ptlfs_xls_es(fecha_comienzo, usuario_log, path_file, name_file, fecha_termino)
    @inicio = fecha_comienzo
    @use = "#{usuario_log.name} #{usuario_log.last_name}"
    @name_file = name_file
    @path_file = path_file
    @fin = fecha_termino

    attachments[@name_file] = File.read("#{@path_file}")
    mail to: usuario_log.email, subject: "Exportación de Resultados de Consulta de Transacciones a Excel Lista: #{@name_file}"
  end

  def send_ptlfs_xls_en(fecha_comienzo, usuario_log, path_file, name_file, fecha_termino)
    @inicio = fecha_comienzo
    @use = "#{usuario_log.name} #{usuario_log.last_name}"
    @name_file = name_file
    @path_file = path_file
    @fin = fecha_termino

    attachments[@name_file] = File.read("#{@path_file}")
    mail to: usuario_log.email, subject: "Result Export from Transactions Search to Excel is ready: #{@name_file}"
  end

  def send_accounts_xls_es(fecha_comienzo, usuario_log, path_file, name_file, fecha_termino)
    @inicio = fecha_comienzo
    @use = "#{usuario_log.name} #{usuario_log.last_name}"
    @name_file = name_file
    @path_file = path_file
    @fin = fecha_termino

    attachments[@name_file] = File.read("#{@path_file}")
    mail to: usuario_log.email, subject: "Exportación de Resultados de Consulta de Cuentas a Excel Lista: #{@name_file}"
  end

  def send_accounts_xls_en(fecha_comienzo, usuario_log, path_file, name_file, fecha_termino)
    @inicio = fecha_comienzo
    @use = "#{usuario_log.name} #{usuario_log.last_name}"
    @name_file = name_file
    @path_file = path_file
    @fin = fecha_termino

    attachments[@name_file] = File.read("#{@path_file}")
    mail to: usuario_log.email, subject: "Result Export from Accounts Search to Excel is ready: #{@name_file}"
  end

  def send_error_500_es(fecha, usuario, error, aplicativo, file, metod)
    @fecha = fecha
    @file = file
    @usuario = "#{usuario.name} #{usuario.last_name}"
    @error = error
    @method = metod
    @aplicativo = aplicativo
    attachments[@error] = File.read("#{@file}")
    mail to: "lmena@kssoluciones.com", subject: "Error en: #{@aplicativo}"
  end


end
