class AreasController < ApplicationController
  before_action :authenticate_user!
before_action :set_area, only: [:show, :edit, :update, :destroy]
before_action :auth, :permisos_vistas, :horarios
require 'time'

begin
  def index
    if @permisos[0] == 8 || @permisos[1] == 4 || @permisos[2] == 2 || @permisos[3] == 1 # si tiene alguno de los permisos se desplegara la tabla
      # >>>>>>>>>>>>>>>>>>>>>>>>>>> Tabla <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      if current_user.area_id == "1" then @areas = Area.select("areas.id,areas.name name1,a2.name name2").joins("left join areas as a2 on areas.area_id = a2.id").order(:name).page(params[:page]).per(10)
      else consult_subareas(current_user.area_id); @areas = Area.select("areas.id,areas.name name1,a2.name name2").joins("left join areas as a2 on areas.area_id = a2.id").where("areas.id #{@consulta_areas}").order(:name).page(params[:page]).per(10); end
      # >>>>>>>>>>>>>>>>>>>>>>>>>>> Tabla <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      # >>>>>>>>>>>>>>>>>>>>>>>>>>> Busqueda <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      if params[:search].present?
        @busqueda = params[:search].gsub(/[^a-zA-Z0-9 ]/,''); # evita sql injection
        registro(3,@busqueda);
        current_user.area_id == "1" ? consulta = "(areas.name LIKE :datos OR a2.name LIKE :datos)" : consulta = "(areas.name LIKE :datos OR a2.name LIKE :datos) AND areas.id #{@consulta_areas}"
        @areas = Area.select("areas.id,areas.name name1,a2.name name2").joins("left join areas as a2 on areas.area_id = a2.id").where(consulta, datos: "%#{@busqueda}%").order(:name).page(params[:page]).per(10)
        @error = 1 if @areas.empty?
      end
      # >>>>>>>>>>>>>>>>>>>>>>>>>>> Busqueda <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    else redirect_to message_path, :alert => t('all.not_access') end
  end

  def show
    registro(5,@area);
    if @permisos[2] == 2 #si tiene permiso de leer
      consult_subareas(current_user.area_id);
      if current_user.area_id != "1"
        if current_user.area_id == @area.id || !@consulta_areas.include?(@area.id.to_s) then redirect_to message_index_path, :alert => t('all.not_access')
        else
          consult_subareas(params[:id]);
          @areas = Area.select("areas.id,areas.name name1,a2.name name2").joins("left join areas as a2 on areas.area_id = a2.id").where("areas.id #{@consulta_areas}")
        end
      else
        consult_subareas(params[:id]);
        @areas = Area.select("areas.id,areas.name name1,a2.name name2").joins("left join areas as a2 on areas.area_id = a2.id").where("areas.id #{@consulta_areas}")
      end
    else redirect_to message_index_path, :alert => t('all.not_access') end
  end

  def new
    if @permisos[0] == 8 #si tiene permiso de crear
      @crear = 0
      if current_user.area_id == "1" then @areas = Area.all
      else
        consult_subareas(current_user.area_id);
        @areas = Area.where("id in (#{@arreglo_general.join(',')},#{current_user.area_id})")
      end
      @area = Area.new
    else redirect_to message_index_path, :alert => t('all.not_access') end
  end

  def edit
    if @permisos[1] == 4
      if current_user.area_id == "1"
        @areas = Area.all
      else
        consult_subareas(current_user.area_id);
        if !@consulta_areas.include?(current_user.area_id)
          redirect_to message_index_path, :alert => t('all.not_access')
        else
          @areas = Area.where("id in (#{@arreglo_general.join(',')},#{@area.area_id})" )
        end
      end
    else
    redirect_to message_index_path, :alert => t('all.not_access')
    end
  end

  def create
    if @permisos[0] == 8 #si tiene permiso de crear
      @nombre = params[:area][:name]
      @existe = Area.find_by_name(@nombre.to_s)
      if @existe.nil?
        @area = Area.new(area_params)
        @area.save
        @perfil = Profile.new
        @perfil.name = ("Administrator " + @area.name)
        @perfil.area_id = @area.id
        @perfil.flag = 1
        @perfil.save
        @view = View.where('father != 0')
        @view.each do |v|
          @pm = Permission.new
          @pm.view_id = v.id
          @pm.view_name = v.name
          @pm.profile_id = @perfil.id
          @pm.horafinal = '00:00'
          @pm.horainicial = '00:00'
          @pm.save
        end
        registro(1,@area);
      respond_to do |format|
        if @area.save
          format.html {redirect_to areas_path, notice: t('activerecord.atributes.ar')}
          format.json {render action: 'index', status: :created, location: @area}
        else
          format.html {render action: 'new'}
          format.json {render json: @area.errors, status: :unprocessable_entity}
        end
      end
    else
      redirect_to areas_path, alert: t('activerecord.atributes.not_saved')
    end
    else
      redirect_to message_index_path, :alert => t('all.not_access')
    end
  end

  # PATCH/PUT /areas/1
  # PATCH/PUT /areas/1.json
  def update
    @area_existente = Area.find_by_name(params[:area][:name])
    if @area_existente.nil? || @area_existente.id == @area.id
    if params[:subarea].present?
      if @area.update(area_params);
        registro(2,@area);
      end
    else
      @area.update_attribute(:name,params[:area][:name]);
      @area.update_attribute(:area_id, nil);
      registro(2,@area);
    end
      redirect_to areas_path, notice: t('activerecord.atributes.suc')
    else
      redirect_to areas_path, alert: t('views.exist_area')
    end
  end

  def destroy
      permisos_vistas;
      if @permisos[3] == 1 #si tiene permiso de eliminar
        @area = Area.find_by_id(params[:id])
        @subarea = Area.where("area_id = #{params[:id]}")
        if @subarea.present?
          consult_subareas(params[:id]);
        else
          @consulta_areas="in(#{params[:id]})"
        end
        usuario = User.where("area_id #{@consulta_areas}")
        subareas = Area.where("id #{@consulta_areas}");
        perfiles = Profile.where("area_id #{@consulta_areas}");
        arreglo_perfiles = Array.new
        perfiles.each do |a|
          arreglo_perfiles.push(a.id)
        end
        @consulta_areas = "in(#{arreglo_perfiles.join(",")})"
        permiso = Permission.where("profile_id #{@consulta_areas}")
        subareas.destroy_all
        perfiles.destroy_all
        usuario.update_all("habilitado = 0")
        usuario.update_all("profile_id = 2")
        usuario.update_all("area_id = null")
        permiso.destroy_all
        registro(4,@area)
      else
        redirect_to message_index_path, :alert => t('all.not_access')
      end
  end

  def cont_areas
    consult_subareas(params[:area_id]);
    cont = Area.where("id #{@consulta_areas}").count
    cont -= 1
    render :json => cont
  end

  private

  def registro(accion,detalle)
    @historic = Historic.new
    @historic.page = "Areas"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  def consult_subareas (x)
    areas = Area.where("area_id = ?",x)
    @arreglo_general = Array.new
    @arreglo_general.push(x);
    arreglo_subareas = Array.new
    control = 0
    # se buscaran todas las subareas de la area actual del usuario logeado
    while control != 1 do
    arreglo_subareas.clear
    areas.each do |a|
      arreglo_subareas.push(a.id) # se almacenaran todas las subareas para la consulta especifica
      @arreglo_general.push(a.id) # se almacenaran todas las areas para la consulta general
    end # <<<<<<<<<<<<<<<<<<<<<<<<<< Each do <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @consulta_areas = "in(#{arreglo_subareas.join(",")})" #creara la consulta para buscar en otras subareas subsecuentes
    if arreglo_subareas.empty? # cuando ya no existan mas subareas acabara el ciclo
      control = 1
    else
      areas = Area.where("area_id #{@consulta_areas}")
    end
    end # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< while <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @consulta_areas="in(#{@arreglo_general.join(",")})" # se hara la consulta general de todos los usuarios de las subareas
  end

  def permisos_vistas
  # se verifican los permisos de la vista y se almacenan
    consulta = Permission.where("view_id = 13 and profile_id = ?", current_user.profile_id)
    consulta.each do |permisos|
    @permisos = Array[permisos.crear, permisos.editar, permisos.leer, permisos.eliminar, permisos.horainicial, permisos.horafinal]
    end
  end
  # Use callbacks to share common setup or constraints between actions.
  def set_area
    @area = Area.find(params[:id])
  end

  def horarios
    unless Time.now >= Time.parse(@permisos[4].strftime("%H:%M")) && Time.now <= Time.parse(@permisos[5].strftime("%H:%M")) || @permisos[5].strftime("%H:%M") == "00:00" || @permisos[4].strftime("%H:%M") == "00:00"
     redirect_to message_index_path, :alert => t('all.not_access');
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def area_params
    params.require(:area).permit(:name, :area_id)
  end

  def auth
    if !current_user || current_user.habilitado == 0 || current_user.activo == 0 then redirect_to destroy_user_session_path, :alert => t('all.please_continue') end
  end

  rescue => e
  logger = Logger.new("log/error_areas.log")
  logger.error(e)
  end
end
