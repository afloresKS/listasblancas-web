class ProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  before_action :auth, :permisos_vistas, :horarios
  require 'time'

begin
def index
  if  @permisos[0] == 8 || @permisos[1] == 4 || @permisos[2] == 2 || @permisos[3] == 1 # si tiene alguno de los permisos se desplegara la tabla
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Mostrar Perfiles >>>>>>>>>>>>>>>>>>>>>>>>>
    if current_user.area_id == "1" # si es de area 1 se desplegaran todos los usuarios de todas las areas
      @profiles = Profile.where("flag != 2").page(params[:page]).per(10)
    else
      consult_subareas();
      @profiles = Profile.where("area_id #{@consulta_areas} and flag != 2").page(params[:page]).per(10)
    end
    if params[:search].present?
      @busqueda = params[:search].gsub(/[^a-zA-Z0-9 ]/,''); # evita sql injection
      registro(3,@busqueda);
      if current_user.area_id == "1"
        consulta = "(profiles.name LIKE :datos OR areas.name LIKE :datos) AND profiles.flag != 2"
      else
        consulta = "(profiles.name LIKE :datos OR areas.name LIKE :datos) AND areas.id #{@consulta_areas} AND profiles.flag != 2"
      end
      @profiles = Profile.joins('inner join areas on areas.id = profiles.area_id').where(consulta, datos: "%#{@busqueda}%").page(params[:page]).per(10);
      @error = 1 if @profiles.empty?
    end
  else redirect_to message_index_path, :alert => t('all.not_access') end
end

def create
  @perfil_existente = Profile.find_by(name: params[:profile][:name], area_id: params[:profile][:area_id])
  if @perfil_existente.nil?
    @profile = Profile.new(profile_params);
    @id_perfil = Profile.find_by_id(params[:profile][:profile_id])
    if @id_perfil.flag == 0 || @id_perfil.flag == 1 #Administrador Central o Administrador de Área
      @profile.update_attribute(:flag,3)
    elsif @id_perfil.flag >= 3 #Cualquier otro perfil
      @profile.update_attribute(:flag,@id_perfil.flag+1)
    end
    @views = View.where('father != 0')
    @views.each do |i|
      @permission = Permission.new
      @permission.view_id = i.id
      @permission.view_name = i.name
      @permission.crear = params[:"#{i.name}_crear"];
      @permission.editar = params[:"#{i.name}_editar"];
      @permission.leer = params[:"#{i.name}_leer"];
      @permission.eliminar = params[:"#{i.name}_eliminar"];
      @permission.horafinal = params[:"#{i.name}_end"];
      @permission.horainicial = params[:"#{i.name}_start"];
      @permission.profile_id = @profile.id
      @permission.save
    end
    if @profile.save
      registro(1,@profile);
      redirect_to profiles_path, :notice => t('views.mess_cont_per_sus')
    end
  else
    redirect_to profiles_path, :alert => t('views.exist_profile')
  end
end

def new
    if @permisos[0] == 8 #si tiene permiso de crear
      consult_subareas();
      @crear=0
      if current_user.area_id == "1"
        @areas = Area.all
      else
        @areas = Area.where("id #{@consulta_areas}")
      end
      @profile = Profile.new
      @views = View.all
      @allprofiles = Profile.where("flag >= #{current_user.profile.flag}")
      # @permis = Permission.joins('inner join profiles on profiles.id = permissions.profile_id').where("profiles.id = 2")
      @permis = Permission.select('permissions.*, profiles.*').joins('inner join views on views.id = permissions.view_id').joins('inner join profiles on profiles.id = permissions.profile_id').where("permissions.profile_id = 2").order('views.father, views.name')
      @fs = View.where(father: '0').take
      @titles = [0,1]
      @titletext = ["all.home","views.profiles_edit.settings"]
    else redirect_to message_index_path, :alert => t('all.not_access') end
end

def edit
    if @permisos[1] == 4 #si tiene permiso de editar
      consult_subareas();
      if current_user.area_id == "1"
        @areas = Area.all
        if @profile.flag < current_user.profile.flag || @profile.id == current_user.profile.id
          redirect_to message_index_path, :alert => t('all.not_access')
        end
      else
        @areas = Area.where("id #{@consulta_areas}")
        if @profile.flag < current_user.profile.flag || @profile.id == current_user.profile.id || !@consulta_areas.include?(@profile.area_id.to_s) # seguridad extra
          redirect_to message_index_path, :alert => t('all.not_access')
        end
      end
      @profile = Profile.find_by_id(params[:id]);
      @views = View.all
      @allprofiles = Profile.where("area_id = #{@profile.area_id}")
      # @permis = Permission.joins('inner join profiles on profiles.id = permissions.profile_id').where("profiles.id = #{params[:id]}")
      @permis = Permission.select('permissions.*, profiles.*').joins('inner join views on views.id = permissions.view_id').joins('inner join profiles on profiles.id = permissions.profile_id').where("permissions.profile_id = #{params[:id]}").order('views.father, views.name')
      @fs = View.where(father: '0').take
      @titles = [0,1]
      @titletext = ["all.home","views.profiles_edit.settings"]
    else redirect_to message_index_path, :alert => t('all.not_access') end
end

def update
  @perfil_existente = Profile.find_by(name: params[:profile][:name], area_id: params[:profile][:area_id])
  if @perfil_existente.nil? || @perfil_existente.id == @profile.id
    if @profile.update(profile_params)
      registro(2,@profile);
      @views = View.all
      @permission = Permission.where("profile_id = #{@profile.id}")
      @permission.each do |i|
        i.update_attribute(:crear,params[:"#{i.view_name}_crear"]);
        i.update_attribute(:editar,params[:"#{i.view_name}_editar"]);
        i.update_attribute(:leer,params[:"#{i.view_name}_leer"]);
        i.update_attribute(:eliminar,params[:"#{i.view_name}_eliminar"]);
        i.update_attribute(:horafinal,params[:"#{i.view_name}_end"]);
        i.update_attribute(:horainicial,params[:"#{i.view_name}_start"]);
      end
      redirect_to profiles_path, :notice => t('change_perfil.permisos_otorgados.subject')
    end
  else
    redirect_to profiles_path, :alert => t('views.exist_profile')
  end
end

def show
    if @permisos[2] == 2 #si tiene permiso de leer
      registro(5,@profile);
      if current_user.area_id != "1"
        consult_subareas();
        if !@consulta_areas.include?(@profile.area_id.to_s) # seguridad extra
          redirect_to profiles_path
        else
          @profile = Profile.find_by_id(params[:id])
          @total_profile = Permission.select('permissions.*, profiles.*').joins('inner join views on views.id = permissions.view_id').joins('inner join profiles on profiles.id = permissions.profile_id').where("permissions.profile_id = #{params[:id]}").order('views.father, views.name')
          # @total_profile = Permission.where("profile_id = #{params[:id]}")
          @fs = View.where(father: '0').take
          @titles = [0,1]
          @titletext = ["all.home","views.profiles_edit.settings"]
        end
      else
        @profile = Profile.find_by_id(params[:id])
        @total_profile = Permission.select('permissions.*, profiles.*').joins('inner join views on views.id = permissions.view_id').joins('inner join profiles on profiles.id = permissions.profile_id').where("permissions.profile_id = #{params[:id]}").order('views.father, views.name')
        # @total_profile = Permission.where("profile_id = #{params[:id]}")
        @fs = View.where(father: '0').take
        @titles = [0,1]
        @titletext = ["all.home","views.profiles_edit.settings"]
      end
    end
end

def destroy
    if @permisos[3] == 1 #si tiene permiso de leer
      @profile = Profile.find_by_id(params[:id])
      consult_subareas();
      if current_user.area_id == "1"
        if @profile.flag < current_user.profile.flag || @profile.id == current_user.profile.id
          redirect_to profiles_path
        else
          @users = User.where("profile_id = #{params[:id]}")
          @users.update_all("profile_id = 2")
          @permisos = Permission.where("profile_id = #{params[:id]}")
          @permisos.destroy_all
          @profile.destroy
          registro(4,@profile);
        end
      else
        if @profile.flag < current_user.profile.flag || @profile.id == current_user.profile.id || !@consulta_areas.include?(@profile.area_id.to_s) # seguridad extra
          redirect_to profiles_path
        else
          @users = User.where("profile_id = #{params[:id]}")
          @users.update_all("profile_id = 2")
          @permisos = Permission.where("profile_id = #{params[:id]}")
          @permisos.destroy_all
          @profile.destroy
          registro(4,@profile);
        end
      end
    end
end

def consult_profiles
  perfiles = Profile.where( "area_id = #{params[:area_id]} and flag >= #{current_user.profile.flag}" )
  render :json => perfiles
end

private

def registro(accion,detalle)
  @historic = Historic.new
  @historic.page = "Profiles"
  @historic.user_name = current_user.name + " " + current_user.last_name
  @historic.user_id = current_user.id
  @historic.action = accion
  @historic.date = Time.now()
  @historic.detail = detalle.to_json
  @historic.save
end

def consult_subareas
  areas = Area.where("area_id = ?",current_user.area_id)
  arreglo_general = Array.new
  arreglo_general.push(current_user.area_id);
  arreglo_subareas = Array.new
  control = 0
  # se buscaran todas las subareas de la area actual del usuario logeado
  while control != 1 do
  arreglo_subareas.clear
  areas.each do |a|
    arreglo_subareas.push(a.id) # se almacenaran todas las subareas para la consulta especifica
    arreglo_general.push(a.id) # se almacenaran todas las areas para la consulta general
  end # <<<<<<<<<<<<<<<<<<<<<<<<<< Each do <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  @consulta_areas = "in(#{arreglo_subareas.join(",")})" #creara la consulta para buscar en otras subareas subsecuentes
  if arreglo_subareas.empty? # cuando ya no existan mas subareas acabara el ciclo
    control = 1
  else
    areas = Area.where("area_id #{@consulta_areas}")
  end
  end # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< while <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  @consulta_areas="in(#{arreglo_general.join(",")})" # se hara la consulta general de todos los usuarios de las subareas
end

def horarios
  unless Time.now >= Time.parse(@permisos[4].strftime("%H:%M")) && Time.now <= Time.parse(@permisos[5].strftime("%H:%M")) || @permisos[5].strftime("%H:%M") == "00:00" || @permisos[4].strftime("%H:%M") == "00:00"
   redirect_to message_index_path, :alert => t('all.not_access');
  end
end

  def permisos_vistas
  # se verifican los permisos de la vista y se almacenan
    consulta = Permission.where("view_id = 12 and profile_id = ?", current_user.profile_id)
    consulta.each do |permisos|
    @permisos = Array[permisos.crear, permisos.editar, permisos.leer, permisos.eliminar, permisos.horainicial, permisos.horafinal]
    end
  end

  def auth
    if !current_user || current_user.habilitado == 0 || current_user.activo == 0 then redirect_to destroy_user_session_path, :alert => t('all.please_continue') end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:name, :flag, :area_id)
  end

  rescue => e
  logger = Logger.new("log/error_profiles.log")
  logger.error(e)
  end
end
