class RegionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_region, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js
  @@region = "Regions"

  def index
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Regions' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearRegion = permisos.crear
            @editarRegion = permisos.editar
            @leerRegion = permisos.leer
            @eliminarRegion = permisos.eliminar
            if @uno == @@region
              @@crear = permisos.crear
              @@editar = permisos.editar
              @@leer = permisos.leer
              @@eliminar = permisos.eliminar

              @crear = permisos.crear
              @editar = permisos.editar
              @leer = permisos.leer
              @eliminar = permisos.eliminar
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4) || (@@eliminar == 1))
            if params[:search].present?
              registro(3, params[:search]);
              busqueda = params[:search]
              if Region.where("name like '%#{busqueda}%'").present?
                @regions = Region.where("name like '%#{busqueda}%'").order("created_at DESC").page(params[:page]).per(20)
                @query = busqueda
              else
                @regions = Region.order("created_at DESC").page(params[:page]).per(20)
                @error = true
              end
            else
              @regions = Region.order("created_at DESC").page(params[:page]).per(20)
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorRegions.log')
      logger.error("Error Regions en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "Regions método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def show
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Regions' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerRegion = permisos.leer
            if @uno == @@region
              @@leer = permisos.leer
              @leer = permisos.leer
            end
          end
          if (@@leer == 2)
            registro(5, @region);
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorRegions.log')
      logger.error("Error Regions en el metodo show #{Time.now.to_s}")
      logger.error(e)
      @metod = "Regions método show"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def new
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Regions' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearRegion = permisos.crear
            if @uno == @@region
              @@crear = permisos.crear
              @crear = permisos.crear
            end
          end
          if (@@crear == 8)
            @region = Region.new
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorRegions.log')
      logger.error("Error Regions en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "Regions método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def edit
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Regions' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @editarRegion = permisos.editar
            if @uno == @@region
              @@editar = permisos.editar
              @editar = permisos.editar
            end
          end
          if (@@editar == 4)

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorRegions.log')
      logger.error("Error Regions en el metodo edit #{Time.now.to_s}")
      logger.error(e)
      @metod = "Regions método edit"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def create
    begin
      registro(1, @region);
      @region = Region.new(region_params)
      respond_to do |format|
        if Region.where("name like ?", @region.name).exists?
          format.html {
            @error2 = true
            render action: :new
          }
        else
          if @region.save
            format.html {redirect_to :controller => 'regions', :action => 'index'}
            flash[:notice] = t('views.message_region.new_region')
            format.json {render :show, status: :created, location: @region}
          else
            format.html {render :new}
            format.json {render json: @region.errors, status: :unprocessable_entity}
          end
        end
      end
    rescue => e
      logger = Logger.new('log/errorRegions.log')
      logger.error("Error Regions en el metodo create #{Time.now.to_s}")
      logger.error(e)
      @metod = "Regions método create"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def update
    registro(2, @region)
    begin
      respond_to do |format|
        @consulta = Region.find_by_name(params[:region][:name])
        if @consulta.nil? || @consulta.id == @region.id
          if @region.update(region_params)
            format.html {redirect_to :controller => 'regions', :action => 'index'}
            flash[:notice] = t('views.message_region.edit')
            format.json {render :show, status: :ok, location: @region}
          else
            format.html {render :edit}
            format.json {render json: @region.errors, status: :unprocessable_entity}
          end
        else
          format.html {
            @error2 = true
            render action: :new
          }
        end
      end
    rescue => e
      logger = Logger.new('log/errorRegions.log')
      logger.error("Error Regions en el metodo update #{Time.now.to_s}")
      logger.error(e)
      @metod = "Regions método update"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def destroy
    if current_user
      @cu = current_user.profile_id
      if @cu != 0
        @per = Permission.where("view_name = 'Regions' and profile_id = ?", @cu)
        @per.each do |permisos|
          @uno = permisos.view_name
          @eliminarRegion = permisos.eliminar
          if @uno == @@region
            @@eliminar = permisos.eliminar
            @eliminar = permisos.eliminar
          end
        end
        if (@@eliminar == 1)
          registro(4, @region)
          @region.destroy
          respond_to do |format|
            format.html {redirect_to regions_url, notice: t('views.message_region.mess_region_swall')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_region
    @region = Region.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def region_params
    params.require(:region).permit(:name)
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "Regions"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end

end
