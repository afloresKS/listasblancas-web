class CardsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_card, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js
  @@card = "Cards"

  require 'action_view'
  include ActionView::Helpers::NumberHelper

  # GET /cards
  # GET /cards.json
  def index
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Cards' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearCard = permisos.crear
            @editarCard = permisos.editar
            @leerCard = permisos.leer
            @eliminarCard = permisos.eliminar
            if @uno == @@card
              @@crear = permisos.crear
              @@editar = permisos.editar
              @@leer = permisos.leer
              @@eliminar = permisos.eliminar
              @crear = permisos.crear
              @editar = permisos.editar
              @leer = permisos.leer
              @eliminar = permisos.eliminar
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4) || (@@eliminar == 1))
            @@tiempo = Time.now.strftime('%Y%m%d')
            if (params.has_key?(:id))
              @card_id = params[:id]
              @client_id = params[:id]
              @cards = Card.where("client_id = ?", params[:id])
              @cards = @cards.order('created_at DESC').page(params[:page]).per(20)
              @boton = true;
              @cardxlsx = Card.where("client_id = ?", params[:id]).order('created_at DESC')
              respond_to do |format|
                format.html
                format.xlsx {
                  response.headers['Content-Disposition'] = "attachment; filename=cards" + @@tiempo + ".xlsx"
                }
              end
            elsif (params.has_key?(:exausted_credit))
              @today = Date.today
              @cards = Card.where("actual_balance = '0' ").page(params[:page]).per(20)
              flash[:notice] = t('views.cards_index.flash1')
              @cardxlsx = Card.where("actual_balance = '0' ")
              respond_to do |format|
                format.html
                format.xlsx {
                  response.headers['Content-Disposition'] = "attachment; filename=cards" + @@tiempo + ".xlsx"
                }
              end
            elsif (params.has_key?(:exausted_limit))
              @today = Date.today
              @cards = Card.where("transaction_actual = '0' ").page(params[:page]).per(20)
              flash[:notice] = t('views.cards_index.flash2')
              @cardxlsx = Card.where("transaction_actual = '0' ")
              respond_to do |format|
                format.html
                format.xlsx {
                  response.headers['Content-Disposition'] = "attachment; filename=cards" + @@tiempo + ".xlsx"
                }
              end
            elsif (params.has_key?(:bloqued))
              @today = Date.today
              @cards = Card.where("active = ? ", false).page(params[:page]).per(20)
              flash[:notice] = t('views.cards_index.flash3')
              @cardxlsx = Card.where("active = ? ", false)
              respond_to do |format|
                format.html
                format.xlsx {
                  response.headers['Content-Disposition'] = "attachment; filename=cards" + @@tiempo + ".xlsx"
                }
              end
            end
            #-------------------------------------------- BUSQUEDA ----------------------------------------------#
            consulta = ""
            filtro = ""
            if params[:number_card].present?
              number_card = params[:number_card].to_s.strip
              if consulta.bytesize == 0
                consulta = "number_card = '#{number_card}'"
                filtro = "#{t('views.cards_index.n_account')}: #{number_card}"
              end
            end
            if params[:actual_balan].present?
              actual_balance = params[:actual_balan].to_s.strip
              if consulta.bytesize == 0
                consulta = "actual_balance = '#{actual_balance}'"
                filtro = "#{t('views.cards_index.a_balance')}: #{number_to_currency((actual_balance.to_f) / 100, locale: :en)} "
              else
                consulta += " AND (actual_balance = '#{actual_balance}' )"
                filtro += " #{t('views.cards_index.and')} #{t('views.cards_index.a_balance')}: #{number_to_currency((actual_balance.to_f) / 100, locale: :en)} "
              end
            end
            if params[:transaction_actual].present?
              trans_actual = params[:transaction_actual].to_s.strip
              if consulta.bytesize == 0
                consulta = "transaction_actual = '#{trans_actual}'"
                filtro = "#{t('views.cards_index.a_operations')}: #{trans_actual}"
              else
                consulta += " AND (transaction_actual = '#{trans_actual}')"
                filtro += " #{t('views.cards_index.and')} #{t('views.cards_index.a_operations')}: #{trans_actual}"
              end
            end
            if params[:active].present?
              if consulta.bytesize == 0
                if params[:active] == "1"
                  consulta = "active = ?", true
                  filtro = "#{t('views.cards_index.a_status')}: #{t('views.cards_index.active')}"
                else
                  consulta = "active = ?", false
                  filtro = "#{t('views.cards_index.a_status')}: #{t('views.cards_index.locked')}"
                end
              else
                if params[:active] == "1"
                  consulta += " AND  (active = '1' )"
                  filtro += " #{t('views.cards_index.and')} #{t('views.cards_index.a_status')}: #{t('views.cards_index.active')}"
                else
                  consulta += " AND (active = '0' )"
                  filtro += " #{t('views.cards_index.and')} #{t('views.cards_index.a_status')}: #{t('views.cards_index.locked')}"
                end
              end
            end
            @query = filtro
            @boton = false;
            @cards = Card.where(consulta).order("created_at DESC").page(params[:page]).per(20)
            if !@cards.present?
              @cards = Card.where(consulta).order("created_at DESC").page(params[:page]).per(20)
              @error = true
            end
            @cardxlsx = Card.where(consulta).order("created_at DESC")
            respond_to do |format|
              format.html
              format.xlsx {
                response.headers['Content-Disposition'] = "attachment; filename=cards" + @@tiempo + ".xlsx"
              }
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorcards.log')
      logger.error("Error cards en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "cards método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def send_file
    begin
      if params[:click].present?
        registro(1, params[:click]);
        @variable = true
        redirect_to cards_path(:seenvio => @variable)
        @idioma = params[:locale]
        puts red('Comienza hilo --Cards Listas Blancas')
        if (params.has_key?(:id))
          @cardxlsx = Card.where("client_id = ?", params[:id]).order('created_at DESC')
          Thread.new do
            begin
              @user = User.find(current_user.id)
              puts cyan('Comienza Generación de XLS-Listas-Blancas')
              reci = CreateFile.new
              reci.creaXlSx(@cardxlsx, @idioma, @user)
            end
          end
        elsif (params.has_key?(:exausted_credit))
          @today = Date.today
          @cards = Card.where("actual_balance = '0' ").page(params[:page]).per(20)
          @cardxlsx = Card.where("actual_balance = '0' ")
          Thread.new do
            begin
              @user = User.find(current_user.id)
              puts cyan('Comienza Generación de XLS-Listas-Blancas')
              reci = CreateFile.new
              reci.creaXlSx(@cardxlsx, @idioma, @user)
            end
          end
        elsif (params.has_key?(:exausted_limit))
          @today = Date.today
          flash[:notice] = "Exhausted its limit of operations"
          @cardxlsx = Card.where("transaction_actual = '0' ")
          Thread.new do
            begin
              @user = User.find(current_user.id)
              puts cyan('Comienza Generación de XLS-Listas-Blancas')
              reci = CreateFile.new
              reci.creaXlSx(@cardxlsx, @idioma, @user)
            end
          end
        elsif (params.has_key?(:bloqued))
          @today = Date.today
          flash[:notice] = "Blooked Accounts"
          @cardxlsx = Card.where("active = ? ", false)
          Thread.new do
            begin
              @user = User.find(current_user.id)
              puts cyan('Comienza Generación de XLS-Listas-Blancas')
              reci = CreateFile.new
              reci.creaXlSx(@cardxlsx, @idioma, @user)
            end
          end
        end
        #-------------------------------------------- BUSQUEDA ----------------------------------------------#
        @cards = Card.all
        Thread.new do
          consulta = ""
          filtro = ""
          actual = ""
          if params[:number_card].present?
            number_card = params[:number_card].to_s.strip
            if consulta.bytesize == 0
              consulta = "number_card = '#{number_card}'"
            end
          end
          if params[:actual_balan].present?
            actual_balance = params[:actual_balan].to_s.strip
            if consulta.bytesize == 0
              consulta = "actual_balance = '#{actual_balance}'"
            else
              consulta += " AND actual_balance = '#{actual_balance}' "
            end
          end
          if params[:transaction_actual].present?
            trans_actual = params[:transaction_actual].to_s.strip
            if consulta.bytesize == 0
              consulta = "transaction_actual = '#{trans_actual}'"
            else
              consulta += " AND transaction_actual = '#{trans_actual}'"
            end
          end
          if params[:active].present?
            if consulta.bytesize == 0
              if params[:active] == "1"
                consulta += " active = '1'"
              else
                consulta += " active = '0'"
              end
            else
              if params[:active] == "1"
                consulta += " AND  active = '1'"
              else
                consulta += " AND active = '0'"
              end
            end
          end
          @consulta = consulta
          begin
            if consulta == ""
              @cardxlsx = Card.all
            else
              @cardxlsx = Card.where(consulta).order("created_at DESC")
            end
            @user = User.find(current_user.id)
            reci = CreateFile.new
            reci.creaXlSx(@cardxlsx, @idioma, @user)
          end
        end
      else
        redirect_to cards_path
      end
    rescue => e
      logger = Logger.new('log/errorcards.log')
      logger.error("Error cards en el metodo sendFile #{Time.now.to_s}")
      logger.error(e)
      @metod = "cards método SendFile"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /cards/1
  # GET /cards/1.json
  def show
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Cards' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerCard = permisos.leer
            if @uno == @@card
              @@leer = permisos.leer
              @leer = permisos.leer
            end
          end
          if (@@leer == 2)
            registro(5, @card);
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorcards.log')
      logger.error("Error cards en el metodo show #{Time.now.to_s}")
      logger.error(e)
      @metod = "cards método show"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /cards/new
  def new
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Cards' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearCard = permisos.crear
            if @uno == @@card
              @@crear = permisos.crear
              @crear = permisos.crear
            end
          end
          if (@@crear == 8)
            @isnew = true;
            client_id = params[:id]
            @card = Card.new
            @card.client_id = client_id
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorcards.log')
      logger.error("Error cards en el metodo new #{Time.now.to_s}")
      logger.error(e)
      @metod = "cards método new"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /cards/1/edit
  def edit
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Cards' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @editarCard = permisos.editar
            if @uno == @@card
              @@editar = permisos.editar
              @editar = permisos.editar
            end
          end
          if (@@editar == 4)
            @btn = true
            @cardtype = @card.cardType_id
            saldo_anterior = @card.initial_balance
            limit_anterior = @card.transaction_initial
            session[:saldo_anterior] = saldo_anterior
            session[:limit_anterior] = limit_anterior
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorcards.log')
      logger.error("Error cards en el metodo edit #{Time.now.to_s}")
      logger.error(e)
      @metod = "cards método edit"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def create
    begin
      registro(1, @card);
      @card = Card.new(card_params)
      @card.number_card = @card.number_card.to_i
      @card.actual_balance = @card.initial_balance
      @card.transaction_actual = @card.transaction_initial
      if Card.where("number_card = '#{@card.number_card}' and cardType_id = #{@card.cardType_id} and cvv = '#{@card.cvv}'").exists?
        redirect_to new_card_path(:id => @card.client_id), :alert => t('views.message_cards.already')
      else
        respond_to do |format|
          if @card.save
            format.html {redirect_to @card, notice: t('views.message_cards.new')}
            format.json {render :show, status: :created, location: @card}
          else
            format.html {render :new}
            format.json {render json: @card.errors, status: :unprocessable_entity}
          end
        end
      end
    rescue => e
      logger = Logger.new('log/errorcards.log')
      logger.error("Error cards en el metodo create #{Time.now.to_s}")
      logger.error(e)
      @metod = "cards método create"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end

  end

  def update
    begin
      registro(2, @card)
      saldo_anterior = session[:saldo_anterior]
      limit_anterior = session[:limit_anterior]
      respond_to do |format|
        @card.number_card = @card.number_card.to_i
        if @card.update(card_params)
          valor = 0
          valTwo = 0
          if @card.active == false
            @card.active = true
          else
            @card.active = false
          end
          if @card.initial_balance.to_i > saldo_anterior.to_i
            dif = @card.initial_balance.to_i - saldo_anterior.to_i
            valor = @card.actual_balance.to_i + dif
          else
            dif = saldo_anterior.to_i - @card.initial_balance.to_i
            valor = @card.actual_balance.to_i - dif
          end
          if @card.transaction_initial.to_i > limit_anterior.to_i
            dif_tra = @card.transaction_initial.to_i - limit_anterior.to_i
            valTwo = @card.transaction_actual.to_i + dif_tra
          else
            dif_tra = limit_anterior.to_i - @card.transaction_initial.to_i
            valTwo = @card.transaction_actual.to_i - dif_tra
          end
          @card.update_attribute("actual_balance", valor)
          @card.update_attribute("transaction_actual", valTwo)
          format.html {redirect_to @card, notice: t('views.message_cards.edit')}
          format.json {render :show, status: :ok, location: @cards}
        else
          format.html {render :edit}
          format.json {render json: @card.errors, status: :unprocessable_entity}
        end
      end
    rescue => e
      logger = Logger.new('log/errorcards.log')
      logger.error("Error cards en el metodo update #{Time.now.to_s}")
      logger.error(e)
      @metod = "cards método update"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # DELETE /cards/1
  # DELETE /cards/1.json
  def destroy
    if current_user
      @cu = current_user.profile_id
      if @cu != 0
        @per = Permission.where("view_name = 'Cards' and profile_id = ?", @cu)
        @per.each do |permisos|
          @uno = permisos.view_name
          @eliminarCard = permisos.eliminar
          if @uno == @@card
            @@eliminar = permisos.eliminar
            @eliminar = permisos.eliminar
          end
        end
        if (@@eliminar == 1)
          registro(4, @card)
          @card.destroy
          respond_to do |format|
            format.html {redirect_to cards_url, notice: t('views.message_cards.mess_card_swall')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_card
    @card = Card.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def card_params
    params.require(:card).permit(:client_id, :number_card, :cardType_id, :initial_balance, :actual_balance, :points, :currency_id, :expiration, :last_at, :cvv, :floor_limit, :active, :transaction_initial, :transaction_actual)
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "Cards"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end

end