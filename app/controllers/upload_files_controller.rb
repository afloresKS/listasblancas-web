class UploadFilesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_upload_file, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js
  @@archive = "Archives"
  # GET /upload_files
  # GET /upload_files.json
  def index
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Archives' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearArchive = permisos.crear
            @leerArchive = permisos.leer
            @eliminarArchive = permisos.eliminar
            if @uno == @@archive
              @@crear = permisos.crear
              @@leer = permisos.leer
              @@eliminar = permisos.eliminar

              @crear = permisos.crear
              @leer = permisos.leer
              @eliminar = permisos.eliminar
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@eliminar == 1))
            @upload_files = UploadFile.all.order("created_at DESC").page(params[:page]).per(20)
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorFiles.log')
      logger.error("Error Files en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "Files método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /upload_files/1
  # GET /upload_files/1.json
  def show
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Archives' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerArchive = permisos.leer
            if @uno == @@archive
              @@leer = permisos.leer
              @leer = permisos.leer
            end
          end
          if (@@leer == 2)
            registro(5, @upload_file);
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorFiles.log')
      logger.error("Error Files en el metodo show #{Time.now.to_s}")
      logger.error(e)
      @metod = "Files método show"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /upload_files/new
  def new
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Archives' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearArchive = permisos.crear
            if @uno == @@archive
              @@crear = permisos.crear
              @crear = permisos.crear
            end
          end
          if (@@crear == 8)
            @upload_file = UploadFile.new
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorFiles.log')
      logger.error("Error Files en el metodo new #{Time.now.to_s}")
      logger.error(e)
      @metod = "Files método new"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /upload_files/1/edit
  def edit
    begin

    rescue => e
      logger = Logger.new('log/errorFiles.log')
      logger.error("Error Files en el metodo edit #{Time.now.to_s}")
      logger.error(e)
      @metod = "Files método edit"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # POST /upload_files
  # POST /upload_files.json
  def create
    begin
      registro(1, @upload_file);
      @upload_file = UploadFile.new(upload_file_params)
      @upload_file.user = current_user
      id_user = current_user.id
      @@file = @upload_file.file.path
      if @upload_file.save
        render json: {message: "success"}, :status => 200
      else
        render json: {error: @upload_file.errors.full_messages.join(',')}, :status => 400
      end
      if @upload_file.save
        @idFile = @upload_file.id
        Thread.new do
          begin
            #Read.process(@@file,@upload_file,@idFile)
            Read.process(@@file, id_user, @idFile, @upload_file.file_file_name)
            #Read.process(@@file,@@file,id_user,@idFile)
          end
        end
      end
    rescue => e
      logger = Logger.new('log/errorFiles.log')
      logger.error("Error Files en el metodo create #{Time.now.to_s}")
      logger.error(e)
      @metod = "Files método create"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # DELETE /upload_files/1
  # DELETE /upload_files/1.json
  def destroy
    if current_user
      @cu = current_user.profile_id
      if @cu != 0
        @per = Permission.where("view_name = 'Archives' and profile_id = ?", @cu)
        @per.each do |permisos|
          @uno = permisos.view_name
          @eliminarArchive = permisos.eliminar
          if @uno == @@archive
            @@eliminar = permisos.eliminar
            @eliminar = permisos.eliminar
          end
        end
        if (@@eliminar == 1)
          registro(4, @upload_file)
          @upload_file.destroy
          respond_to do |format|
            format.html {redirect_to upload_files_url, notice: 'Upload file was successfully destroyed.'}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_upload_file
    @upload_file = UploadFile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def upload_file_params
    params.require(:upload_file).permit(:file)
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "Files"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

end
