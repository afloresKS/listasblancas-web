class DistrictsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_district, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js
  @@district = "Districts"

  def index
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Districts' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearDistrict = permisos.crear
            @editarDistrict = permisos.editar
            @leerDistrict = permisos.leer
            @eliminarDistrict = permisos.eliminar
            if @uno == @@district
              @@crear = permisos.crear
              @@editar = permisos.editar
              @@leer = permisos.leer
              @@eliminar = permisos.eliminar

              @crear = permisos.crear
              @editar = permisos.editar
              @leer = permisos.leer
              @eliminar = permisos.eliminar
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4) || (@@eliminar == 1))
            if params[:search].present?
              registro(3, params[:search]);
              busqueda = params[:search].to_s.strip
              if District.where("name like '%#{busqueda}%'").present?
                @districts = District.where("name like '%#{busqueda}%'").order("created_at DESC").page(params[:page]).per(20)
                @query = busqueda
              else
                @districts = District.order("created_at DESC").page(params[:page]).per(20)
                @error = true
              end
            else
              @districts = District.order("created_at DESC").page(params[:page]).per(20)
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorDistricts.log')
      logger.error("Error Districts en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "Districts método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def show
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Districts' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerDistrict = permisos.leer
            if @uno == @@district
              @@leer = permisos.leer
              @leer = permisos.leer
            end
          end
          if (@@leer == 2)
            registro(5, @district);
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorDistricts.log')
      logger.error("Error Districts en el metodo show #{Time.now.to_s}")
      logger.error(e)
      @metod = "Districts método show"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def new
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Districts' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearDistrict = permisos.crear
            if @uno == @@district
              @@crear = permisos.crear
              @crear = permisos.crear
            end
          end
          if (@@crear == 8)
            @district = District.new
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorDistricts.log')
      logger.error("Error Districts en el new index #{Time.now.to_s}")
      logger.error(e)
      @metod = "Districts método new"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def edit
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Districts' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @editarDistrict = permisos.editar
            if @uno == @@district
              @@editar = permisos.editar
              @editar = permisos.editar
            end
          end
          if (@@editar == 4)

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorDistricts.log')
      logger.error("Error Districts en el metodo edit #{Time.now.to_s}")
      logger.error(e)
      @metod = "Districts método edit"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def create
    begin
      registro(1, @district);
      @district = District.new(district_params)
      respond_to do |format|
        if District.where("name like ?", @district.name).exists?
          format.html {
            @error2 = true
            render action: :new
          }
        else
          if @district.save
            format.html {redirect_to :controller => 'districts', :action => 'index'}
            flash[:notice] = t('views.message_districts.new_districts')
            format.json {render :show, status: :created, location: @district}
          else
            format.html {render :new}
            format.json {render json: @district.errors, status: :unprocessable_entity}
          end
        end
      end
    rescue => e
      logger = Logger.new('log/errorDistricts.log')
      logger.error("Error Districts en el metodo create #{Time.now.to_s}")
      logger.error(e)
      @metod = "Districts método create"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def update
    begin
      registro(2, @district)
      respond_to do |format|
        @consulta = District.find_by_name(params[:district][:name])
        if @consulta.nil? || @consulta.id == @district.id
          if @district.update(district_params)
            format.html {redirect_to :controller => 'districts', :action => 'index'}
            flash[:notice] = t('views.message_districts.edit')
            format.json {render :show, status: :ok, location: @district}
          else
            format.html {render :edit}
            format.json {render json: @district.errors, status: :unprocessable_entity}
          end
        else
          format.html {
            @error2 = true
            render action: :new
          }
        end
      end
    rescue => e
      logger = Logger.new('log/errorDistricts.log')
      logger.error("Error Districts en el metodo update #{Time.now.to_s}")
      logger.error(e)
      @metod = "Districts método update"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def destroy
    if current_user
      @cu = current_user.profile_id
      if @cu != 0
        @per = Permission.where("view_name = 'Districts' and profile_id = ?", @cu)
        @per.each do |permisos|
          @uno = permisos.view_name
          @eliminarDistrict = permisos.eliminar
          if @uno == @@district
            @@eliminar = permisos.eliminar
            @eliminar = permisos.eliminar
          end
        end
        if (@@eliminar == 1)
          registro(4, @district)
          @district.destroy
          respond_to do |format|
            format.html {redirect_to districts_url, notice: t('views.message_districts.mess_dist_swall')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_district
    @district = District.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def district_params
    params.require(:district).permit(:name)
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "Districts"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end

end
