class User::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    super

    @users = User.all.count
    @usern = params[:user][:user_name]
    @usera = params[:user][:area_id]
    puts "******************************#{params[:user][:area_id]}"

    if @user.save
      @user.name = @user.name.titleize
      @user.last_name = @user.last_name.titleize
      @user.employment = @user.employment.titleize
      @user.boss_name = @user.boss_name.titleize
      @user.habilitado = 1
      @user.activo = 0
      @user.activo = 1 if @users == 1
      @user.save
      #---------------------------------------------------------------------------- ASIGNACIÓN DE PERFIL Y ENVIO DE CORREOS A USUARIOS ADMINISTRADORES

      # Si no hay usuarios, el perfil asignado es Administrador Central
      if @users == 1
        @user.profile_id = 1
        @user.save

      else # Si hay al menos un usuario
        if @usera == "1" # si selecciona Central, se asigna perfil Default de Central
          @perfil = Profile.find_by(flag: 2, area_id: @usera)

          @user.profile_id = @perfil.id
          @user.save

          # Se envía correo a los Administradores Centrales
          @admina = Profile.find_by_flag(0)
          @admin = User.where("profile_id = ?", @admina.id)
          @admin.each do |f|
            @email = f.email
            ChangePerfil.mandar_permisos_signup(@user, @email).deliver
          end

        else # Es otra área
          @area_users = User.where("area_id = ?", @usera).count

          if @area_users <= 1 # Si no existen usuarios en el área, se asigna perfil administrador del Área
            @perfil = Profile.find_by(flag: 1, area_id: @usera)

            @user.profile_id = @perfil.id
            @user.save

            # Se envía correo a los Administradores Centrales
            @admina = Profile.find_by_flag(0)
            @admin = User.where("profile_id = ?", @admina.id)
            @admin.each do |f|
              @email = f.email
              ChangePerfil.mandar_permisos_signup(@user, @email).deliver
            end

          else #Si existen usuarios, se asigna perfil Default del Área
            @perfil = Profile.find_by(flag: 2)

            @user.profile_id = 2
            @user.save

            # Se envía correo a los Administradores de Área
            @admina = Profile.find_by(flag: 1, area_id: @usera)
            @admin = User.where("profile_id = ?", @admina.id)
            @admin.each do |f|
              @email = f.email
              ChangePerfil.mandar_permisos_signup(@user, @email).deliver
            end
          end
        end
      end
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    super
  end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    super(resource)
  end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    super(resource)
  end
end
