class HistoricsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_historic, only: [:show, :edit, :update, :destroy]
  before_action :auth, :permisos_vistas, :horarios
  require 'time'
  # GET /historics
  # GET /historics.json
  def index
    if @permisos[2] == 2
      if params[:search].present? || params[:historic].present? || params[:start].present? || params[:end].present?
        @consulta = ""
        @busqueda = ""
        @registro = ""
        if params[:search].present?
          @busqueda = params[:search].gsub(/[^a-zA-Z0-9 ]/, '');
          @consulta = @consulta + "user_name LIKE '%#{@busqueda}%'"
          @registro = @registro + "User: #{@busqueda}"
        end
        if params[:historic][:action].present?
          @consulta = @consulta + " AND" if @consulta.present?
          @consulta = @consulta + " action = '#{params[:historic][:action]}'"
          @registro = @registro + " Action: #{params[:historic][:page]}"
        end
        if params[:start].present?
          @consulta = @consulta + " AND" if @consulta.present?
          @consulta = @consulta + " date >= '#{params[:start].to_date.strftime("%Y-%m-%d")}'"
          @registro = @registro + " Start date: #{params[:start]}"
        end
        if params[:end].present?
          @consulta = @consulta + " AND" if @consulta.present?
          @consulta = @consulta + " date <= '#{params[:end].to_date.strftime("%Y-%m-%d")}'"
          @registro = @registro + " End date: #{params[:end]}"
        end
        if params[:historic][:page].present?
          @consulta = @consulta + " AND" if @consulta.present?
          @consulta = @consulta + " page = '#{params[:historic][:page]}'"
          @registro = @registro + " Page: #{params[:historic][:page]}"
        end
        registro(3, @registro)
        puts "Query #{@consulta}"
        @historics = Historic.where("#{@consulta}").page(params[:page]).order(date: :desc).per(10)
        @error = 1 if @historics.empty?
      else
        @historics = Historic.all.page(params[:page]).order(date: :desc).per(10)
      end
    else
      redirect_to message_path, :alert => t('all.not_access')
    end
  end


  def show
    redirect_to no_permission_index_path, :alert => t('all.not_access')
  end

  def new
    redirect_to no_permission_index_path, :alert => t('all.not_access')
  end

  def edit
    redirect_to no_permission_index_path, :alert => t('all.not_access')
  end

  def create
    redirect_to no_permission_index_path, :alert => t('all.not_access')
  end

  def update
    redirect_to no_permission_index_path, :alert => t('all.not_access')
  end

  def destroy
    redirect_to no_permission_index_path, :alert => t('all.not_access')
  end

  private

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "Binnacle"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_historic
    @historic = Historic.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def historic_params
    params.require(:historic).permit(:string, :string, :string, :interger, :string)
  end

  def permisos_vistas
    # se verifican los permisos de la vista y se almacenan
    consulta = Permission.where("view_id = 14 and profile_id = ?", current_user.profile_id)
    consulta.each do |permisos|
      @permisos = Array[permisos.crear, permisos.editar, permisos.leer, permisos.eliminar, permisos.horainicial, permisos.horafinal]
    end
  end

  def horarios
    unless Time.now >= Time.parse(@permisos[4].strftime("%H:%M")) && Time.now <= Time.parse(@permisos[5].strftime("%H:%M")) || @permisos[5].strftime("%H:%M") == "00:00" || @permisos[4].strftime("%H:%M") == "00:00"
      redirect_to message_index_path, :alert => t('all.not_access');
    end
  end

  def auth
    unless current_user && current_user.habilitado != 0 && current_user.activo != 0 then
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end
end
