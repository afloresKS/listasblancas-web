class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  protect_from_forgery prepend: true
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :set_locale

  private
  def set_locale
    I18n.locale = params[:locale] #|| I18n.default_locale
    #Rails.application.routes.default_url_options[:locale]= I18n.locale
  end

  def self.default_url_options(options={})
    options.merge({ :locale => I18n.locale })
  end

  protected

  def configure_permitted_parameters
    @rails = Gem::Specification.find_by_name('rails')
    @railsn = @rails.version

    @version = @railsn.to_s.split('.')[0]

    if @version == "5"
      puts '5'
      # devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :password, :password_confirmation, :profile_id, :area_id) }
      # devise_parameter_sanitizer.permit(:sign_in) { |u| u.permit(:login, :user_name, :email, :password, :remember_me, :birthday, :profile_id) }
      # devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :current_password, :profile_id, :area_id) }

      devise_parameter_sanitizer.permit(:sign_up, keys: [:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :password, :password_confirmation, :area_id])
      devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :user_name, :email, :password, :remember_me, :birthday, :profile_id])
      devise_parameter_sanitizer.permit(:account_update, keys: [:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :current_password, :profile_id, :area_id])

      # devise_parameter_sanitizer.permit(:sign_up, keys: [:user_name, :name, :email, :password, :password_confirmation, :last_name, :birthday, :boss_name, :phone, :ext, :name_profile_id, :area, :employment])
      # devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :user_name, :email, :password, :remember_me, :birthday, :name_profile_id])
      # devise_parameter_sanitizer.permit(:account_update, keys: [:username, :name, :email, :last_name, :birthday, :boss_name, :phone, :ext, :area, :employment, :profile_id, :current_password, :name_profile_id])


    elsif @version == "4"
      puts '4'
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :password, :password_confirmation, :profile_id, :area_id) }
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :user_name, :email, :password, :remember_me, :birthday, :profile_id) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:user_name, :name, :last_name, :email, :birthday, :boss_name, :employment, :phone, :ext, :current_password, :profile_id, :area_id) }
    end
  end
end
