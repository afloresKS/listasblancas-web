class PtlfsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_ptlf, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js
  require 'action_view'
  include ActionView::Helpers::NumberHelper

  @@ptlf = "Transactions"
  # GET /cards
  # GET /cards.json
  def index
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Transactions' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerPtlf = permisos.leer
            @editarPtlf = permisos.editar
            if @uno == @@ptlf
              @@leer = permisos.leer
              @@editar = permisos.editar
              @leer = permisos.leer
              @editar = permisos.editar
            end
          end
          if ((@@leer == 2) || (@editar == 4))
            @last = Date.today.beginning_of_month.last_month
            @yesterday = Date.today - 1
            @month = Date.today.beginning_of_month
            @today = Date.today

            if (params.has_key?(:id))

              @client_id = params[:client_id]

              ptlfs_id = params[:id]
              @ptlf_id = ptlfs_id
              @all_ptlf = Ptlf.where(:card_id => @ptlf_id)

              @all_ptlf = @all_ptlf.order("created_at DESC").page(params[:page]).per(20)

            elsif (params.has_key?(:che_dev_mes))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (devolution= ?)", @last.to_s, @month.to_s, true).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:che_dev_hoy))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at <= ? ) AND (devolution= ?)", @month.to_s, @today.to_s, true).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:che_dev_ayer))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (devolution= ?) ", @yesterday.to_s, @today.to_s, true).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:che_dev_aldia))
              @all_ptlf = Ptlf.where("created_at >= ? AND devolution= ?", Time.now.to_date.to_s, true).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:recove_aproval_mes))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @last.to_s, @month.to_s, true, true).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:recove_aproval_hoy))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at <= ? )  AND (devolution = ?) AND (recovered = ?)", @month.to_s, @today.to_s, true, true).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:recove_aproval_ayer))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @yesterday.to_s, @today.to_s, true, true).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:recove_aproval_ahoy))
              @all_ptlf = Ptlf.where("(created_at >= ?)  AND (devolution = ?) AND (recovered = ?)", @today.to_s, true, true).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:recu_recove_aproval_mes))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @last.to_s, @month.to_s, true, false).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:recu_recove_aproval_hoy))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at <= ? )  AND (devolution = ?) AND (recovered = ?)", @month.to_s, @today.to_s, true, false).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:recu_recove_aproval_ayer))
              @all_ptlf = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @yesterday.to_s, @today.to_s, true, false).order("created_at DESC").page(params[:page]).per(20)
            elsif (params.has_key?(:recu_recove_aproval_ahoy))
              @all_ptlf = Ptlf.where("(created_at >= ?)  AND (devolution = ?) AND (recovered = ?)", @today.to_s, true, false).order("created_at DESC").page(params[:page]).per(20)
            else

              @all_ptlf = Ptlf.all
              if params.present?
                registro(3, params);
                consulta = ""
                if params[:name].present?
                  @nombre = params[:name].to_s.strip
                  clientes = Client.where("name like '%#{@nombre}%'")
                  tar_id = ""
                  clientes.each do |tarjeta|
                    tarjeta.cards.each do |datos|
                      if tar_id.bytesize == 0
                        tar_id = " (card_id = '#{datos.id}'"
                      else
                        tar_id += " OR card_id = '#{datos.id}'"
                      end
                    end
                  end
                  if tar_id.bytesize > 0
                    tar_id += ") "
                  end
                  if consulta.bytesize == 0
                    consulta = tar_id
                  else
                    consulta += "AND #{tar_id}"
                  end
                  puts consulta
                  if consulta.bytesize > 0
                    @all_ptlf = Ptlf.where(consulta).order("created_at DESC").page(params[:page]).per(20)
                    @query = params[:name]
                  else
                    @error = true
                  end
                end
                if params[:account].present?
                  if consulta.bytesize == 0
                    if !Card.find_by_number_card(params[:account]).nil?
                      @all_ptlf = Card.find_by_number_card(params[:account]).ptlfs
                      @all_ptlf = @all_ptlf.order("created_at DESC").page(params[:page]).per(20)
                      @query = params[:account]
                    else
                      @error = true
                    end
                  else
                    if !Card.find_by_number_card(params[:account]).nil?
                      @all_ptlf = Card.find_by_number_card(params[:account]).ptlfs.where(consulta)
                      @all_ptlf = @all_ptlf.order("created_at DESC").page(params[:page]).per(20)
                      @query = params[:account]
                    else
                      @error = true
                    end
                  end
                end
              end
              @all_ptlf = @all_ptlf.order("created_at DESC").page(params[:page]).per(20)
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorptlfs.log')
      logger.error("Error ptlfs en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "ptlfs método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # Vista De Reportes
  def filtro
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Transactions' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerPtlf = permisos.leer
            @crearPtlf = permisos.crear
            @editarPtlf = permisos.editar
            if @uno == @@ptlf
              @@leer = permisos.leer
              @@crear = permisos.crear
              @@editar = permisos.editar

              @leer = permisos.leer
              @crear = permisos.crear
              @editar = permisos.editar
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@editar = 4))

            #-------------------------------------------- BUSQUEDA ----------------------------------------------#
            if params[:Search].present?
              registro(3, params[:search]);
              @Contract = params[:Contract]
              @Card_Type = params[:Card_Type]
              @Region = params[:Region]
              @District = params[:District]
              @Store = params[:Store]
              @Response = params[:Response]
              @Tran_Res = params[:Tran_Res]
              @Devolution = params[:Devolution]
              @Check = params[:Check]
              @Transaction = params[:Transaction]

              @DateIni = "#{params[:DateIni].to_s.to_datetime.strftime("%Y-%d-%m")} 00:00:00.000"
              @DateFin = "#{params[:DateFin].to_s.to_datetime.strftime("%Y-%d-%m")} 23:59:59.999"

              @Name = params[:Name]
              @Account = params[:Account]

              consulta = ""
              filtro = ""
              response = ""
              devolution = ""
              check = ""
              transaction = ""
              id_cuenta = ""

              #-------------------------------------------- Número de Contrato ----------------------------------------------#
              if @Contract.present?
                if consulta.bytesize == 0
                  consulta += "(affiliation_id = '" + @Contract.to_s + "' )"
                  contrato = @Contract
                  filtro = "Contract number: " + contrato + " "
                  if consulta == ""
                    @error = true
                  end
                end
              end

              #-------------------------------------------- Tipo de Cuenta ----------------------------------------------#
              if @Card_Type.present?
                if consulta.bytesize == 0
                  cuenta = Card.where("cardType_id = #{@Card_Type}").limit(5000)
                  if cuenta.present?
                    cuenta.each do |cue_ntas|
                      if id_cuenta.bytesize == 0
                        id_cuenta = " card_id = #{cue_ntas.id} "
                        @@n_t = CardType.where("id = #{cue_ntas.cardType_id}")
                      else
                        id_cuenta += " or card_id = #{cue_ntas.id} "
                      end
                    end
                    consulta = id_cuenta
                    filtro = "Account Type: " + @@n_t.name
                  else
                    filtro = t('views.ptlfs_filtro.type')
                  end
                end
              end

              #-------------------------------------------- Región ----------------------------------------------#
              if @Region.present?
                if consulta.bytesize == 0
                  aff = Affiliation.where('region_id = ' + @Region.to_s)
                  region_id = ""
                  region_name = ""
                  aff.each do |region|
                    if region_id.bytesize == 0
                      region_id = "(affiliation_id = " + region.affiliation_id.to_s
                      region_name = region.region.name.to_s
                    else
                      region_id += " OR affiliation_id = " + region.affiliation_id.to_s
                      regio_name = region.region.name.to_s
                    end
                  end
                  if region_id.bytesize > 0
                    region_id += ")"
                  end
                  consulta = region_id
                  filtro += "Region: " + region_name.to_s
                else
                  filtro += ", Region: " + region_name.to_s + " "
                end
              end

              #-------------------------------------------- Distrito ----------------------------------------------#
              if @District.present?
                if consulta.bytesize == 0
                  aff = Affiliation.where('district_id = ' + @District.to_s)
                  district_id = ""
                  district_name = ""
                  aff.each do |distrito|
                    if district_id.bytesize == 0
                      district_id = "(affiliation_id = " + distrito.affiliation_id.to_s
                      district_name = distrito.district.name.to_s
                    else
                      district_id += " OR affiliation_id = " + distrito.affiliation_id.to_s
                      district_name = distrito.district.name.to_s + " "
                    end
                  end
                  if district_id.bytesize > 0
                    district_id += ")"
                  end
                  consulta = district_id
                  filtro += "District: " + district_name.to_s
                else
                  filtro += ", District: " + district_name.to_s + " "
                end
              end

              #-------------------------------------------- Tienda ----------------------------------------------#
              if @Store.present?
                if consulta.bytesize == 0
                  aff = Affiliation.where('id like ' + @Store.to_s)
                  store_id = ""
                  store_name = ""
                  aff.each do |tienda|
                    if store_id.bytesize == 0
                      store_id = "(affiliation_id = " + tienda.affiliation_id.to_s
                      store_name = tienda.name
                    else
                      store_id = " OR affiliation_id = " + tienda.affiliation_id.to_s
                      store_name = tienda.name
                    end
                  end
                  if store_id.bytesize > 0
                    store_id += ")"
                  end
                  consulta = store_id
                  filtro += "Store: " + store_name.to_s
                else
                  filtro += ", Store: " + store_name.to_s + " "
                end
              end

              #-------------------------------------------- Número de Transacción ----------------------------------------------#
              if params[:NumTrans].present?
                if consulta.bytesize == 0
                  aff = Affiliation.where('number like ' + params[:NumTrans].to_s)
                  numstore_id = ""
                  numstore_name = ""
                  aff.each do |tienda|
                    if numstore_id.bytesize == 0
                      numstore_id = "(affiliation_id = " + tienda.affiliation_id.to_s
                      numstore_name = tienda.name
                    else
                      numstore_id = " OR affiliation_id = " + tienda.affiliation_id.to_s
                      numstore_name = tienda.name
                    end
                  end
                  if numstore_id.bytesize > 0
                    numstore_id += ")"
                  end
                  consulta = numstore_id
                  filtro += "Number Store: " + numstore_name.to_s
                else
                  filtro += ", Number Store: " + numstore_name.to_s + " "
                end
              end

              #-------------------------------------------- Código de Respuesta ----------------------------------------------#
              if @Response.present?
                if consulta.bytesize == 0
                  consulta += "(response_code = '" + @Response + "' )"
                  if @Response == "02"
                    response = "Approval"
                  elsif @Response == "13"
                    response = "Invalid amount"
                  elsif @Response == "14"
                    response = "Invalid account"
                  elsif @Response == "12"
                    response = "Invalid transaction"
                  elsif @Response == "51"
                    response = "Insufficient balance"
                  elsif @Response == "53"
                    response = "Nonexistent account"
                  elsif @Response == "61"
                    response = "Exceeds floor limit"
                  elsif @Response == "76"
                    response = "Blocked account"
                  elsif @Response == "93"
                    response = "Operation unavailable"
                  end
                  filtro += "Response code: " + response + " "
                  if consulta.nil?
                    flash[:alert] = t('views.ptlfs_filtro.the_Filter')
                  end
                else
                  consulta += "AND (response_code = '" + @Response + "' )"
                  if @Response == "02"
                    response = "Approval"
                  elsif @Response == "13"
                    response = "Invalid amount"
                  elsif @Response == "14"
                    response = "Invalid account"
                  elsif @Response == "12"
                    response = "Invalid transaction"
                  elsif @Response == "51"
                    response = "Insufficient balance"
                  elsif @Response == "53"
                    response = "Nonexistent account"
                  elsif @Response == "61"
                    response = "Exceeds floor limit"
                  elsif @Response == "76"
                    response = "Blocked account"
                  elsif @Response == "93"
                    response = "Operation unavailable"
                  end
                  filtro += ", Response code: " + response + " "
                end
              end

              #-------------------------------------------- Resultados de la Transacción ----------------------------------------------#
              if @Tran_Res.present?
                if consulta.bytesize == 0
                  if @Tran_Res == "1"
                    consulta += "(response_code = 2 )"
                    filtro += "Transaction Results: Authorized "
                  elsif @Tran_Res == "2"
                    consulta += "(response_code = 13 or response_code = 51 or response_code = 53 or response_code = 61 or response_code = 76 or response_code = 93 )"
                    filtro += "Transaction Results: Rejected "
                  elsif @Tran_Res == "3"
                    consulta += "(response_code = 14 or response_code = 12 )"
                    filtro += "Transaction Results: NA "
                  end
                else
                  if @Tran_Res == "1"
                    consulta += "AND (response_code = 2 )"
                    filtro += "Transaction Results: Authorized "
                  elsif @Tran_Res == "2"
                    consulta += "AND (response_code = 13 or response_code = 51 or response_code = 53 or response_code = 61 or response_code = 76 or response_code = 93 )"
                    filtro += "Transaction Results: Rejected "
                  elsif @Tran_Res == "3"
                    consulta += "AND (response_code = 14 or response_code = 12 )"
                    filtro += "Transaction Results: NA "
                  end
                end
              end

              #-------------------------------------------- Devolución ----------------------------------------------#
              if @Devolution.present?
                if consulta.bytesize == 0
                  consulta += "(devolution = '" + @Devolution + "' )"
                  devolution = @Devolution.to_s
                  if devolution == 'true'
                    dev = "Returned"
                  elsif devolution == 'false'
                    dev = "Not Returned"
                  end
                  filtro += "Devolution Status: " + dev.to_s + " "
                else
                  consulta += "AND (devolution = '" + @Devolution + "' )"
                  if devolution == 'true'
                    dev = "Returned"
                  elsif devolution == 'false'
                    dev = "Not Returned"
                  end
                  filtro += ", Devolution estatus: " + dev.to_s + " "
                end
              end

              #-------------------------------------------- Comprobación de Estado ----------------------------------------------#
              if @Check.present?
                if consulta.bytesize == 0
                  consulta += "(returned_balance = '" + @Check.to_s + "' )"
                  check = @Check.to_s
                  if check == 'true'
                    chec = "Cashed"
                  elsif check == 'false'
                    chec = "In process"
                  end
                  filtro += "Check Status: " + chec.to_s + " "
                else
                  consulta += "AND (returned_balance = '" + @Check.to_s + "' )"
                  if check == 'true'
                    chec = "Cashed"
                  elsif check == 'false'
                    chec = "In process"
                  end
                  filtro += ", Check Status: " + chec.to_s + " "
                end
              end

              #-------------------------------------------- Tipo de Transacción ----------------------------------------------#
              if @Transaction.present?
                if consulta.bytesize == 0
                  consulta += " (transacctionType_id = '" + @Transaction + "' )"
                  if @Transaction == "1"
                    transaction = "Venta"
                  elsif @Transaction == "2"
                    transaction = "Cancelacion"
                  end
                  filtro += "Transaction Type: " + transaction + " "
                else
                  consulta += "AND ( transacctionType_id = '" + @Transaction + "' )"
                  if @Transaction == "1"
                    transaction = "Venta"
                  elsif @Transaction == "2"
                    transaction = "Cancelacion"
                  end
                  filtro += ", Transaction Type: " + transaction + " "
                end
              end

              #-------------------------------------------- Fecha inicial ----------------------------------------------#
              if @DateIni.present?
                if @DateFin.present?
                  if consulta.bytesize == 0
                    consulta += "created_at between Convert(datetime,'#{@DateIni}',103) and Convert(datetime,'#{@DateFin}',103)"
                    dateIni = @DateIni.to_s
                    dateFin = @DateFin.to_s
                    filtro += "Start Date: " + dateIni + ", End Date: " + dateFin + " "
                  else
                    consulta += "created_at between Convert(datetime,'#{@DateIni}',103) and Convert(datetime,'#{@DateFin}',103)"
                    dateIni = @DateIni.to_s
                    dateFin = @DateFin.to_s
                    filtro += ", Start Date: " + dateIni + ", End Date: " + dateFin + " "
                  end
                end
              end

              #-------------------------------------------- Número de Cuenta ----------------------------------------------#
              if @Account.present?
                if consulta.bytesize == 0
                  if !Card.find_by_number_card(@Account).nil?
                    @ptlfs = Card.find_by_number_card(@Account).ptlfs.page(params[:page]).per(20)
                    account = @Account
                    filtro += "Account: " + account + " "
                  end
                  if @ptlfs.nil?
                    @error = true
                  end
                else
                  if !Card.find_by_number_card(@Account).nil?
                    @ptlfs = Card.find_by_number_card(@Account).ptlfs.where(consulta).page(params[:page]).per(20)
                    account = @Account
                    filtro += ", Account: " + account + " "
                  end
                  if @ptlfs.nil?
                    @error = true
                  end
                end
              elsif @Name.present?
                clientes = Client.where("name like '%" + @Name.to_s + "%' OR last_name like '%" + @Name.to_s + "%'")
                name = clientes
                filtro = "Customer Name: " + name.to_s + " "
                tarjetas_id = ""
                nombre = ""
                clientes.each do |tarjeta|
                  nombre = tarjeta.name.to_s + " " + tarjeta.last_name.to_s
                  tarjeta.cards.each do |datos|
                    if tarjetas_id.bytesize == 0
                      tarjetas_id = " (card_id = " + datos.id.to_s
                    else
                      tarjetas_id += " OR card_id = " + datos.id.to_s
                    end
                  end
                end
                if tarjetas_id.bytesize > 0
                  tarjetas_id += ") "
                end
                if consulta.bytesize == 0
                  consulta = tarjetas_id
                  filtro = "Customer Name: " + nombre.to_s + " "
                else
                  consulta += "AND " + tarjetas_id
                  filtro += ", Customer Name: " + nombre.to_s + " "
                end
                if consulta.bytesize > 0
                  @ptlfs = Ptlf.where(consulta).page(params[:page]).per(20)
                  @filtro = filtro
                  if @ptlfs.nil?
                    @error = true
                  end
                else
                  if @ptlfs.nil?
                    @error = true
                  end
                end
              else
                if consulta.bytesize > 0
                  @ptlfs = Ptlf.where(consulta).page(params[:page]).per(20)
                  @filtro = filtro
                  if @ptlfs.nil?
                    @error = true
                  end
                else
                  @filtro = filtro
                  @error = true
                end
              end
            end

            if params[:xls_g].present?

              Thread.new do
                begin
                  puts red('Comienza hilo --Transacciones Listas Blancas')
                  @user_id = params[:user_id]
                  @idioma = params[:idioma]
                  @Contract = params[:Contract]
                  @Card_Type = params[:Card_Type]
                  @Region = params[:Region]
                  @District = params[:District]
                  @Store = params[:Store]
                  @Response = params[:Response]
                  @Tran_Res = params[:Tran_Res]
                  @Devolution = params[:Devolution]
                  @Check = params[:Check]
                  @Transaction = params[:Transaction]
                  @DateIni = params[:DateIni]
                  @DateFin = params[:DateFin]
                  @Name = params[:Name]
                  @Account = params[:Account]

                  consulta = ""
                  filtro = ""
                  response = ""
                  devolution = ""
                  check = ""
                  transaction = ""
                  id_cuenta = ""

                  #-------------------------------------------- Número de Contrato ----------------------------------------------#
                  if @Contract.present?
                    if consulta.bytesize == 0
                      consulta += "(affiliation_id = '" + @Contract.to_s + "' )"
                      contrato = @Contract
                      filtro = "Contract number: " + contrato + " "
                      if consulta == ""
                        flash[:alert] = t('views.ptlfs_filtro.the_Filter')
                      end
                    end
                  end

                  #-------------------------------------------- Tipo de Cuenta ----------------------------------------------#
                  if @Card_Type.present?
                    if consulta.bytesize == 0
                      cuenta = Card.where("cardType_id = #{@Card_Type}").limit(5000)
                      if cuenta.present?
                        cuenta.each do |cue_ntas|
                          if id_cuenta.bytesize == 0
                            id_cuenta = " card_id = #{cue_ntas.id} "
                            @@n_t = CardType.where("id = #{cue_ntas.cardType_id}")
                          else
                            id_cuenta += " or card_id = #{cue_ntas.id} "
                          end
                        end
                        consulta = id_cuenta
                        filtro = "Account Type: " + @@n_t.name
                      else
                        filtro = t('views.ptlfs_filtro.type')
                      end
                    end
                  end

                  #-------------------------------------------- Región ----------------------------------------------#
                  if @Region.present?
                    if consulta.bytesize == 0
                      aff = Affiliation.where('region_id = ' + @Region.to_s)
                      region_id = ""
                      region_name = ""
                      aff.each do |region|
                        if region_id.bytesize == 0
                          region_id = "(affiliation_id = " + region.affiliation_id.to_s
                          region_name = region.region.name.to_s
                        else
                          region_id += " OR affiliation_id = " + region.affiliation_id.to_s
                          regio_name = region.region.name.to_s
                        end
                      end
                      if region_id.bytesize > 0
                        region_id += ")"
                      end
                      consulta = region_id
                      filtro += "Region: " + region_name.to_s
                    else
                      filtro += ", Region: " + region_name.to_s + " "
                    end
                  end

                  #-------------------------------------------- Distrito ----------------------------------------------#
                  if @District.present?
                    if consulta.bytesize == 0
                      aff = Affiliation.where('district_id = ' + @District.to_s)
                      district_id = ""
                      district_name = ""
                      aff.each do |distrito|
                        if district_id.bytesize == 0
                          district_id = "(affiliation_id = " + distrito.affiliation_id.to_s
                          district_name = distrito.district.name.to_s
                        else
                          district_id += " OR affiliation_id = " + distrito.affiliation_id.to_s
                          district_name = distrito.district.name.to_s + " "
                        end
                      end
                      if district_id.bytesize > 0
                        district_id += ")"
                      end
                      consulta = district_id
                      filtro += "District: " + district_name.to_s
                    else
                      filtro += ", District: " + district_name.to_s + " "
                    end
                  end

                  #-------------------------------------------- Tienda ----------------------------------------------#
                  if @Store.present?
                    if consulta.bytesize == 0
                      aff = Affiliation.where('id like ' + @Store.to_s)
                      store_id = ""
                      store_name = ""
                      aff.each do |tienda|
                        if store_id.bytesize == 0
                          store_id = "(affiliation_id = " + tienda.affiliation_id.to_s
                          store_name = tienda.name
                        else
                          store_id = " OR affiliation_id = " + tienda.affiliation_id.to_s
                          store_name = tienda.name
                        end
                      end
                      if store_id.bytesize > 0
                        store_id += ")"
                      end
                      consulta = store_id
                      filtro += "Store: " + store_name.to_s
                    else
                      filtro += ", Store: " + store_name.to_s + " "
                    end
                  end

                  #-------------------------------------------- Número de Transacción ----------------------------------------------#
                  if params[:NumTrans].present?
                    if consulta.bytesize == 0
                      aff = Affiliation.where('number like ' + params[:NumTrans].to_s)
                      numstore_id = ""
                      numstore_name = ""
                      aff.each do |tienda|
                        if numstore_id.bytesize == 0
                          numstore_id = "(affiliation_id = " + tienda.affiliation_id.to_s
                          numstore_name = tienda.name
                        else
                          numstore_id = " OR affiliation_id = " + tienda.affiliation_id.to_s
                          numstore_name = tienda.name
                        end
                      end
                      if numstore_id.bytesize > 0
                        numstore_id += ")"
                      end
                      consulta = numstore_id
                      filtro += "Number Store: " + numstore_name.to_s
                    else
                      filtro += ", Number Store: " + numstore_name.to_s + " "
                    end
                  end

                  #-------------------------------------------- Código de Respuesta ----------------------------------------------#
                  if @Response.present?
                    if consulta.bytesize == 0
                      consulta += "(response_code = '" + @Response + "' )"
                      if @Response == "02"
                        response = "Approval"
                      elsif @Response == "13"
                        response = "Invalid amount"
                      elsif @Response == "14"
                        response = "Invalid account"
                      elsif @Response == "12"
                        response = "Invalid transaction"
                      elsif @Response == "51"
                        response = "Insufficient balance"
                      elsif @Response == "53"
                        response = "Nonexistent account"
                      elsif @Response == "61"
                        response = "Exceeds floor limit"
                      elsif @Response == "76"
                        response = "Blocked account"
                      elsif @Response == "93"
                        response = "Operation unavailable"
                      end
                      filtro += "Response code: " + response + " "
                      if consulta.nil?
                        flash[:alert] = t('views.ptlfs_filtro.the_Filter')
                      end
                    else
                      consulta += "AND (response_code = '" + @Response + "' )"
                      if @Response == "02"
                        response = "Approval"
                      elsif @Response == "13"
                        response = "Invalid amount"
                      elsif @Response == "14"
                        response = "Invalid account"
                      elsif @Response == "12"
                        response = "Invalid transaction"
                      elsif @Response == "51"
                        response = "Insufficient balance"
                      elsif @Response == "53"
                        response = "Nonexistent account"
                      elsif @Response == "61"
                        response = "Exceeds floor limit"
                      elsif @Response == "76"
                        response = "Blocked account"
                      elsif @Response == "93"
                        response = "Operation unavailable"
                      end
                      filtro += ", Response code: " + response + " "
                    end
                  end

                  #-------------------------------------------- Resultados de la Transacción ----------------------------------------------#
                  if @Tran_Res.present?
                    if consulta.bytesize == 0
                      if @Tran_Res == "1"
                        consulta += "(response_code = 2 )"
                        filtro += "Transaction Results: Authorized "
                      elsif @Tran_Res == "2"
                        consulta += "(response_code = 13 or response_code = 51 or response_code = 53 or response_code = 61 or response_code = 76 or response_code = 93 )"
                        filtro += "Transaction Results: Rejected "
                      elsif @Tran_Res == "3"
                        consulta += "(response_code = 14 or response_code = 12 )"
                        filtro += "Transaction Results: NA "
                      end
                    else
                      if @Tran_Res == "1"
                        consulta += "AND (response_code = 2 )"
                        filtro += "Transaction Results: Authorized "
                      elsif @Tran_Res == "2"
                        consulta += "AND (response_code = 13 or response_code = 51 or response_code = 53 or response_code = 61 or response_code = 76 or response_code = 93 )"
                        filtro += "Transaction Results: Rejected "
                      elsif @Tran_Res == "3"
                        consulta += "AND (response_code = 14 or response_code = 12 )"
                        filtro += "Transaction Results: NA "
                      end
                    end
                  end

                  #-------------------------------------------- Devolución ----------------------------------------------#
                  if @Devolution.present?
                    if consulta.bytesize == 0
                      consulta += "(devolution = '" + @Devolution + "' )"
                      devolution = @Devolution.to_s
                      if devolution == 'true'
                        dev = "Returned"
                      elsif devolution == 'false'
                        dev = "Not Returned"
                      end
                      filtro += "Devolution Status: " + dev.to_s + " "
                    else
                      consulta += "AND (devolution = '" + @Devolution + "' )"
                      if devolution == 'true'
                        dev = "Returned"
                      elsif devolution == 'false'
                        dev = "Not Returned"
                      end
                      filtro += ", Devolution estatus: " + dev.to_s + " "
                    end
                  end

                  #-------------------------------------------- Comprobación de Estado ----------------------------------------------#
                  if @Check.present?
                    if consulta.bytesize == 0
                      consulta += "(returned_balance = '" + @Check.to_s + "' )"
                      check = @Check.to_s
                      if check == 'true'
                        chec = "Cashed"
                      elsif check == 'false'
                        chec = "In process"
                      end
                      filtro += "Check Status: " + chec.to_s + " "
                    else
                      consulta += "AND (returned_balance = '" + @Check.to_s + "' )"
                      if check == 'true'
                        chec = "Cashed"
                      elsif check == 'false'
                        chec = "In process"
                      end
                      filtro += ", Check Status: " + chec.to_s + " "
                    end
                  end

                  #-------------------------------------------- Tipo de Transacción ----------------------------------------------#
                  if @Transaction.present?
                    if consulta.bytesize == 0
                      consulta += " (transacctionType_id = '" + @Transaction + "' )"
                      if @Transaction == "1"
                        transaction = "Venta"
                      elsif @Transaction == "2"
                        transaction = "Cancelacion"
                      end
                      filtro += "Transaction Type: " + transaction + " "
                    else
                      consulta += "AND ( transacctionType_id = '" + @Transaction + "' )"
                      if @Transaction == "1"
                        transaction = "Venta"
                      elsif @Transaction == "2"
                        transaction = "Cancelacion"
                      end
                      filtro += ", Transaction Type: " + transaction + " "
                    end
                  end

                  #-------------------------------------------- Fecha inicial ----------------------------------------------#
                  if @DateIni.present?
                    if @DateFin.present?
                      if consulta.bytesize == 0
                        consulta += "created_at >=  '" + @DateIni.to_s + "' and created_at <= ' " + @DateFin.to_s + " 23:59:59' "
                        dateIni = @DateIni.to_s
                        dateFin = @DateFin.to_s
                        filtro += "Start Date: " + dateIni + ", End Date: " + dateFin + " "
                      else
                        consulta += "AND (created_at >= '" + @DateIni.to_s + "' and created_at <= ' " + @DateFin.to_s + " 23:59:59' )"
                        dateIni = @DateIni.to_s
                        dateFin = @DateFin.to_s
                        filtro += ", Start Date: " + dateIni + ", End Date: " + dateFin + " "
                      end
                    end
                  end

                  #-------------------------------------------- Número de Cuenta ----------------------------------------------#
                  if @Account.present?
                    if consulta.bytesize == 0
                      if !Card.find_by_number_card(@Account).nil?
                        @ptlfs = Card.find_by_number_card(@Account).ptlfs.page(params[:page]).per(20)
                        account = @Account
                        filtro += "Account: " + account + " "
                      end
                      if @ptlfs.nil?
                        flash[:alert] = t('views.ptlfs_filtro.the_Filter')
                      end
                    else
                      if !Card.find_by_number_card(@Account).nil?
                        @ptlfs = Card.find_by_number_card(@Account).ptlfs.where(consulta).page(params[:page]).per(20)
                        account = @Account
                        filtro += ", Account: " + account + " "
                      end
                      if @ptlfs.nil?
                        flash[:alert] = t('views.ptlfs_filtro.the_Filter')
                      end
                    end
                  end

                  #-------------------------------------------- Nombre del Cliente ----------------------------------------------#
                  if @Name.present?
                    clientes = Client.where("name like '%" + @Name.to_s + "%' OR last_name like '%" + @Name.to_s + "%'")
                    name = clientes
                    filtro = "Customer Name: " + name.to_s + " "
                    tarjetas_id = ""
                    nombre = ""
                    clientes.each do |tarjeta|
                      nombre = tarjeta.name.to_s + " " + tarjeta.last_name.to_s
                      tarjeta.cards.each do |datos|
                        if tarjetas_id.bytesize == 0
                          tarjetas_id = " (card_id = " + datos.id.to_s
                        else
                          tarjetas_id += " OR card_id = " + datos.id.to_s
                        end
                      end
                    end
                    if tarjetas_id.bytesize > 0
                      tarjetas_id += ") "
                    end
                    if consulta.bytesize == 0
                      consulta = tarjetas_id
                      filtro = "Customer Name: " + nombre.to_s + " "
                    else
                      consulta += "AND " + tarjetas_id
                      filtro += ", Customer Name: " + nombre.to_s + " "
                    end
                    if consulta.bytesize > 0
                      @ptlfs = Ptlf.where(consulta).page(params[:page]).per(20)
                      @filtro = filtro
                      if @ptlfs.nil?
                        flash[:alert] = t('views.ptlfs_filtro.the_Filter')
                      end
                    else
                      if @ptlfs.nil?
                        flash[:alert] = t('views.ptlfs_filtro.the_Filter')
                      end
                    end
                  end

                  if consulta.bytesize > 0
                    @user = User.find(current_user.id)
                    @ptlfs = Ptlf.where(consulta).page(params[:page]).per(20)
                    @total = Ptlf.where(consulta)
                    @filtro = filtro
                    puts cyan('Comienza Generación de XLS-Listas-Blancas')
                    reci = CreateReport.new
                    reci.creaReporte(@total, @idioma, @user)
                  end
                end
              end

            end

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorptlfs.log')
      logger.error("Error ptlfs en el metodo filtro #{Time.now.to_s}")
      logger.error(e)
      @metod = "ptlfs método filtro"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def show
    begin
      registro(5, @ptlf);
    rescue => e
      logger = Logger.new('log/errorptlfs.log')
      logger.error("Error ptlfs en el metodo show #{Time.now.to_s}")
      logger.error(e)
      @metod = "ptlfs método show"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /cards/new
  def new
    begin
      @ptlf = Ptlf.new
    rescue => e
      logger = Logger.new('log/errorptlfs.log')
      logger.error("Error ptlfs en el metodo new #{Time.now.to_s}")
      logger.error(e)
      @metod = "ptlfs método new"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /cards/1/edit
  def edit
    begin
      @@id_client = params[:id_client]
      @id_card = params[:id_card]
      @id_client = params[:id_client]
    rescue => e
      logger = Logger.new('log/errorptlfs.log')
      logger.error("Error ptlfs en el metodo edit #{Time.now.to_s}")
      logger.error(e)
      @metod = "ptlfs método edit"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # PATCH/PUT /cards/1
  # PATCH/PUT /cards/1.json
  def update
    begin
      registro(2, @ptlf)
      id_card = params[:id_card]
      @@card = Card.find(id_card.to_i)
      @ptlf.user = current_user
      if (params[:ptlf][:returned_balance] == '1') && (@ptlf.returned_balance != true) && (@ptlf.response_code == '02') && (@ptlf.devolution != true)
        @ptlf.returned_balance = true
        if @@card.actual_balance == nil
          @@card.actual_balance = @ptlf.amount
        elsif @@card.initial_balance < @@card.actual_balance
          @@card.actual_balance = @@card.initial_balance
        else
          @@card.actual_balance += @ptlf.amount
        end

        #@@card.actual_balance = @@card.actual_balance.to_i + @ptlf.amount.to_i
      end

      respond_to do |format|

        if @ptlf.update(ptlf_params)

          @active = @ptlf.devolution
          @@activ = 0

          if @active == true
            @@card.active = false
          else
            @@card.active = true
          end
          @@card.save

          @@save = t('views.ptlfs_filtro.update')
          PtlfMailer.alert_confirmation(@ptlf, @@save).deliver
          format.html {redirect_to @ptlf, notice: t('views.ptlfs_filtro.update')}
          #format.json { render :show, status: :ok, location: @cards }
        else

          format.html {render :edit}
          format.json {render json: @ptlf.errors, status: :unprocessable_entity}
        end
      end
    rescue => e
      logger = Logger.new('log/errorptlfs.log')
      logger.error("Error ptlfs en el metodo update #{Time.now.to_s}")
      logger.error(e)
      @metod = "ptlfs método update"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # DELETE /cards/1
  # DELETE /cards/1.json
  def destroy
    registro(4, @ptlf)
    @ptlf.destroy
    respond_to do |format|
      format.html {redirect_to ptlfs_url, notice: t('views.ptlfs_filtro.destroy')}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_ptlf
    @ptlf = Ptlf.find(params[:id])
  end

  def ptlf_params
    #params.require(:ptlf).permit( :approval,:devolution , :returned_balance)
    params.require(:ptlf).permit(:devolution, :recovered, :paid_balance)
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "PTLFS"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end


end