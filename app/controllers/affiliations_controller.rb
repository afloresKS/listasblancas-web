class AffiliationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_affiliation, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js
  @@store = "Shops"

  def index
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Shops' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearStore = permisos.crear
            @editarStore = permisos.editar
            @leerStore = permisos.leer
            @eliminarStore = permisos.eliminar
            if @uno == @@store
              @@crear = permisos.crear
              @@editar = permisos.editar
              @@leer = permisos.leer
              @@eliminar = permisos.eliminar

              @crear = permisos.crear
              @editar = permisos.editar
              @leer = permisos.leer
              @eliminar = permisos.eliminar
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4) || (@@eliminar == 1))
            if params[:search].present?
              registro(3, params[:search]);
              busqueda = params[:search].strip
              if Affiliation.where("name like '%#{busqueda}%'").present?
                @affiliations = Affiliation.where("name like '%#{busqueda}%'").order("created_at DESC").page(params[:page]).per(20)
                @query = busqueda
              elsif Affiliation.where("address like '%#{busqueda}%'").present?
                @affiliations = Affiliation.where("address like '%#{busqueda}%'").order("created_at DESC").page(params[:page]).per(20)
                @query = busqueda
              elsif Region.where("name like '%#{busqueda}%'").present?
                @regi = Region.where("name like '%#{busqueda}%'")
                @regi.each do |region|
                  @affiliations = Affiliation.where("region_id = ?", region.id).order("created_at DESC").page(params[:page]).per(20)
                  @query = busqueda
                end
              elsif District.where("name like '%#{busqueda}%'").present?
                @distric = District.where("name like '%#{busqueda}%'")
                @distric.each do |dist|
                  @affiliations = Affiliation.where("district_id = ?", dist.id).order("created_at DESC").page(params[:page]).per(20)
                  @query = busqueda
                end
              else
                @affiliations = Affiliation.order("created_at DESC").page(params[:page]).per(20)
                @error = true
              end
            else
              @affiliations = Affiliation.order("created_at DESC").page(params[:page]).per(20)
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/erroraffiliations.log')
      logger.error("Error affiliations en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "affiliations método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def show
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Shops' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerStore = permisos.leer
            if @uno == @@store
              @@leer = permisos.leer

              @leer = permisos.leer
            end
          end
          if (@@leer == 2)
            registro(5, @affiliation);
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/erroraffiliations.log')
      logger.error("Error affiliations en el metodo show #{Time.now.to_s}")
      logger.error(e)
      @metod = "affiliations método show"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def new
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Shops' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearStore = permisos.crear
            if @uno == @@store
              @@crear = permisos.crear

              @crear = permisos.crear
            end
          end
          if (@@crear == 8)
            @affiliation = Affiliation.new
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/erroraffiliations.log')
      logger.error("Error affiliations en el metodo new #{Time.now.to_s}")
      logger.error(e)
      @metod = "affiliations método new"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def edit
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Shops' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @editarStore = permisos.editar
            if @uno == @@store
              @@editar = permisos.editar

              @editar = permisos.editar
            end
          end
          if (@@editar == 4)

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/erroraffiliations.log')
      logger.error("Error affiliations en el metodo edit #{Time.now.to_s}")
      logger.error(e)
      @metod = "affiliations método edit"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def create
    begin
      registro(1, @affiliation);
      @affiliation = Affiliation.new(affiliation_params)
      respond_to do |format|
        if Affiliation.where("name like ?", @affiliation.name).exists?
          format.html {
            @error2 = true
            render action: :new
          }
        else
          if @affiliation.save
            format.html {redirect_to :controller => 'affiliations', :action => 'index'}
            flash[:notice] = t('views.message_affi.new_affi')
            format.json {render :show, status: :created, location: @affiliation}
          else
            format.html {render :new}
            format.json {render json: @affiliation.errors, status: :unprocessable_entity}
          end
        end
      end
    rescue => e
      logger = Logger.new('log/erroraffiliations.log')
      logger.error("Error affiliations en el metodo create #{Time.now.to_s}")
      logger.error(e)
      @metod = "affiliations método create"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def update
    begin
      registro(2, @affiliation)
      respond_to do |format|
        @consulta = Affiliation.find_by_name(params[:affiliation][:name])
        if @consulta.nil? || @consulta.id == @affiliation.id
          if @affiliation.update(affiliation_params)
            format.html {redirect_to :controller => 'affiliations', :action => 'index'}
            flash[:notice] = t('views.message_affi.edit')
            format.json {render :show, status: :created, location: @affiliation}
          else
            format.html {render :edit}
            format.json {render json: @affiliation.errors, status: :unprocessable_entity}
          end
        else
          format.html {
            @error2 = true
            render action: :new
          }
        end
      end
    rescue => e
      logger = Logger.new('log/erroraffiliations.log')
      logger.error("Error affiliations en el metodo update #{Time.now.to_s}")
      logger.error(e)
      @metod = "affiliations método update"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def destroy
    if current_user
      @cu = current_user.profile_id
      if @cu != 0
        @per = Permission.where("view_name = 'Shops' and profile_id = ?", @cu)
        @per.each do |permisos|
          @uno = permisos.view_name
          @eliminarStore = permisos.eliminar
          if @uno == @@store
            @@eliminar = permisos.eliminar

            @eliminar = permisos.eliminar
          end
        end
        if (@@eliminar == 1)
          registro(4, @affiliation)
          @affiliation.destroy
          respond_to do |format|
            format.html {redirect_to affiliations_url, notice: t('views.message_affi.mess_affi_swall')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_affiliation
    @affiliation = Affiliation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def affiliation_params
    params.require(:affiliation).permit(:name, :address, :affiliation_id, :district_id, :region_id, :number, :port)
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "Affiiations"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end
end
