class ViewsController < ApplicationController
  before_action :set_view, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
  end

  def update
  end

  def destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_view
    @view = View.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def view_params
    params.require(:view).permit(:name, :crear, :editar, :eliminar, :leer)
  end
end
