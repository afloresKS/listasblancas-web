class HomeController < ApplicationController
  before_action :authenticate_user!
  before_action :set_locale
  @@home = "Home"
  respond_to :html, :js

  def index
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Home' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerHome = permisos.leer
            if @uno == @@home
              @@leer = permisos.leer
              @leer = permisos.leer
            end
          end
          if (@@leer == 2)
            @last = Date.today.beginning_of_month.last_month
            @yesterday = Date.today - 1
            @month = Date.today.beginning_of_month
            @today = Date.today

            fecha = Time.now.to_date - 31

            if ENV['database'] = 'SQL Server'

              @total = Ptlf.group('convert(VARCHAR(10),created_at,102)').where("created_at >= ? AND response_code='02' ", fecha.to_date.to_s).count
              @t_amount = Ptlf.group('convert(VARCHAR(10),created_at,102)').where("created_at >= ? AND response_code='02' ", fecha.to_date.to_s).sum("amount/100")
              @t_amount_porcent = Ptlf.group('convert(VARCHAR(10),created_at,103)').where("created_at >= ? AND response_code='02'", fecha.to_date.to_s).sum("(amount * 0.017)/100")

            else
              @total = Ptlf.group('date(created_at)').where('created_at >= ?', fecha.to_date.to_s).count
              @t_amount = Ptlf.group('date(created_at)').where('created_at >= ?', fecha.to_date.to_s).sum("amount/100")
              @t_amount_porcent = Ptlf.group('date(created_at)').where("created_at >= ? AND response_code='02'", fecha.to_date.to_s).sum("(amount * 0.017)/100")

            end
            @numero_clientes = Client.count

            @acc_act_agot_credito = Card.where("actual_balance = '0' ").count
            @acc_act_agot_lim_ope = Card.where("transaction_actual = '0' ").count
            @acc_act_agot_acc_bloc = Card.where("active = ? ", false).count


            @tra_apr_mes = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code='02')", @last.to_s, @month.to_s).count
            @tra_apr_ayer = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code='02') ", @yesterday.to_s, @today.to_s).count
            @tra_apr_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? )AND (response_code='02')", @month.to_s, @today.to_s).count
            @tra_apr_ahoy = Ptlf.where("created_at >= ? AND response_code='02'", Time.now.to_date.to_s).count

            @tra_amo_mes = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code='02')", @last.to_s, @month.to_s).sum("(CONVERT(bigint,amount))")
            @tra_amo_ayer = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code='02') ", @yesterday.to_s, @today.to_s).sum("(CONVERT(bigint,amount))")
            @tra_amo_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? ) AND (response_code='02')", @month.to_s, @today.to_s).sum("(CONVERT(bigint,amount))")
            @tra_amo_ahoy = Ptlf.where("created_at >= ? AND response_code='02'", Time.now.to_date.to_s).sum("(CONVERT(bigint,amount))")


            @che_dev_mes = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code='02') AND (devolution='1')", @last.to_s, @month.to_s).count
            @che_dev_ayer = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code='02') AND (devolution='1') ", @yesterday.to_s, @today.to_s).count
            @che_dev_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? ) AND (response_code='02') AND (devolution='1')", @month.to_s, @today.to_s).count
            @che_dev_aldia = Ptlf.where("created_at >= ? AND response_code='02' AND devolution='1'", Time.now.to_date.to_s).count

            @che_dev_amo_mes = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code='02') AND (devolution='1')", @last.to_s, @month.to_s).sum("(CONVERT(bigint,amount))")
            @che_dev_amo_ayer = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code='02') AND (devolution='1') ", @yesterday.to_s, @today.to_s).sum("(CONVERT(bigint,amount))")
            @che_dev_amo_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? ) AND (response_code='02') AND (devolution='1')", @month.to_s, @today.to_s).sum("(CONVERT(bigint,amount))")
            @che_dev_amo_aldia = Ptlf.where("created_at >= ? AND response_code='02' AND devolution='1'", Time.now.to_date.to_s).sum("amount")

            @che_fue_valid_mes = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code = 13 or response_code = 51 or response_code = 61 or response_code = 93)", @last.to_s, @month.to_s).count
            @che_fue_valid_ayer = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code = 13 or response_code = 51 or response_code = 61 or response_code = 93)", @yesterday.to_s, @today.to_s).count
            @che_fue_valid_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? ) AND (response_code = 13 or response_code = 51 or response_code = 61 or response_code = 93)", @month.to_s, @today.to_s).count
            @che_fue_valid_ahoy = Ptlf.where("(created_at >= ?) AND (response_code = 13 or response_code = 51 or response_code = 61 or response_code = 93)", Time.now.to_date.to_s).count


            @che_fue_amo_valid_mes = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code = 13 or response_code = 51 or response_code = 61 or response_code = 93)", @last.to_s, @month.to_s).sum("(CONVERT(bigint,amount))")
            @che_fue_amo_valid_ayer = Ptlf.where("(created_at >= ? AND created_at < ? ) AND (response_code = 13 or response_code = 51 or response_code = 61 or response_code = 93)", @yesterday.to_s, @today.to_s).sum("(CONVERT(bigint,amount))")
            @che_fue_amo_valid_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? ) AND (response_code = 13 or response_code = 51 or response_code = 61 or response_code = 93)", @month.to_s, @today.to_s).sum("(CONVERT(bigint,amount))")
            @che_fue_amo_valid_ahoy = Ptlf.where("(created_at >= ?) AND (response_code = 13 or response_code = 51 or response_code = 61 or response_code = 93)", Time.now.to_date.to_s).sum("(CONVERT(bigint,amount))")

            @acco_activ_mes = Ptlf.where("response_code = '02' AND (created_at >= ? AND created_at < ? )", @last.to_s, @month.to_s).distinct.pluck('card_id').count
            @acco_activ_ayer = Ptlf.where("response_code = '02' AND (created_at >= ? AND created_at < ? )", @yesterday.to_s, @today.to_s).distinct.pluck('card_id').count
            @acco_activ_hoy = Ptlf.where("response_code = '02' AND (created_at >= ? AND created_at <= ? )", @month.to_s, @today.to_s).distinct.pluck('card_id').count
            @acco_activ_ahoy = Ptlf.where("response_code = '02' AND created_at = ?", @today.to_s).distinct.pluck('card_id').count

            @recove_aproval_mes = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @last.to_s, @month.to_s, true, true).count
            @recove_aproval_ayer = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @yesterday.to_s, @today.to_s, true, true).count
            @recove_aproval_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? )  AND (devolution = ?) AND (recovered = ?)", @month.to_s, @today.to_s, true, true).count
            @recove_aproval_ahoy = Ptlf.where("(created_at >= ?)  AND (devolution = ?) AND (recovered = ?)", @today.to_s, true, true).count

            @recove_amo_aproval_mes = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @last.to_s, @month.to_s, true, true).sum("(CONVERT(bigint,amount))")
            @recove_amo_aproval_ayer = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @yesterday.to_s, @today.to_s, true, true).sum("(CONVERT(bigint,amount))")
            @recove_amo_aproval_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? )  AND (devolution = ?) AND (recovered = ?)", @month.to_s, @today.to_s, true, true).sum("(CONVERT(bigint,amount))")
            @recove_amo_aproval_ahoy = Ptlf.where("(created_at >= ?)  AND (devolution = ?) AND (recovered = ?)", @today.to_s, true, true).sum("(CONVERT(bigint,amount))")

            @recu_recove_aproval_mes = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @last.to_s, @month.to_s, true, false).count
            @recu_recove_aproval_ayer = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @yesterday.to_s, @today.to_s, true, false).count
            @recu_recove_aproval_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? )  AND (devolution = ?) AND (recovered = ?)", @month.to_s, @today.to_s, true, false).count
            @recu_recove_aproval_ahoy = Ptlf.where("(created_at >= ?)  AND (devolution = ?) AND (recovered = ?)", @today.to_s, true, false).count

            @recu_amo_recove_aproval_mes = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @last.to_s, @month.to_s, true, false).sum("(CONVERT(bigint,amount))")
            @recu_amo_recove_aproval_ayer = Ptlf.where("(created_at >= ? AND created_at < ? )  AND (devolution = ?) AND (recovered = ?)", @yesterday.to_s, @today.to_s, true, false).sum("(CONVERT(bigint,amount))")
            @recu_amo_recove_aproval_hoy = Ptlf.where("(created_at >= ? AND created_at <= ? )  AND (devolution = ?) AND (recovered = ?)", @month.to_s, @today.to_s, true, false).sum("(CONVERT(bigint,amount))")
            @recu_amo_recove_aproval_ahoy = Ptlf.where("(created_at >= ?)  AND (devolution = ?) AND (recovered = ?)", @today.to_s, true, false).sum("(CONVERT(bigint,amount))")
          else
            @Without_Permission = 100
            redirect_to message_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to message_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to message_index_path, :alert => t('all.please_continue')
      end
    end
  rescue => e
    logger = Logger.new('log/errorhome.log')
    logger.error("Error Home en el metodo index #{Time.now.to_s}")
    logger.error(e)
    @metod = "Home método index"
    file = "#{Rails.root}/app/views/errors/file.txt"
    File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
    @error = "#{e.message }"
    file_send
    if e.message == "ActionController::UnknownFormat"
      render_404
    else
      render_500
    end
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end

end
