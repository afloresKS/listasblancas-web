class BanksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_bank, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js
  @@bank = "Banks"

  def index
    begin

      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Banks' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearBank = permisos.crear
            @editarBank = permisos.editar
            @leerBank = permisos.leer
            @eliminarBank = permisos.eliminar
            if @uno == @@bank
              @@crear = permisos.crear
              @@editar = permisos.editar
              @@leer = permisos.leer
              @@eliminar = permisos.eliminar

              @crear = permisos.crear
              @editar = permisos.editar
              @leer = permisos.leer
              @eliminar = permisos.eliminar
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4) || (@@eliminar == 1))
            if params[:search].present?
              registro(3, params[:search]);
              busqueda = params[:search].to_s.strip
              if Bank.where("name like '%#{busqueda}%'").present?
                @banks = Bank.where("name like '%#{busqueda}%'").order("created_at DESC").page(params[:page]).per(20);
                @query = busqueda
              elsif Bank.where("bank_id = '#{busqueda}'").present?
                @banks = Bank.where("bank_id = '#{busqueda}'").order("created_at DESC").page(params[:page]).per(20);
                @query = busqueda
              else
                @banks = Bank.order("created_at DESC").page(params[:page]).per(20);
                @error = true
              end
            else
              @banks = Bank.order("created_at DESC").page(params[:page]).per(20);
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end

    rescue => e
      logger = Logger.new('log/errorbanks.log')
      logger.error("Error Banks en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "Banks método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def show
    begin

      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Banks' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerBank = permisos.leer
            if @uno == @@bank
              @@leer = permisos.leer

              @leer = permisos.leer
            end
          end
          if (@@leer == 2)
            registro(5, @bank);
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorbanks.log')
      logger.error("Error Banks en el metodo show #{Time.now.to_s}")
      logger.error(e)
      @metod = "Banks método show"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def new
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Banks' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearBank = permisos.crear
            if @uno == @@bank
              @@crear = permisos.crear

              @crear = permisos.crear
            end
          end
          if (@@crear == 8)
            @bank = Bank.new
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorbanks.log')
      logger.error("Error Banks en el metodo new #{Time.now.to_s}")
      logger.error(e)
      @metod = "Banks método new"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def edit
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Banks' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @editarBank = permisos.editar
            if @uno == @@bank
              @@editar = permisos.editar

              @editar = permisos.editar
            end
          end
          if (@@editar == 4)

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorbanks.log')
      logger.error("Error Banks en el metodo edit #{Time.now.to_s}")
      logger.error(e)
      @metod = "Banks método edit"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def create
    begin
      registro(1, @bank);
      @bank = Bank.new(bank_params)

      respond_to do |format|
        if Bank.where("name = ? or bank_id = ?", @bank.name, @bank.bank_id).exists?
          format.html {
            @error2 = true
            render action: :new
          }
        else
          if @bank.save
            format.html {redirect_to :controller => 'banks', :action => 'index'}
            flash[:notice] = t('views.message_banks.new_bank')
            format.json {render :show, status: :created, location: @bank}
          else
            format.html {render :new}
            format.json {render json: @bank.errors, status: :unprocessable_entity}
          end
        end
      end
    rescue => e
      logger = Logger.new('log/errorbanks.log')
      logger.error("Error Banks en el metodo create #{Time.now.to_s}")
      logger.error(e)
      @metod = "Banks método create"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def update
    begin
      registro(2, @bank)
      respond_to do |format|
        @consulta = Bank.find_by_name(params[:bank][:name])
        @consulta2 = Bank.find_by_bank_id(params[:bank][:bank_id])
        if @consulta.nil? || @consulta.id == @bank.id
          if @consulta2.nil? || @consulta2.id == @bank.id
            if @bank.update(bank_params)
              format.html {redirect_to :controller => 'banks', :action => 'index'}
              flash[:notice] = t('views.message_banks.edit_bank')
              format.json {render :show, status: :ok, location: @bank}
            else
              format.html {render :edit}
              format.html {render json: @bank.errors, status: :unprocessable_entity}
            end
          else
            format.html {
              @error2 = true
              render action: :new
            }
          end
        else
          format.html {
            @error2 = true
            render action: :new
          }
        end
      end
    rescue => e
      logger = Logger.new('log/errorbanks.log')
      logger.error("Error Banks en el metodo update #{Time.now.to_s}")
      logger.error(e)
      @metod = "Banks método update"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def destroy
    if current_user
      @cu = current_user.profile_id
      if @cu != 0
        @per = Permission.where("view_name = 'Banks' and profile_id = ?", @cu)
        @per.each do |permisos|
          @uno = permisos.view_name
          @eliminarBank = permisos.eliminar
          if @uno == @@bank
            @@eliminar = permisos.eliminar
            @eliminar = permisos.eliminar
          end
        end
        if (@@eliminar == 1)
          registro(4, @bank)
          @bank.destroy
          respond_to do |format|
            format.html {redirect_to banks_url, notice: t('views.message_banks.mess_banks_swall')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private

  def set_bank
    @bank = Bank.find(params[:id])
  end

  def bank_params
    params.require(:bank).permit(:name, :bank_id)
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "Banks"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end


end
