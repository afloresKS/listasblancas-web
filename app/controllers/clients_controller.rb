class ClientsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_client, only: [:show, :show_client, :edit, :update, :destroy]
  respond_to :html, :js

  @@client = "Clients"

  # GET /clients
  # GET /clients.json
  def index
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Clients' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearClient = permisos.crear
            @editarClient = permisos.editar
            @leerClient = permisos.leer
            @eliminarClient = permisos.eliminar
            if @uno == @@client
              @@crear = permisos.crear
              @@editar = permisos.editar
              @@leer = permisos.leer
              @@eliminar = permisos.eliminar

              @crear = permisos.crear
              @editar = permisos.editar
              @leer = permisos.leer
              @eliminar = permisos.eliminar
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4) || (@@eliminar == 1))
            @t_c = Card.where(:client_id => @clients).count
            if params[:search].present?
              registro(3, params[:search]);
              busqueda = params[:search].to_s.strip
              if busqueda == "N/A"
                consulta = "name IS NULL or last_name IS NULL or email IS NULL or account IS NULL or street IS NULL or city IS NULL or town IS NULL or state IS NULL"
              else
                consulta = "name like '%#{busqueda}%' or last_name like '%#{busqueda}%' or email like '%#{busqueda}%'
                                     or account = '#{busqueda}' or street like '%#{busqueda}%'
                                     or city like '%#{busqueda}%' or town like '%#{busqueda}%' or state like '%#{busqueda}%'"
              end
              if Client.where(consulta).present?
                @clients = Client.where(consulta).order("created_at DESC").page(params[:page]).per(20)
                @query = params[:search]
              else
                @clients = Client.where(consulta).order("created_at DESC").page(params[:page]).per(20)
                @error = true
              end
            else
              @clients = Client.order("created_at DESC").page(params[:page]).per(20)
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorclients.log')
      logger.error("Error clients en el metodo index #{Time.now.to_s}")
      logger.error(e)
      @metod = "clients método index"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Clients' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @leerClient = permisos.leer
            @crearClient = permisos.crear
            @editarClient = permisos.editar


            if @uno == @@client
              @@leer = permisos.leer
              @@crear = permisos.crear
              @@editar = permisos.editar

              @editar = permisos.editar
              @crear = permisos.crear
              @leer = permisos.leer
            end
          end
          if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4))
            registro(5, @client);
            cliente_id = params[:id]
            @cliente_id = cliente_id
            @t_c = Card.where(:client_id => @cliente_id).count
            @total_cards = Card.where(:client_id => @cliente_id)
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorclients.log')
      logger.error("Error clients en el metodo show #{Time.now.to_s}")
      logger.error(e)
      @metod = "clients método show"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  def dynamic

    if params[:codigo].present?
      codigo = params[:codigo].to_s
      @nuevo = Codigos.new(codigo)
    end
    if params[:id].present?
      @client = Client.find_by('id = ?', params[:id])
    end
    render :partial => 'dynamic'

  end

  def show_client
    @cliente_id = params[:id]
    @client_new = Client.where('id = ' + params[:id])
  end

  # GET /clients/new
  def new
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Clients' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @crearClient = permisos.crear
            if @uno == @@client
              @@crear = permisos.crear
              @crear = permisos.crear
            end
          end
          if (@@crear == 8)
            @client = Client.new
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorclients.log')
      logger.error("Error clients en el metodo new #{Time.now.to_s}")
      logger.error(e)
      @metod = "clients método new"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # GET /clients/1/edit
  def edit
    begin
      if current_user
        @cu = current_user.profile_id
        if @cu != 0
          @per = Permission.where("view_name = 'Clients' and profile_id = ?", @cu)
          @per.each do |permisos|
            @uno = permisos.view_name
            @editarClient = permisos.editar
            if @uno == @@client
              @@editar = permisos.editar
              @editar = permisos.editar
            end
          end
          if (@@editar == 4)

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    rescue => e
      logger = Logger.new('log/errorclients.log')
      logger.error("Error clients en el metodo edit #{Time.now.to_s}")
      logger.error(e)
      @metod = "clients método edit"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # POST /clients
  # POST /clients.json
  def create
    begin
      registro(1, @client);
      @client = Client.new(client_params)
      respond_to do |format|
        if Client.where("name = '#{@client.name}' and last_name = '#{@client.last_name}' and account = '#{@client.account}'").exists?
          format.html {
            @error = t('views.message_client.notice_blackup')
            render action: :new
          }
        else
          if params[:add_client]
            if @client.save
              format.html {redirect_to :controller => 'clients', :action => 'index', :id => @client.id}
              flash[:notice] = t('views.message_client.new_client')
              format.json {render :show, status: :created, location: @client}
            else
              format.html {render :new}
              format.json {render json: @client.errors, status: :unprocessable_entity}
            end
          elsif params[:client_card]
            if @client.save
              format.html {redirect_to :controller => 'cards', :action => 'new', :id => @client.id}
              flash[:notice] = t('views.message_client.new_client')
              format.json {render :show, status: :created, location: @client}
            else
              format.html {render :new}
              format.json {render json: @client.errors, status: :unprocessable_entity}
            end
          end
        end
      end
    rescue => e
      logger = Logger.new('log/errorclients.log')
      logger.error("Error clients en el metodo create #{Time.now.to_s}")
      logger.error(e)
      @metod = "clients método create"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    begin
      registro(2, @client)
      respond_to do |format|
        @consulta = Client.find_by_name(params[:client][:name])
        @consulta2 = Client.find_by_last_name(params[:client][:last_name])
        @consulta3 = Client.find_by_account(params[:client][:account])
        if @consulta.nil? || @consulta.id == @client.id || @consulta2.nil? || @consulta2.id == @client.id || @consulta3.nil? || @consulta3.id == @client.id
          if @client.update(client_params)
            format.html {redirect_to @client}
            flash[:notice] = t('views.message_client.edit')
            format.json {render :show, status: :ok, location: @client}
          else
            format.html {render :edit}
            format.json {render json: @client.errors, status: :unprocessable_entity}
          end
        else
          format.html {
            @error = t('views.message_client.notice_blackup')
            render action: :new
          }
        end
      end
    rescue => e
      logger = Logger.new('log/errorclients.log')
      logger.error("Error clients en el metodo update #{Time.now.to_s}")
      logger.error(e)
      @metod = "clients método update"
      file = "#{Rails.root}/app/views/errors/file.txt"
      File.open(file, 'w') {|file| file.write("#{e.backtrace}")}
      @error = "#{e.message }"
      file_send
      if e.message == "ActionController::UnknownFormat"
        render_404
      else
        render_500
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    if current_user
      @cu = current_user.profile_id
      if @cu != 0
        @per = Permission.where("view_name = 'Clients' and profile_id = ?", @cu)
        @per.each do |permisos|
          @uno = permisos.view_name
          @eliminarClient = permisos.eliminar
          if @uno == @@client
            @@eliminar = permisos.eliminar
            @eliminar = permisos.eliminar
          end
        end
        if (@@eliminar == 1)
          @cards = Card.where("client_id = ?", @client.id);
          @cards.each do |card|
            registro(4, @client)
            card.destroy
          end
          @client.destroy
          respond_to do |format|
            format.html {redirect_to clients_url, notice: t('views.message_client.mess_cli_swall')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_client
    @client = Client.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def client_params
    params.require(:client).permit(:last_name, :name, :email, :sex, :street, :city, :town, :state, :cp, :birthday, :last_at, :account, :sal_ant)
  end

  def file_send
    @file = "#{Rails.root}/app/views/errors/file.txt"
    @fecha = Time.now.to_datetime.strftime("%I:%M:%S%p")
    usuariolog = current_user.id
    @user = User.find_by_id(usuariolog)
    @app = t('all.profile_template')
  end

  def registro(accion, detalle)
    @historic = Historic.new
    @historic.page = "Clients"
    @historic.user_name = current_user.name + " " + current_user.last_name
    @historic.user_id = current_user.id
    @historic.action = accion
    @historic.date = Time.now()
    @historic.detail = detalle.to_json
    @historic.save
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end

  def render_404
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/404", :status => :not_found}
    end
  end

  def render_500
    SendFile.send_error_500_es(@fecha, @user, @error, @app, @file, @metod).deliver
    respond_to do |format|
      format.html {render :file => "#{Rails.root}/app/views/errors/500", :status => :not_found}
    end
  end



end
