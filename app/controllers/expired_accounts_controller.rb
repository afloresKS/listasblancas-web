class ExpiredAccountsController < ApplicationController
  before_filter :authenticate_user!, except: [:new]

  def new
    usuario = User.where('user_name = :dato OR email = :dato', dato: params[:email])
    if usuario.nil? || usuario.present?
      usuario.each do |u|
        u.update_attribute(:last_activity_at, Time.zone.now - 2.months) if u.last_activity_at.nil?
        last = u.last_activity_at
        if last < 1.month.ago && u.profile.flag != 0
          admin = User.joins('profiles on profiles.id = users.profile_id').where('flag = 0') if u.profile.flag == 1
          admin = User.joins('profiles on profiles.id = users.profile_id').where('flag = 1 and users.area_id = ?', u.area_id) if u.profile.flag >= 2
          admin.each do |a|
            u.activo = 1
            u.save
            Expired.avisar_activacion(a.email, user).deliver
          end
        elsif last < 1.month.ago && u.profile.flag == 0
          admin = User.joins('profiles on profiles.id = users.profile_id').where('flag = 0')
          # if admin.present?
            admin.each do |a|
              u.activo = 1
              u.save
              Expired.avisar_activacion(a.email, user).deliver
            end
          # end
        end
      end
      redirect_to new_user_session_path, notice: t('all.we_activation')
    end
  end
end
