class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy, :activa, :activiser]
  before_action :auth, :permisos_vistas, :horarios
  require 'time'

  begin
    def index
      if @permisos[0] == 8 || @permisos[1] == 4 || @permisos[2] == 2 || @permisos[3] == 1 # si tiene alguno de los permisos se desplegara la tabla
        # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Mostrar Usuarios >>>>>>>>>>>>>>>>>>>>>>>>>
        if current_user.area_id == '1' # si es de area 1 se desplegaran todos los usuarios de todas las areas
          @users = User.all.order(:name).page(params[:page]).per(10)
        else # si es de otra area se desplegaran todos los usuarios de las subareas y de su area actual
          consult_subareas
          @users = User.where("area_id #{@consulta_areas}").order(:name).page(params[:page]).per(10) # consulta general de todas las subareas del area actual
        end
        # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Mostrar Usuarios <<<<<<<<<<<<<<<<<<<<<<<<<
        # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Buscar Usuarios >>>>>>>>>>>>>>>>>>>>>>>>>>
        if params[:search].present?
          @busqueda = params[:search] #.gsub(/[^a-zA-Z0-9 ]/, ''); # evita sql injection
          registro(3, @busqueda)
          if current_user.area_id == '1'
            consulta = '(name LIKE :datos OR last_name LIKE :datos OR user_name LIKE :datos OR email LIKE :datos)'
          else
            consulta = "(name LIKE :datos OR last_name LIKE :datos OR user_name LIKE :datos OR email LIKE :datos) AND area_id #{@consulta_areas}"
          end
          @users = User.where(consulta, datos: "%#{@busqueda}%").order('created_at DESC').page(params[:page]).per(10)
          @error = 1 if @users.empty?
        end
        # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Buscar Usuarios <<<<<<<<<<<<<<<<<<<<<<<
      else
        redirect_to home_index_path, alert: t('all.not_access')
      end # <<<<<<<<<<<<<<<<<<<<<<<<< if permisos[0]... <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    end

    # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Index <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    def show
      if @permisos[2] == 2 # si tiene permiso de leer
        registro(5, @user)
        consult_subareas
        if current_user.area_id != '1'
          unless @consulta_areas.include?(@user.area_id.to_s) # seguridad extra
            redirect_to home_index_path, alert: t('all.not_access')
          end
        end
        @profile = Profile.where("area_id = #{@user.area_id}")
      else
        redirect_to home_index_path, alert: t('all.not_access')
      end
    end

    def edit
      if @permisos[1] == 4 # si tiene permiso de editar
        consult_subareas
        if current_user.area_id == '1'
          @areas = Area.all
          if @user.profile.flag <= current_user.profile.flag || @user.id == current_user.id # seguridad extra
            redirect_to home_index_path, alert: t('all.not_access')
          end
        else
          @areas = Area.where("id #{@consulta_areas}")
          if @user.profile.flag < current_user.profile.flag || @user.id == current_user.id || !@consulta_areas.include?(@user.area_id.to_s) # seguridad extra
            redirect_to home_index_path, alert: t('all.not_access')
          end
        end
        @profile = Profile.where("area_id = #{@user.area_id}")
      else
        redirect_to home_index_path, alert: t('all.not_access')
      end
    end

    def update

      if @user.update_attributes(secure_params)
        registro(2, @user)
        @user.activo = 0
        @user.last_activity_at = Time.now
        @user.expired_at = nil
        @user.save
        ChangePerfil.permisos_otorgados(@user).deliver
        redirect_to users_path, notice: t('views.mess_cont_us_up')
      end
    end

    def destroy
      if @permisos[3] == 1

        consult_subareas
        if current_user.area_id == '1'
          if @user.profile.flag < current_user.profile.flag || @user.id == current_user.id
            redirect_to home_index_path, alert: t('all.not_access')
          else
            registro(4, @user)
            if @user.habilitado == 1
              @user.update_attribute(:habilitado, 0)
            elsif @user.habilitado == 0
              @user.update_attribute(:habilitado, 1)
            end
          end
        else
          if @user.profile.flag < current_user.profile.flag || !@consulta_areas.include?(@user.area_id.to_s) || @user.id == current_user.id
            redirect_to home_index_path, alert: t('all.not_access')
          else
            registro(4, @user)
            if @user.habilitado == 1
              @user.update_attribute(:habilitado, 0)
            elsif @user.habilitado == 0
              @user.update_attribute(:habilitado, 1)
            end
          end
        end
      else
        redirect_to home_index_path, alert: t('all.not_access')
      end
    end

    def consult_profiles
      perfiles = Profile.where("area_id = #{params[:area_id]} and flag >= #{current_user.profile.flag}")
      render json: perfiles
    end

    def activa
      @user.update_attributes(activo: 1, last_activity_at: Time.now)
    end

    def activiser
      @user.update_attribute(:profile_id, params[:profile_id])
      activa
    end

    private

    def permisos_vistas
      # se verifican los permisos de la vista y se almacenan
      consulta = Permission.where('view_id = 11 and profile_id= ?', current_user.profile_id)
      consulta.each do |permisos|
        @permisos = Array[permisos.crear, permisos.editar, permisos.leer, permisos.eliminar, permisos.horainicial, permisos.horafinal]
      end
      consulta = Permission.where('view_id = 13 and profile_id = ?', current_user.profile_id)
      consulta.each do |permisos|
        @permisos_perfiles = Array[permisos.crear, permisos.editar, permisos.leer, permisos.eliminar]
      end
    end

    def consult_subareas
      areas = Area.where('area_id = ?', current_user.area_id)
      arreglo_general = []
      arreglo_general.push(current_user.area_id)
      arreglo_subareas = []
      control = 0
      # se buscaran todas las subareas de la area actual del usuario logeado
      while control != 1
        arreglo_subareas.clear
        areas.each do |a|
          arreglo_subareas.push(a.id) # se almacenaran todas las subareas para la consulta especifica
          arreglo_general.push(a.id) # se almacenaran todas las areas para la consulta general
        end # <<<<<<<<<<<<<<<<<<<<<<<<<< Each do <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        @consulta_areas = "in(#{arreglo_subareas.join(',')})" # creara la consulta para buscar en otras subareas subsecuentes
        if arreglo_subareas.empty? # cuando ya no existan mas subareas acabara el ciclo
          control = 1
        else
          areas = Area.where("area_id #{@consulta_areas}")
        end
      end # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< while <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      @consulta_areas = "in(#{arreglo_general.join(',')})" # se hara la consulta general de todos los usuarios de las subareas
    end

    def registro(accion, detalle)
      @historic = Historic.new
      @historic.page = "Users"
      @historic.user_name = current_user.name + " " + current_user.last_name
      @historic.user_id = current_user.id
      @historic.action = accion
      @historic.date = Time.now()
      @historic.detail = detalle.to_json
      @historic.save
    end

    def user_params
      params.require(:user).permit(:email, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :sign_in_count, :current_sing_in_at, :last_sign_in_at, :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email, :failed_attempts, :unlock_token, :locked_at, :created_at, :updated_at, :username, :name, :password, :password_confirmation, :last_name, :birthday, :boss_name, :phone, :ext, :profile_id, :employment, :role, :area_id, :activo, :last_activity_at, :expired_at)
    end

    def set_user
      @user = User.find(params[:id])
    end

    def secure_params
      params.require(:user).permit(:profile_id, :area_id, :name, :last_name, :phone, :boss_name, :employment)
    end

    def horarios
      unless Time.now >= Time.parse(@permisos[4].strftime("%H:%M")) && Time.now <= Time.parse(@permisos[5].strftime("%H:%M")) || @permisos[5].strftime("%H:%M") == "00:00" || @permisos[4].strftime("%H:%M") == "00:00"
        redirect_to message_index_path, :alert => t('all.not_access');
      end
    end

    def auth
      if !current_user || current_user.habilitado == 0 || current_user.activo == 0 then
        redirect_to destroy_user_session_path, :alert => t('all.please_continue')
      end
    end

  rescue => e
    logger = Logger.new("log/error_areas.log")
    logger.error(e)
  end

end
