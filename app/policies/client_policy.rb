class ClientPolicy < ApplicationPolicy

  def index?
    true
  end

  def create?
    user.admin?
  end

  def destroy?
    user.admin?
  end

  def edit?
    user.admin?
  end


end