#class ApplicationPolicy
class UserPolicy < ApplicationPolicy

  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    puts @current_user.name
    @user = model

  end

  def user_index?
    @current_user.admin?
  end

  def show?
    @current_user.admin? or @current_user == @user
  end

  def update?
    @current_user.admin?
  end

  def destroy?
    return false if @current_user == @user
    @current_user.admin?
  end
  #agrego el metodo enviarcorreo que se encuentra tambien en la clase User
  #porque pundit intenta validar que el metodo se encuentre en esta clase sino manda error
  def enviarcorreo?
    return true
  end
end