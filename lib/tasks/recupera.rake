namespace :recupera do

  desc "Proceso de liberacion de saldo"
  task saldo: :environment do
    datos = Saldos.new
    datos.recuperar
  end

end
