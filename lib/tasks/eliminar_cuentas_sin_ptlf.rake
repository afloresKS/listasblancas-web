namespace :eliminar do

  task cuentas_sin_ptlf: :environment do
    begin

      tarjetas = Card.all
      contador = 0

      tarjetas.each do |card|
        card.ptlfs.count
        @count = Ptlf.where("card_id = ?", card.id).count

        if @count.to_i < 1
          contador += 1
          card.destroy
          if card.client.present?
            cliente = Client.find(card.client_id)
            cliente.destroy
          end
        end
      end

      puts "Total #{contador}"
      
    rescue => e
      puts 'Error encontrado:'+Time.now.to_s
      puts 'ArchivoError: log/errores_pruebas_envio_email.log'
      logger = Logger.new('log/errores_pruebas_envio_email.log')
      logger.error('-------------------|||||||||||||||||-------------------|||||||||||||||||||-----------------|||||||||||||||||||||||')
      logger.error(e)
    end
  end
end