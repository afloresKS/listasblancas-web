require 'uri'
namespace :read_file do
  task re_fi: :environment do
    files = Dir.glob("#{Rails.root}/carga masiva 10/*.xlsx").sort
    @usuario = User.find_by_email("lmena@kssoluciones.com")
    archivos = 0
    puts "Inicio: "+Time.now.to_s
    #files.each do |fil|
    files.sort_by do |fil|
      archivos += 1
      puts "Archivo #{archivos.to_s}: #{fil}"
      begin
          Read.process(fil.to_s,@usuario.id,0         , archivos)
      rescue => e
        puts 'Error contrado: '+Time.now.to_s
        puts 'Archivo: log/errores.log'
        logger = Logger.new('log/errores.log')
        logger.error('----------------------------------------------------------------------')
        logger.error(e)
      end
    end
    puts "Termino: "+Time.now.to_s
  end
end