namespace :tarea do

  desc "Proceso de liberacion de saldo"
  task saldo: :environment do
    Tareas.recuperar
  end

  desc "Porceso de actualizacion de limite de transacción"
  task limit: :environment do
    Tareas.devuelto
  end


end