namespace :pruebas do

  task envio_email: :environment do
    begin
      valor = 'Hola'
      EnvioPruebas.alerta_envios(valor).deliver
      puts 'TerminoEnviandoCorreos'
    rescue => e
      puts 'Error encontrado:'+Time.now.to_s
      puts 'ArchivoError: log/errores_pruebas_envio_email.log'
      logger = Logger.new('log/errores_pruebas_envio_email.log')
      logger.error('-------------------|||||||||||||||||-------------------|||||||||||||||||||-----------------|||||||||||||||||||||||')
      logger.error(e)
    end
  end
end