class AddFilesRegisterTranServicePtlf < ActiveRecord::Migration[5.0]
  def change
    add_column :ptlfs, :registration_id, :integer
    add_column :ptlfs, :transaction_number, :integer
    add_column :ptlfs, :service_number, :integer
  end
end
