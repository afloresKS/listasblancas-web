class ChangeApprovalFormatInPtlfs < ActiveRecord::Migration[5.0]
  def change
  	change_column :ptlfs, :approval, :string
  end
end