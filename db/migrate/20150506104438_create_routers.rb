class CreateRouters < ActiveRecord::Migration[5.0]
  def change
    create_table :routers do |t|
      t.string :name
      t.string :ip
      t.integer :port
      t.timestamps null: false
    end
  end
end
