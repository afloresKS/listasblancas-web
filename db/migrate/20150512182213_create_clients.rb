class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string   :last_name
      t.string   :name
      t.string   :email
      t.integer  :sex
      t.string   :street
      t.string   :city
      t.string   :town
      t.string   :state
      t.integer  :cp
      t.date     :birthday
      t.datetime :last_at
      t.string   :account
      t.timestamps null: false
    end
  end
end
