class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.integer :id_profile
      t.integer :id_page
      t.boolean :read
      t.boolean :write
      t.boolean :edit
      t.boolean :delete

      t.timestamps null: false
    end
  end
end
