class AddFileDevolutionAndReturnedBalance < ActiveRecord::Migration[5.0]
  def change
    add_column :ptlfs, :devolution, :boolean
    add_column :ptlfs, :returned_balance, :boolean
  end
end
