class CreateBins < ActiveRecord::Migration[5.0]
  def change
    create_table :bins do |t|
      t.belongs_to :cardType , index: true
      t.integer :name
      t.string  :fiid
      t.integer :cardType_id
      t.integer :count
      t.integer :amount
      t.timestamps null: false
    end
  end
end
