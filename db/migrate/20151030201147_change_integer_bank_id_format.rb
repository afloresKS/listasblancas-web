class ChangeIntegerBankIdFormat < ActiveRecord::Migration[5.0]
  def up
    change_column :banks, :bank_id, :string
  end

  def down
    change_column :banks, :bank_id, :integer
  end
end
