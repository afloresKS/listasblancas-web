class AddHorafinalAddHorainicialToPermissions < ActiveRecord::Migration[5.0]
  def change
    add_column :permissions, :horafinal, :time
    add_column :permissions, :horainicial, :time
  end
end
