class CreateCards < ActiveRecord::Migration[5.0]
  def change
    create_table :cards do |t|

    	t.belongs_to :client , index:true
      t.belongs_to :cardType , index:true
      t.belongs_to :currency , index:true

      t.integer :client_id
      t.string :number_card
      t.integer :cardType_id
      t.integer :initial_balance
      t.integer :actual_balance
      t.integer :points
      t.integer :currency_id
      t.string  :expiration
      t.date    :last_at
      t.string  :cvv

      t.timestamps null: false
    end
  end
end
