class CreatePtlfs < ActiveRecord::Migration[5.0]
  def change
    create_table :ptlfs do |t|

      t.belongs_to :card , index:true
      t.belongs_to :transacctionType , index:true
      t.belongs_to :plan , index:true
      t.belongs_to :promotion , index:true
      t.belongs_to :affiliation , index:true

      t.integer :card_id
      t.integer :transacctionType_id
      t.string :name
      t.integer :amount
      t.integer :amount2
      t.integer :approval
      t.integer :response_code
      t.integer :affiliation_id
      t.string :address
      t.integer :deferral
      t.integer :partial
      t.integer :plan_id
      t.integer :promotion_id

      t.timestamps null: false
    end
  end
end
