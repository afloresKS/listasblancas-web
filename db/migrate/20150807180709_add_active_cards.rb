class AddActiveCards < ActiveRecord::Migration[5.0]
  def change
    add_column :cards, :active, :boolean
  end
end
