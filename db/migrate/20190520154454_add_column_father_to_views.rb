class AddColumnFatherToViews < ActiveRecord::Migration[5.0]
  def change
    add_column :views, :father, :string
  end
end
