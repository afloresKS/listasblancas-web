class AddFilesAffiliation < ActiveRecord::Migration[5.0]
  def change
    add_column :affiliations, :affiliation_id, :integer
    add_column :affiliations, :district_id, :string
    add_index :affiliations, :district_id
    add_column :affiliations, :region_id, :string
    add_index  :affiliations, :region_id
  end
end
