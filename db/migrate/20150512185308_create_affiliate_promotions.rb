class CreateAffiliatePromotions < ActiveRecord::Migration[5.0]
  def change
    create_table :affiliate_promotions do |t|

      t.belongs_to :affiliation , index: true
      t.belongs_to :promotion , index: true

      t.integer :affiliation_id
      t.integer :promotion_id

      t.timestamps null: false
    end
  end
end
