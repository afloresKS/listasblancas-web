class CreatePromotions < ActiveRecord::Migration[5.0]
  def change
    create_table :promotions do |t|
      t.belongs_to :transactionType , index: true
      t.belongs_to :bin , index: true
      t.string :name
      t.integer :bin_id
      t.integer :transactionType_id
      t.integer :percent
      t.integer :amount_base
      t.integer :amount_desc
      t.date :start_at
      t.date :end_at
      t.timestamps null: false
    end
  end
end
