class CreateShops < ActiveRecord::Migration[5.0]
  def change
    create_table :shops do |t|
      t.string :name
      t.string :port
      t.string :contract
      t.timestamps null: false
    end
  end
end
