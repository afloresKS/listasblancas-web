class ChangeResponseFormatInPtlfs < ActiveRecord::Migration[5.0]
  def change
  	change_column :ptlfs, :response_code, :string
  end
end
