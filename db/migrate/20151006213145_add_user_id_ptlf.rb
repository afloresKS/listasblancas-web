class AddUserIdPtlf < ActiveRecord::Migration[5.0]
  def change
    add_column :ptlfs, :user_id, :integer
    add_index :ptlfs, :user_id
  end
end
