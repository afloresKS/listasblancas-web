class CreateHistorics < ActiveRecord::Migration[5.0]
  def change
    create_table :historics do |t|
      t.string :page
      t.string :action
      t.string :user_name
      t.integer :user_id
      t.string :detail
      t.datetime :date

      t.timestamps
    end
  end
end
