class AddNumberAndPortToAffilation < ActiveRecord::Migration[5.0]
  def change
    add_column :affiliations, :number , :integer
    add_column :affiliations, :port , :integer
  end
end
