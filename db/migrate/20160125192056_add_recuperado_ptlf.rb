class AddRecuperadoPtlf < ActiveRecord::Migration[5.0]
  def change
    add_column :ptlfs, :recovered, :boolean
  end
end
