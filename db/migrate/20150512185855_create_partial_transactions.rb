class CreatePartialTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :partial_transactions do |t|

      t.belongs_to :ptlf , index:true
      t.belongs_to :card , index:true

      t.integer :ptlf_id
      t.integer :card_id
      t.integer :amount
      t.integer :number
      t.integer :partial
      t.date :charge_at

      t.timestamps null: false
    end
  end
end
