class AddDateTimeEndThreadUploadFiles < ActiveRecord::Migration[5.0]
  def change
    add_column :upload_files, :end_upload_file, :datetime
  end
end
