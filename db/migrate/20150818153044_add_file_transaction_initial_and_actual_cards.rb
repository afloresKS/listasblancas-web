class AddFileTransactionInitialAndActualCards < ActiveRecord::Migration[5.0]
  def change
    add_column :cards, :transaction_initial, :integer
    add_column :cards, :transaction_actual, :integer
  end
end
