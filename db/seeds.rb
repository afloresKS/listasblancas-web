# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# #
# # Examples:
# #
# #   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# #   Character.create(name: 'Luke', movie: movies.first)
#
##################### VISTAS BASE ######################
vista = View.new
vista.name = "Home"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.father = 15
vista.save

vista = View.new
vista.name = "Clients"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 16
vista.save

vista = View.new
vista.name = "Cards"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 12
vista.save

vista = View.new
vista.name = "Transactions"
vista.crear = 0
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.father = 16
vista.save

vista = View.new
vista.name = "Reports"
vista.crear = 0
vista.editar = 1
vista.eliminar = 0
vista.leer = 1
vista.father = 16
vista.save

vista = View.new
vista.name = "Banks"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 16
vista.save

vista = View.new
vista.name = "Shops"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 17
vista.save

vista = View.new
vista.name = "Districts"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 17
vista.save

vista = View.new
vista.name = "Regions"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 17
vista.save

vista = View.new
vista.name = "Archives"
vista.crear = 1
vista.editar = 0
vista.eliminar = 1
vista.leer = 1
vista.father = 18
vista.save

vista = View.new
vista.name = "Users"
vista.crear = 0
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 19
vista.save

vista = View.new
vista.name = "Profiles"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 19
vista.save

vista = View.new
vista.name = "Areas"
vista.crear = 1
vista.editar = 1
vista.eliminar = 1
vista.leer = 1
vista.father = 19
vista.save

vista = View.new
vista.name = "Binnacle"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 1
vista.father = 19
vista.save
#######################Vistas PADRE #####################
vista = View.new
vista.name = "Home"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 0
vista.father = 0
vista.save

vista = View.new
vista.name = "White Lists"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 0
vista.father = 0
vista.save

vista = View.new
vista.name = "Stores"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 0
vista.father = 0
vista.save

vista = View.new
vista.name = "Files"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 0
vista.father = 0
vista.save

vista = View.new
vista.name = "Settings"
vista.crear = 0
vista.editar = 0
vista.eliminar = 0
vista.leer = 0
vista.father = 0
vista.save

##################### AREA CENTRAL ######################
area = Area.new
area.name = "Central"
area.save

##################### PERFIL ADMINISTRADOR CENTRAL ######################
perfil = Profile.new
perfil.name = 'Administrator Central'
perfil.area_id = 1
perfil.flag = 0
perfil.save

permiso = Permission.new
permiso.view_id = 1
permiso.view_name = View.find(1).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 2
permiso.view_name = View.find(2).name
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 3
permiso.view_name = View.find(3).name
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 4
permiso.view_name = View.find(4).name
permiso.crear = nil
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 5
permiso.view_name = View.find(5).name
permiso.crear = nil
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 6
permiso.view_name = View.find(6).name
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 7
permiso.view_name = View.find(7).name
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 8
permiso.view_name = View.find(8).name
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 9
permiso.view_name = View.find(9).name
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 10
permiso.view_name = View.find(10).name
permiso.crear = 8
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 11
permiso.view_name = View.find(11).name
permiso.crear = nil
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 12
permiso.view_name = View.find(12).name
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 13
permiso.view_name = View.find(13).name
permiso.crear = 8
permiso.editar = 4
permiso.leer = 2
permiso.eliminar = 1
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 14
permiso.view_name = View.find(14).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = 2
permiso.eliminar = nil
permiso.profile_id = 1
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

##################### PERFIL Default CENTRAL ######################
perfil = Profile.new
perfil.name = 'Default Central'
perfil.area_id = 1
perfil.flag = 2
perfil.save

permiso = Permission.new
permiso.view_id = 1
permiso.view_name = View.find(1).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 2
permiso.view_name = View.find(2).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 3
permiso.view_name = View.find(3).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 4
permiso.view_name = View.find(4).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 5
permiso.view_name = View.find(5).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 6
permiso.view_name = View.find(6).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 7
permiso.view_name = View.find(7).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 8
permiso.view_name = View.find(8).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 9
permiso.view_name = View.find(9).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 10
permiso.view_name = View.find(10).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 11
permiso.view_name = View.find(11).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 12
permiso.view_name = View.find(12).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 13
permiso.view_name = View.find(13).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save

permiso = Permission.new
permiso.view_id = 14
permiso.view_name = View.find(14).name
permiso.crear = nil
permiso.editar = nil
permiso.leer = nil
permiso.eliminar = nil
permiso.profile_id = 2
permiso.horafinal = '00:00'
permiso.horainicial = '00:00'
permiso.save


####Crear Tipo de tarjeta
#
tipo = CardType.new
tipo.name = "Check"
tipo.save

tipo = CardType.new
tipo.name = "Post Date"
tipo.save

tc = CardType.new
tc.name = "Revolving Check"
tc.save

####Crear Tipo Transacción
#
tran = TransacctionType.new
tran.name = "Venta"
tran.save

tran = TransacctionType.new
tran.name = "Cancelacion"
tran.save

####Crear los tipos de moneda

moneda = Currency.new
moneda.name = "MXN"
moneda.save



#####1000.times do
#####  Ptlf.create([{
#####                   card_id: Faker::Number.number(4),
#####                   transacctionType_id: '1',
#####                   affiliation_id: Faker::Number.number(6),
#####                   check_number: 'CHECK20',
#####                   amount: Faker::Number.number(7),
#####                   approval: 'HD0000',
#####                   response_code: '53',
#####                   devolution: '0',
#####                   returned_balance: '0',
#####                   post_date: Faker::Time.forward(50, :morning)
#####               }])
#####end