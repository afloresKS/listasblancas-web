# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190917233122) do

  create_table "affiliate_promotions", force: :cascade do |t|
    t.integer  "affiliation_id"
    t.integer  "promotion_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "affiliations", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.string   "fiid"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "affiliation_id"
    t.string   "district_id"
    t.string   "region_id"
    t.integer  "number"
    t.integer  "port"
  end

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "area_id"
  end

  create_table "banks", force: :cascade do |t|
    t.string   "name"
    t.string   "bank_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bins", force: :cascade do |t|
    t.integer  "cardType_id"
    t.integer  "name"
    t.string   "fiid"
    t.integer  "count"
    t.integer  "amount"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "card_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "cardType_id"
    t.integer  "currency_id"
    t.string   "number_card"
    t.integer  "initial_balance"
    t.integer  "actual_balance"
    t.integer  "points"
    t.string   "expiration"
    t.date     "last_at"
    t.string   "cvv"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "floor_limit"
    t.boolean  "active"
    t.integer  "transaction_initial"
    t.integer  "transaction_actual"
  end

  create_table "clients", force: :cascade do |t|
    t.string   "last_name"
    t.string   "name"
    t.string   "email"
    t.integer  "sex"
    t.string   "street"
    t.string   "city"
    t.string   "town"
    t.string   "state"
    t.integer  "cp"
    t.date     "birthday"
    t.datetime "last_at"
    t.string   "account"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "districts", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "logs", force: :cascade do |t|
    t.string   "name"
    t.string   "path"
    t.string   "nomenclature"
    t.integer  "filte_type"
    t.string   "host"
    t.integer  "port"
    t.string   "db"
    t.string   "user"
    t.string   "password"
    t.integer  "db_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "old_passwords", force: :cascade do |t|
    t.string   "encrypted_password",       null: false
    t.string   "password_salt"
    t.string   "password_archivable_type", null: false
    t.integer  "password_archivable_id",   null: false
    t.datetime "created_at"
  end

  create_table "pages", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "partial_transactions", force: :cascade do |t|
    t.integer  "ptlf_id"
    t.integer  "card_id"
    t.integer  "amount"
    t.integer  "number"
    t.integer  "partial"
    t.date     "charge_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.integer  "view_id"
    t.string   "view_name"
    t.integer  "crear"
    t.integer  "editar"
    t.integer  "leer"
    t.integer  "eliminar"
    t.integer  "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "name"
    t.integer  "flag"
    t.integer  "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.integer  "transactionType_id"
    t.integer  "bin_id"
    t.string   "name"
    t.integer  "percent"
    t.integer  "amount_base"
    t.integer  "amount_desc"
    t.date     "start_at"
    t.date     "end_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "ptlfs", force: :cascade do |t|
    t.integer  "card_id"
    t.integer  "transacctionType_id"
    t.integer  "plan_id"
    t.integer  "promotion_id"
    t.integer  "affiliation_id"
    t.integer  "amount"
    t.integer  "amount2"
    t.string   "approval"
    t.string   "response_code"
    t.string   "address"
    t.integer  "deferral"
    t.integer  "partial"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "devolution"
    t.boolean  "returned_balance"
    t.datetime "post_date"
    t.integer  "user_id"
    t.integer  "registration_id"
    t.integer  "transaction_number"
    t.integer  "service_number"
    t.boolean  "recovered"
    t.string   "name"
    t.string   "check_number"
  end

  create_table "regions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.integer  "id_profile"
    t.integer  "id_page"
    t.boolean  "read"
    t.boolean  "write"
    t.boolean  "edit"
    t.boolean  "delete"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "routers", force: :cascade do |t|
    t.string   "name"
    t.string   "ip"
    t.integer  "port"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string   "name"
    t.string   "port"
    t.string   "contract"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "switches", force: :cascade do |t|
    t.string   "bin"
    t.string   "router"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transacction_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "upload_files", force: :cascade do |t|
    t.string   "file"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "user_id"
    t.datetime "end_upload_file"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                            default: "", null: false
    t.string   "encrypted_password",               default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                  default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "password_changed_at"
    t.string   "unique_session_id",      limit: 1
    t.datetime "last_activity_at"
    t.datetime "expired_at"
    t.string   "user_name"
    t.string   "name"
    t.string   "last_name"
    t.date     "birthday"
    t.string   "boss_name"
    t.string   "phone"
    t.string   "ext"
    t.integer  "profile_id"
    t.string   "area_id"
    t.string   "employment"
    t.integer  "activo"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["user_name"], name: "index_users_on_user_name", unique: true
  end

  create_table "views", force: :cascade do |t|
    t.string   "name"
    t.integer  "crear"
    t.integer  "editar"
    t.integer  "eliminar"
    t.integer  "leer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
